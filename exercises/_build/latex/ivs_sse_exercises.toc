\select@language {english}
\contentsline {chapter}{\numberline {1}Contents:}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Exercise 01: Mean Molecular Weight}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Introduction}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Instructions}{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Visualization}{4}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Task}{4}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Exercise 02: Specific Heat Capacities in Stars}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Introduction}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Instructions}{5}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Task}{6}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Exercise03: Stars as Mixture of Gas and Photons}{7}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Introduction}{7}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Instructions}{7}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Task}{8}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Exercise04: Continuity Equation}{8}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Introduction}{8}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Intensive versus Extensive Variables in MESA}{8}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Transfering Intensive Quantities to the Cell Boundary}{9}{subsection.1.4.3}
\contentsline {subsection}{\numberline {1.4.4}Task}{9}{subsection.1.4.4}
\contentsline {section}{\numberline {1.5}Exercise05: Hydrostatic Balance}{10}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Introduction}{10}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Task}{11}{subsection.1.5.2}
\contentsline {section}{\numberline {1.6}Exercise 06: Numerical Differentiation with MESA}{12}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Introduction}{12}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Instructions}{12}{subsection.1.6.2}
\contentsline {subsection}{\numberline {1.6.3}Task}{12}{subsection.1.6.3}
\contentsline {section}{\numberline {1.7}Exercise 07: Simple Stellar Models with Polytropes}{14}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}Introduction}{14}{subsection.1.7.1}
\contentsline {subsection}{\numberline {1.7.2}Instructions}{14}{subsection.1.7.2}
\contentsline {subsection}{\numberline {1.7.3}Task}{15}{subsection.1.7.3}
\contentsline {section}{\numberline {1.8}Exercise 08: Degenerate Electrons in Stellar Interiors}{16}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Introduction}{16}{subsection.1.8.1}
\contentsline {subsection}{\numberline {1.8.2}Instruction}{17}{subsection.1.8.2}
\contentsline {subsection}{\numberline {1.8.3}Task}{18}{subsection.1.8.3}
\contentsline {section}{\numberline {1.9}Exercise 09: Distinguishing Radiative versus Convective Layers}{18}{section.1.9}
\contentsline {subsection}{\numberline {1.9.1}Introduction}{18}{subsection.1.9.1}
\contentsline {subsection}{\numberline {1.9.2}Mixing Length Theory of Convection}{19}{subsection.1.9.2}
\contentsline {subsection}{\numberline {1.9.3}Task}{20}{subsection.1.9.3}
\contentsline {section}{\numberline {1.10}Exercise 10: Opacity of Stellar Material}{21}{section.1.10}
\contentsline {subsection}{\numberline {1.10.1}Introduction}{21}{subsection.1.10.1}
\contentsline {subsection}{\numberline {1.10.2}The Rosseland Mean Opacity}{22}{subsection.1.10.2}
\contentsline {subsection}{\numberline {1.10.3}Rosseland Opacity vs. Initial Mass}{23}{subsection.1.10.3}
\contentsline {subsection}{\numberline {1.10.4}Remarks}{23}{subsection.1.10.4}
\contentsline {subsection}{\numberline {1.10.5}Tasks}{24}{subsection.1.10.5}
\contentsline {chapter}{\numberline {2}Hints:}{27}{chapter.2}
\contentsline {chapter}{\numberline {3}Indices and tables}{29}{chapter.3}
