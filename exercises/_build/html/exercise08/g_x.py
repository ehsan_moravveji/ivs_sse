
import sys, os, glob
import numpy as np 
import pylab as plt

eps = 1e-4

def plot_g_integral(x_arr, g_arr):
	"""
	Create a plot of g(x) as a function of x 
	"""
	
	fig = plt.figure(figsize=(6,3))
	ax  = fig.add_subplot(111)
	plt.subplots_adjust(left=0.12, right=0.97, bottom=0.16, top=0.97)
	
	ax.plot(x_arr, np.log10(g_arr), linestyle='solid', color='black', lw=2)
	ax.grid(b=True, which='major', color='grey', linestyle='-')
	
	ax.set_xlabel(r'$x=\frac{p_F}{m_e c}$')
	ax.set_ylabel(r'$\log_{10} g(x)$')
	txt = r'$g(x)=\int_0^x \, \frac{x^4}{(1+x^2)^{1/2}}\,dx$'
	ax.annotate(txt, xy=(0.60, 0.37), xycoords='axes fraction', fontsize='large')
	
	plt.savefig('g_x.png')
	plt.close()
	

def g_analytic(x0):
	"""
	Using Wolfram Mathematica integral calculator, the analytic solution go g(x) integral is
	   g(x) = [x * sqrt(1+x^2) * (2x^2 - 3) + 3 * arcsinh(x)] / 8
	where arcsinh(x) is Inverse Hyperbolic Sine function.
	@param x0: the upper integration limit
	@type x0: float
	@return: the integral g(x0)
	@rtype: float
	"""
	
	return (x0 * np.sqrt(1.+x0**2)*(2*x0**2-3) + 3*np.arcsinh(x0))/8
	

def g_integral(x0=1, dx=0.0001):
	"""
	Solve the Integral below with Trapezoidal rule
	   g(x0) = \int_0^x0 x^4 / sqrt(1 + x^2) * dx
	where x0 is the upper integration band. By default, dx is set to 0.0001, but you may change it if you like.
	You can call it like 
	g_x = g_integral(x0=1.245)
	"""
	
	n = int(x0 / dx) + 1
	x = np.linspace(eps, x0, num=n, endpoint=True) 
	func = np.power(x, 4) / np.sqrt(1 + x*x) * dx
	result = np.sum(func)
	
	# print ' - g(x0={0}) = {1}'.format(x0, result)
	return result


def main():
	
	if True:
		# print 'Estimate g(x) integral for a large range of 0 <= x <= 5'
		n = 101
		x_arr = np.linspace(eps, 1, num=n, endpoint=True)
		g_arr = np.zeros(n)
		for i, x in enumerate(x_arr): g_arr[i] = g_integral(x)
		
		plot_g_integral(x_arr, g_arr)
		
	return None
	
		
if __name__ == '__main__':
	status = main()
	sys.exit(status)
	