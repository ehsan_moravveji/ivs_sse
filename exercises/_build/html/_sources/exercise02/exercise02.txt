.. _exercise02:

**********************************************
Exercise 02: Specific Heat Capacities in Stars
**********************************************

.. _Introduction:

Introduction 
============

In stellar physics, we deal with basic quantities, e.g. temperature, density, etc. and their differentials.
In this exercise, we are going to practice that. 
For a practical case, we choose Eq. (2.12) in the lecture note, which is

.. math::
   c_P - c_V = T \left( \frac{\partial v}{\partial T} \right)_P \left( \frac{\partial P}{\partial T} \right)_v, 
   :label: e01

where all quantities have their usual meaning, and :math:`v=\rho^{-1}`.
Thus, we simply rewrite the above equation in favor of :math:`\rho`

.. math::
   c_P - c_V = -\frac{T}{\rho^2} \left( \frac{\partial \rho}{\partial T} \right)_P \left( \frac{\partial P}{\partial T} \right)_\rho. 
   :label: e02

In thermodynamics, the last term is a famous one, and is defined as 
:math:`\chi_T=\left( \partial \ln P/\partial \ln T \right)_\rho`.
This quantity is already availale as a column named **chiT** in your profies.
The first term between paranthesis is already defined in Eq. (2.4) of the notes

.. math::
   \delta = -\left( \frac{\partial \ln \rho}{\partial \ln T} \right)_P = -\frac{T}{\rho} \left( \frac{\partial \rho}{\partial T} \right)_P,
   :label: e03

Thus, we can rewrite the relation for the difference between the specific heats as

.. math:: 
   c_P - c_V = \delta \, \chi_T \frac{P}{\rho T},
   :label: e04

Eq. (2.14) in the notes introduces yet another relation for :math:`c_P-c_V`

.. math::
   c_P - c_V = \frac{P \, \delta^2}{T \, \rho \, \alpha}, 
   :label: e05

where :math:`\alpha=(\partial \ln \rho/\partial \ln P)_T`.
In MESA, :math:`\alpha` and :math:`\delta` are available with column names **dlnRho_dlnPgas_const_T**
and **dlnRho_dlnT_const_Pgas**, respectively.

In MESA, there are two different quantities:

* Intensive quantities like temperature :math:`T`, density :math:`\rho`, pressure :math:`P`, etc. 
  By doubling the volume, the intensive quantities do not double.
  These quantities are always defined at the center of a cell, and are called ``cell-averages``.
  In this case, the center of the zone is defined by mass, not radius.

* Extensive quantities like radius :math:`r`, mass :math:`m`, etc.
  By doubling the volume, the extensive quantities double.
  These quantities are always defined at cell boundaries.

Clearly, in Eqs. :eq:`e01` to :eq:`e05`, all quantities are intensive; hence the specific heats, and their difference 
:math:`c_P-c_V` is cell-averaged.


.. _Instructions:

Instructions
============

We use a similar MESA work directory :file:`exercise02/work` as in :ref:`Exercise01 <exercise01>`. 
So, do the following in your terminal::

    cd exercise02/work
    ./mk
    ./rn

Once finished, you need to use the last profile which is :file:`exercise02/work/LOGS/profile5.prof`.
Read in this profile, and make use of the following columns **cp**, **cv**, **logT**, **logP**, **logRho** and **radius**.
Three useful derivatives are already available as **chiT**, **dlnRho_dlnPgas_const_T** and **dlnRho_dlnT_const_Pgas**.

.. _Task:

Task
====

#. The idea is to compare what MESA internally calculates for :math:`c_P-c_V` which is the left-hand side (LHS) 
   of e.g. Eq. :eq:`e04` and what you can calculate on the right-hand-side (RHS) of Eqs. :eq:`e04` and :eq:`e05`.
   Instead of the absolute difference between the LHS and RHS, we are interested in their relative difference.

   .. math::
      \Delta = \frac{|LHS - RHS|}{LHS}.
      :label: Delta

   then, you must create a plot of the relative difference :math:`\Delta` versus radius using 

   #. RHS in Eq. :eq:`e04`,
   #. and RHS in Eq. :eq:`e05`.

   Finally, you must recover something similar to the following graph.
   Top panel shows the absolute value of the difference of specific heats. 
   Black line is the LHS, and the blue and red lines correspond to the RHS of Eqs. :eq:`e04` and :eq:`e05`, respectively.
   Bottom panel shows the relative difference between the LHS and RHS. Line colors have the same meaning.

   .. image:: ans02.png 
      :width: 100%
      :alt: alternate text

   Why is there a sharp increase close to the surface?

   Repeat the same exercise for a 5M :math:`\!_\odot` model at :math:`X_c=0.50`. What is the difference?

#. If the *assumption* of ideal gas equation of state applied everywhere inside the star, then 

   .. math::
     c_P - c_V = \frac{\mathcal{R}}{\mu},
     :label: R_mu

   where :math:`\mathcal{R}` is the gas constant, and :math:`\mu` is the mean molecular weight (see :ref:`exercise01`).
   Test this assumption inside the model.


