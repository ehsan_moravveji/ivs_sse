
import sys, os, glob
import numpy as np 
from ivs_sse import read
import pylab as plt 

# read the last profile
profiles  = sorted(glob.glob('work/LOGS/profile*.profile'))
last_file = 'work/LOGS/profile7.prof'
dic_prof  = read.read_multiple_mesa_files([last_file], is_hist=False, is_prof=True)[0]
header    = dic_prof['header']
data      = dic_prof['prof']

# from "data", extract the columns corresponding to the radius and mu 
radius    = data['radius']
mu        = data['mu']

# now, plot mu versus radius, 
fig = plt.figure(figsize=(6,4))
ax  = fig.add_subplot(111)

ax.plot(radius, mu, linestyle='solid', lw=2, color='black')

ax.set_xlim(0, 1.1)
ax.set_xlabel(r'Radius')
ax.set_ylim(0.6, 0.9)
ax.set_ylabel(r'Mean Molecular Weight')
fig.tight_layout()
plt.show()
