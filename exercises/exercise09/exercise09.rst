.. _exercise09:

**************************************************************
Exercise 09: Distinguishing Radiative versus Convective Layers
**************************************************************

.. _Introduction:

Introduction 
============
All stars in the universe, with any mass and at any age (evolutionary phase) have only three means to transfer photons from the deep interior towards the surface:

* Radiation

* Convection

* Conduction (that we ignore for now)

While the first two are the major energy transport mechanisms on the most part of the evolution, the latter contributes significantly in degenerate cores of white dwarfs and neutron stars. 
We skip the conduction here, and focus more on the convective and radiative energy transport.

Take a typical main sequence star, where the energy generation occurs in the core. 
Based on the mass and metallicity of the star, different parts of the star are convective *and/or*
radiative. As an example, our Sun has a radiative burning core, and a rather thick convective 
envelope (`Watch this amazing time lapse <https://www.youtube.com/watch?v=W_Scoj4HqCQ>`_) .
On the other hand, a B-type star has an opposite internal structure: a fully developed convective core, 
and a radiative envelope.
During the advanced phases, such as the Asymptotic Giant Branch (AGB), the energy transport becomes even more complex, and multiple convective regions (shells) occur inside the stars 
(where in some of these regions, nuclear burning take place, and in the rest it does not).
Therefore, one of the critical information that we always like to determine (from theory and 
observation) is the extent and properties of the convective and radiative regions, with an 
emphasis on the transition layer between the two. One (among many reasons) is that our current 
*helioseismic* and *asteroseismic* models of pulsating stars are extremely sensitive to the 
extent of convective zones, and exact position of the *convective boundaries*.

Gas giants and low-mass M-type main sequence stars (:math:`M\lesssim0.5\,M_\odot`) are 
*almost* fully convective.
By increasing the initial mass, the core becomes radiative, and a G-type star (e.g. our Sun) 
turns out having a radiative core and an outer convective envelope. Moving up in mass to 
F-type stars with :math:`\sim1.5 M_\odot`,
a small convective core develops in the core, and the size of the outer convective envelope 
shrinks. In A-type stars (:math:`\sim 2.2 M_\odot`), the outer convective layer becomes very
shallow, and almost disappers. Finally, main sequence stars earlier than late B-type 
(:math:`M \gtrsim 3 M_\odot`) have a fully-mixed convective core and a radiative envelope. 
For this exercise, we explore this interesting phenomena for a broad range of initial masses.

.. _MLT:

Mixing Length Theory of Convection
==================================

Currently, the most widely used physical model for convection is the so-called Mixing Length 
Theory (MLT) model proposed by `Erika Bohm-Vitense <http://books.google.be/books?id=msZMEvEpxG8C&pg=PA64&lpg=PA64&dq=MIXING+LENGTH+THEORY+ERIKA+BOHM+VITENSE&source=bl&ots=SU1wC31Hu5&sig=YPDULMXaljkTqhBmWAKMkPD_QeA&hl=en&sa=X&ei=1utlVNzwE8fUaqeegrAJ&ved=0CB8Q6AEwAA#v=onepage&q&f=false>`_. 
The MLT is a local model that expresses the convective flux and velocity of a moving matter in 
terms of average quantities. A very extensive description of the theory can be found in the 
excellent monograph by `Cox and Giuli's Principles of Stellar Structure (1958) <http://adsabs.harvard.edu/abs/2004cgps.book.....W>`_. Based on MLT, The energy flux carried by convection 
:math:`F_{\rm conv}` depends on the difference between the adiabatic temperature gradient 
:math:`\nabla_{\rm ad}` and the radiative temperature gradient :math:`\nabla_{\rm rad}`.
In other words, if an observer stands inside the convective zone, 
:math:`F_{\rm conv}\propto(\nabla_{\rm rad}-\nabla_{\rm ad})`. These two gradients are defined as 

.. math::
   \nabla_{\rm ad} = \left(\frac{\partial \ln T}{\partial \ln P}\right)_{S, \mu}, \\
   \nabla_{\rm rad} = \frac{3}{16\pi acG}\frac{P}{T^4}\frac{\kappa \, L(r)}{m}.
   :label: def_nablas

The *Schwarzschild* condition for convective instability (i.e. for convection to take place)
anywhere inside the star is:

.. math::   
   \nabla_{\rm rad} - \nabla_{ad} \geq 0.
   :label: Sch

Consequently, anywhere in a non-degenerate star that fulfils the condition in Eq. :eq:`Sch` 
is convective, and elsewhere is radiative. As a result of this, the position of the convective 
boundary is where the convective flux becomes zero (i.e. energy is no longer carried by 
convective motion). Based on the Schwarzschild condition, at the convective boundary we have

.. math::
   \left(\nabla_{\rm rad}-\nabla_{\rm ad}\right)_{\rm boundary}=0, 
   :label: bdry

In MESA, :math:`\nabla_{\rm ad}` is returned by the EOS module, whereas :math:`\nabla_{\rm rad}`
is calculated at every point :math:`r`, using the local properties at that mesh point in 
the model, i.e. the local pressure :math:`P(r)`, temperature :math:`T(r)`, 
opacity :math:`\kappa(r)`, mass :math:`m(r)`, and luminosity :math:`L(r)`.
Note that in MESA :math:`\nabla_{\rm ad}` is evaluated and retuned at *cell center*, while 
:math:`\nabla_{\rm rad}` is returned at the face of the cell; see 
:ref:`Intensive_Extensive_Variables` for a thourough description, and how to translate
a variable from cell average to cell face.
In the current MESA versions (<7624), the convective boundaries do not 
exactly fulfil the criteria given in Eq. :eq:`bdry`. Instead, at the cell where the boundary is, 
:math:`\nabla_{\rm rad}-\nabla_{\rm ad}` changes sign. Therefore, one shall search for the 
*local minima* of :math:`|\nabla_{\rm rad}-\nabla_{\rm ad}|` in order to locate the mesh point(s) 
which we switch between radiative/convective zones.

Based on :eq:`bdry`, the position(s) of convective layer(s) is determined by comparing the two
temperature gradients. However, :math:`\nabla_{\rm rad}` depends on the Rosseland mean 
opacity :math:`\kappa(r)`, which depends sensitively on the metallicity :math:`Z`. Therefore,
the whole discussion in this exercise and your final results depends on the adopted initial 
metallicity (e.g. Galactic, LMC, SMC, etc.).

The purpose of this exercise is to explore the convective and radiative regions in different 
stellar models, with an emphasis on the transition layer between the two.

.. _Task:

Task
====

For this task, a convenient work directory is provided in 
:file:`<ivs_sse>/exercises/exercise09/work`. I strongly recommend using this, since the inlists
are tuned towards solving this exercise. In addition, you are already assigned an initial mass
to begin with. For the choice of metallicity, we only explore Solar metallicity :math:`Z=0.014`.
Check the unit of the output columns; some of them could be in CGS, and some with respect to 
the solar value. Thus, for the sake of consistency, carry out all your computations in CGS units,
using the physical constants that MESA usses under the hood. To see the full list of such, 
look at :file:`<mesa-root>/const/public/const_def.f90`.

#. Evolve a model until :math:`X_c=0.60` on the main sequence, and store the last profile.
   Using Eq. :eq:`def_nablas`, and the profile of thermodynamic variables in the output profile,
   compute the radiative temperature gradient :math:`\nabla_{\rm rad}`. Before that, make sure 
   all your quantities of interest are evaluated at the cell face (not cell center). 

   For a quick guide on how to consistently handle this, you can refer to 
   :ref:`Intensive_Extensive_Variables`. If you have doubts whether a quantity is cell-average
   or is at cell-face, you can either refer to the MESA first instrument paper, or the 
   :file:`<mesa>/star/public/star_data.inc` file for additional information.

#. MESA, indeed, computes :math:`\nabla_{\rm rad}` and writes that out in a profile file. For 
   clarity, let's call this :math:`\nabla_{\rm rad}^{\rm (MESA)}`. Now, we want to compare the 
   :math:`\nabla_{\rm rad}` that we have computed with :math:`\nabla_{\rm rad}^{\rm (MESA)}`.
   Ideally, they should be identical up to the round-off level of a 64-bit precison computer 
   machine. To test that, we compute the *relative difference* between the two radiative 
   gradients :math:`\Delta`, and plot :math:`\log\Delta` versus mass (to resolve the core) 
   *and* radius (to resolve the outer layers).

   .. math::
      \Delta = \frac{|\nabla_{\rm rad}^{\rm (MESA)} - \nabla_{\rm rad}|}{\nabla_{\rm rad}^{\rm (MESA)}}.
      :label: delta

   The resulting profile of :math:`\log\Delta` versus mass in the model is presented in Figure
   below. The average deviaton between MESA and user's computation of :math:`\nabla_{\rm rad}`
   is roughly :math:`\Delta\sim 10^{-8}`. This is currently much better than the relative 
   precision of e.g. our seismic observations.

   .. image:: d_gradr.png
      :width: 75%

#. Plot :math:`\nabla_{\rm rad}` and :math:`\nabla_{\rm ad}` in your model, and after a visual
   inspection, explain which part(s) of your model is/are convective or radiative.
   Locate the *relative* mass and radius coordinate of your convective zones by 
   finding the minima of the Schwarzschild criteria, i.e. the minima of 
   :math:`|\nabla_{\rm rad}-\nabla_{\rm ad}|`. Hint: using Python, 
   :file:`scipy.signal.argrelmin` can help you here. :math:`\nabla_{\rm ad}` is given in 
   the profile columns. 
   Note that for any total mass :math:`M_\star` and radius :math:`R_\star` of a model,  
   the relative coordinates (:math:`r/R_\star` and :math:`m/M_\star`) lie between 0 and 1.

   What are the thickness and mass of the convective zones in your model?
   To make this easier, you can fill up a table like the following, from the innermost convective
   boundary (which is the core) towards the outermost convective boundary (which is the surface).
   Remember that the relative mass and radius are between 0 and 1.
   Make sure the location of your boundaries agrees with your figures.
   Also include the surface and the core, because they serve as boundaries anyway.

               +-------------------+------------------+-----------------+
               | Conv. Zone #      | relative mass    | relative radius |
               +-------------------+------------------+-----------------+
               | 1                 | 0.0              | 0.0             |
               +-------------------+------------------+-----------------+
               | 2                 | 0.15             | 0.10            |
               +-------------------+------------------+-----------------+
               | ...               | ...              | ...             |
               +-------------------+------------------+-----------------+

#. During the post-main sequence evolution, the number, location and size of convective 
   regions changes progressively. To explore this, repeat the above three tasks, for a 
   post main sequencey model. To do so, make sure your star is beyond the main sequence 
   (i.e. :math:`X_c=0.0`) when it reaches :math:`T_{\rm eff}=4\,500` K. Once these two
   conditions are fulfiled, save a model, and repeat your analysis. 

   What structural differences do you observe between a main sequence and a post-main sequence
   model?


