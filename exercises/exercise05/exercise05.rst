.. _exercise05:

*******************************
Exercise05: Hydrostatic Balance
*******************************

.. _Introduction:

Introduction
============
For stars that are not in their rapid evolutionary phase (e.g. suprnova phase), the changes in the 
sturcture of the star are moderate.
If the star is in a stable nuclear burning phase (e.g. main sequence, or core He-burning phase), then
the evolution is governed by the nuclear time scale :math:`\tau_{\rm nuc}`. 
In case a star is in between two stable burning phases (like the sub-giant phase, being between end of 
core H burning and beginning of core He burning), then evolution is on Kelvin-Helmholtz time scale
:math:`\tau_{\rm KH}`, which is considerably shorter than :math:`\tau_{\rm nuc}`. 
Therefore, we can safely assume the star being in *hydrostatic equilibrium* (HSE) over shorter timescales,
e.g. the dynamical timescale :math:`\tau_{\rm dyn}<<\tau_{\rm KH}<<\tau_{\rm nuc}`.
An example of this is our Sun, at this very moment, *now!*
Although, the Sun burns few thousand tons of hydrogen every second, that is considered negligible 
compared to its total mass;
therefore, the Sun can be considered in perfect HSE on the *human* timescale.
In such a situation, the total pressure force is counter-balanced by the pull of gravity;
therefore, in the Lagrangian framework we have

.. math::
   \frac{\partial P}{\partial m}=-\,\frac{G\,m}{4\pi\, r^4},
   :label: eq1

See Eq. (3.17) in the notes, also :ref:`Intensive_Extensive_Variables` and :ref:`cell_center_to_cell_edge`.
In this exercise, we would like to check how well the hydrostatic equilibrium is fulfilled at each step of 
evolution, in each cell inside the model.
This exercise is also similar to :ref:`exercise04` and :ref:`exercise06`.

.. _Task:

Task 
====

To give more flavour of different evolutionary phases, this time, we are 
going to check HSE in a core helium burning red giant. 
These stars are called secondary red clump stars.

#. Use the provided :file:`work` directory to evolve a 2.30 :math:`{\rm M}_\odot` 
   model up to the core helium burning phase, and stop at center helium mass 
   fraction :math:`Y_{\rm core}=0.25`, and save a profile at this point.
   
#. Translate :math:`\overline{dm}_k` (:file:`dq` or :file:`dm` columns in the profile) 
   from cell center to the cell boundary. 
   Instructions for this are already given in :ref:`cell_center_to_cell_edge`.
   
#. Since pressure :math:`P` is a cell average quantity, its derivative 
   :math:`\overline{dP}_k=P_{k-1}-P_k` is at cell boundary by definition.
   So, you do not need to do more on that.
   
#. Evaluate the LHS of Eq. :eq:`eq1`, at every cell :math:`k`, by the simple division 
   :math:`\overline{dP/dm}_k=\overline{dP}_k/\overline{dm}_k`; thus,
   :math:`\overline{dP/dm}` is certainly at cell boundary.
   
#. The ingredients of the RHS of Eq. :eq:`eq1` are all extensive quantities. 
   So, life's been already made easy.
   
#. Define a dimensionless parameter :math:`\Delta` to measure the quality of the equality in Eq. :eq:`eq1`, and plot 
   :math:`\log_{10}\Delta` versus mass and/or radius in your model.
   
   .. math::
      \Delta = \left| \frac{{\rm LHS} - {\rm RHS}}{{\rm RHS}} \right|.
      
#. How do you compare the overall equality in Eq. :eq:`eq1` with *double precision* numerical precision?
   Which part(s) of the model are in better HSE? 
   In which part(s) the HSE is maintained (relatively) worse?
   Why?
   
At the end, you will come up with a plot like the following.

.. image:: HSE.png
   :width: 75%   
   
