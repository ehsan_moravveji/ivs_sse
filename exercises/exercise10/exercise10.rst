
.. _exercise10:

******************************************
Exercise 10: Opacity of Stellar Material
******************************************

.. _Introduction:

Introduction 
============

Stellar radiative opacity -- designated by :math:`\kappa` in stellar structure and evolution 
equations -- is one of the most critical ingredients of the equations.
The luminosity of a star depends explicitly on how opaque it is.
A tiny little change in :math:`\kappa` changes the global properties of the star like its luminosity, 
radius and effective temperature, in addition to its internal properties, like the extent of 
the convective layers, etc.
Therefore, :math:`\kappa`  must be treated well.
The detials of stellar opacity and different contributing sources are considered in chapter 8 of 
the monograph by 
`Andre Maeder (2009) <http://www.springer.com/astronomy/extraterrestrial+physics,+space+sciences/book/978-3-540-76948-4>`_.
Below, I *quote* him, when necessary.
The idea of this exercise is to reproduce Fig. 8.4 there, and check the validity of the Kramer's law 
(for low mass stars), and the electron scattering for (high mass stars).

Let's briefly review the sources of opacity, without going in depth!
From now, the opacity is expressed in :math:`{\rm cm}^2 {\rm gr}^{-1}`.
There are four major sources that absorp and/or scatter the photons inside stars, and shorten 
their mean free path, hence, contributing to the stellar opacity:


* **Bound-bound transition** :math:`\kappa_{\rm bb}` (line absorption):
  This corresponds to the absorption of photons by partially ionized species, giving rise to 
  inter-shell transitions.
  *"The absorption by spectral lines, due to bound-bound (bb) transitions between atomic levels, 
  makes about 10% of the opacity in the solar center and about 50% in the outer layers ...
  The difficulty to account for all spectral lines is a major source of uncertainty in the 
  calculation of stellar opacities."*
  
  Currently, different projects (like `OP`_, and `OPAL`_) independently update :math:`\kappa_{\rm bb}` 
  by accounting 
  for millions of line transitions between Iron-group elements, such as Fe, Ni, Mn, Cr etc.
  The Iron opacity bump around :math:`\log T\approx5.3` inside massive stars is the famous result of 
  bound-bound transitions. There are no simplifying relations for :math:`\kappa_{\rm bb}`.

  .. _OP:   http://cdsweb.u-strasbg.fr/topbase/home.html
  .. _OPAL: http://opalopacity.llnl.gov

* **Electron scattering** :math:`\kappa_{\rm es}`: 
  This is dominant in massive stars, and gives rise to a (roughly constant) background opacity.
  *"Free electrons cannot absorb photons, but they only diffuse them according to the conservation laws.
  Diffusion, nevertheless, produces an attenuation of the flux in the propagation direction."*
  With :math:`X` the local hydrogen abundance inside the star, :math:`\kappa_{\rm es}` is

  .. math::
    \kappa_{\rm es}=0.200 \, (1\,+\,X),
    :label: k_es

* **Bound-free transitions** :math:`\kappa_{\rm bf}` (photo-ionization):
  When the energy of an incident photon is larger than or equal to the binding energy of an electron, the electron can be
  liberated. 
  A rough estimate of the bound-free (bf) opacity is given by the so called Kramers law:

  .. math::
    \kappa_{\rm bf} = \kappa^{(0)}_{\rm bf} \, \rho \, T^{-3.5}, \quad {\rm with} \quad \kappa^{(0)}_{\rm bf} \simeq 4.3 \times 10^{25} \, Z \, (1+X),
    :label: k_bf

  where :math:`\kappa^{(0)}_{\rm bf}` is a coefficient which depends on the metallicity :math:`Z`. 
  Thus, :math:`\kappa_{\rm bf}` is a main contributor to the opacity in lower mass stars like the Sun.

  It is worth mentioning that *only* over the temperature range of 3 000 to 10 000 K (which corresponds
  to very outer layers of cooler stars), a free electron can marry a neutral hydrogen, and the form 
  :math:`H^-` ion.
  The required free electrons are supplied by metals in the outer layers of stars. Elements such as Na, K, Ca and Al
  have low ionization potentials, and can contribute effectively to supply free electrons. Consequently, such
  free electrons can bind with netural hydrogen atoms, and form the :math:`H^-` ion. However, photons with 
  :math:`\lambda < 1.655 \,\mu{\rm m}` disrupt this unstable ion. This additional photon absorption contributes
  to the bound-free opacity (only in the outer layers of cool stars). The corresponding opacity behaves like

  .. math::
    \kappa_{H^-} = \kappa^{(0)}_{H^-} \, \sqrt{\rho} \, T^9, \quad {\rm with} \quad \kappa^{(0)}_{H^-} \simeq 2.5\times10^{-31} \frac{Z}{0.02},
    :label: k_H

  Note that :math:`\kappa_{H^-}` is applicable only at :math:`T\approx (3-6)\times10^3` K, 
  :math:`\rho\approx(10^{-10} - 10^{-5})` g cm :math:`^3`, and :math:`Z=0.001 - 0.02`.
  To account for the full bf opacity :math:`\kappa_{\rm bf}`, one must sum up :math:`\kappa_{\rm bf}` 
  from Eq. :eq:`k_bf` 
  with :math:`\kappa_{H^-}` in Eq. :eq:`k_H` over the correct temperature/density range (so some 
  care should be paid here).

* **Free-Free transitions** :math:`\kappa_{\rm ff}`:
  *"A free electron of velocity* :math:`v` *moving in the Coulomb filed of an ion of charge* :math:`Z_j e` *can absorb (ff absorption) or emit
  (Bremsstrahlung) a photon of frequency* :math:`\nu`".
  Again, a rough approximation to :math:`\kappa_{\rm ff}` is 

  .. math::
    \kappa_{\rm ff} = \kappa^{(0)}_{\rm ff} \, \rho\, T^{-3.5}, \quad {\rm with} \quad \kappa^{(0)}_{\rm ff} \simeq 3.7\times10^{22} (X+Y)(1+X),
    :label: k_ff

  where :math:`Y` is the local helium abundance.
  *"The coefficient* :math:`\kappa^{(0)}_{\rm ff}` *is smaller than* :math:`\kappa^{(0)}_{\rm bf}` 
  *for standard composition, so that bf transitions 
  dominate over ff effects. However, the free-free opacity is essentially independent of 
  metallicity* :math:`Z` *and thus it dominates over bound-free opacity in low-Z stars."*

**A Remark on the Scaling Relations:** 
Note that the scaling relations for different opacity sources in Eqs. :eq:`k_es`,
:eq:`k_bf`, :eq:`k_H`, and :eq:`k_ff` are approximate, and useful for an order-of-magnitude
estimation. 
The detailed computation of each opacity source require quantum mechanical consideration which
are elaborated in the literature. 
For further information, please refer to `Maeder (2009)`_ and the new editon of `Cox \& Giuli (1968)`_

.. _Cox \& Giuli (1968): http://adsabs.harvard.edu/abs/2004cgps.book.....W
.. _Maeder (2009): http://link.springer.com/book/10.1007%2F978-3-540-76949-1


.. _Rosseland:

The Rosseland Mean Opacity
=================================

Except the electron scattering :math:`\kappa_{\rm es}`, the other sources of opacity depend on the frequency of the emitted photon :math:`\nu`.
Therefore, the net opacity is frequency depenent:

.. math::
  \kappa_\nu = \left[ \kappa_{\rm bf}(\nu) + \kappa_{\rm ff}(\nu) + \kappa_{\rm bb}(\nu) + \cdots  \right]\left( 1- e^{-h\nu/kT} \right) + \kappa_{\rm es} + \cdots,
  :label: k_nu

Thus, the effective radiative opacity is the integral over all frequencies, asuuming that the photon flux in stellar interiors obeys 
the black body emissivity distribution function :math:`B_\nu(T)`:

.. math::
  \frac{1}{\kappa} = \frac{1}{dB/dT} \int_0^\infty \frac{1}{\kappa_\nu} \frac{dB_\nu}{dT}\, d\nu, 
  :label: k_rad

.. math::
  {\rm with} \quad \frac{dB}{dT} = \int_0^\infty \frac{dB_\nu}{dT} = \frac{ac}{\pi}\, T^3, \\
  {\rm and} \quad B_\nu(T) = \frac{2h}{c^2}\frac{\nu^3}{\exp{[h\nu/kT]}-1}.
  :label: B_nu

Note that :math:`\kappa_\nu` is plugged in from Eq. :eq:`k_nu`.
The good news is that the `OP <http://cdsweb.u-strasbg.fr/topbase/home.html>`_ and 
`OPAL <http://opalopacity.llnl.gov>`_ folks have already took care of the tedious task of computing 
the Rosseland mean opacity :math:`\kappa` as in Eq.  :eq:`k_rad`.
As a result, the Rosseland mean opacity is already computed and tabulated (as ASCII tables) for use in
stellar evolution codes.
For instance, MESA loads all required opacity tables at the begining of the run, and interpolates between
all opacity tables, to find the Rosseland opacity at every point inside the star 
:math:`\kappa=\kappa(\rho, T, Z)`, given the local temperature :math:`T`, density :math:`\rho` 
and metallicity :math:`Z`.
Therefore, for the computation of stellar models (by e.g. MESA), it is not required to go through 
Eq. :eq:`k_rad`; instead, one can read from the pre-compiled opacity tables. 
Another example of such is also given in the `OP_Mono`_ package.

.. _OP_Mono: https://bitbucket.org/ehsan_moravveji/op_mono/overview

In this exercise, we explore the behaviour of :math:`\kappa(\rho, T, Z)` as a function of stellar 
initial mass, metallicity and temperature profile from core to the surface.


.. _kappa_vs_mass:

Rosseland Opacity vs. Initial Mass
==================================
Let's consider the scaling relations to explore which of the above opacity mechanisms dominate
inside the star, depending on the stellar mass. We can explore this over a broad range of initial
masses on the entire main sequence (from M-type to O-type stars).
For the main sequence stars, the effective temperature increases by an increase in initial mass. 
That is why on the HR diagram, the main sequence stretches diagonally from cool low-luminosity end 
(for M-dwarfs) to hot high-luminosity end (for O-type dwarfs).
In addition, by increasing the initial mass of stars, the mean density of the stars decreases,
and bound-free (Eqs. :eq:`k_bf` and :eq:`k_H`) and free-free (Eq. :eq:`k_ff`) opacities that 
depend explicitly on density start loosing their importane.

The combined effect of the variation in density and effective temperature on the resulting opacity 
is that depending on the initial mass of a star, the hydrogen, helium or both in the atmosphere 
could be fully ionized. 
In such cases, the photo-ionization contribution (:math:`\kappa_{H^-}` and :math:`\kappa_{\rm bf}`)
loose their importance, and the bound-bound transition and electron scattering start to dominate.

An illustration of this fact is presented in the figure below, where different opacity profiles 
are presented inside several models from the core to the surface. 
All models are calculated with :math:`Z=0.02` and at the age :math:`X_{\rm c}=0.50`, over a broad
mass range from 1 to 30 :math:`M_\odot`.
Evidently, the *opacity contrast* between the core up to the surface in lower-mass stars is much
stronger than in high-mass stars. 
Also note that in higher-mass models, the surface temperature increases by mass.
This exercise tries to exploit all these facts.

   .. image:: log_opacity.png
     :width: 75%

.. _Remarks:

Remarks
=======
#.  For this exercise, you can use the work directory in:
    :file:`<ivs_sse>/exercises/exercise10/work`.

#.  The Rosseland opacity is given in the profile output file under the :file:`opacity` column.
    Moreover, you can find the profiles of temperature :math:`T`, density :math:`\rho`, hydrogen
    :math:`X`, helium :math:`Y` and metal abundance :math:`Z` in the same file.

#.  For the followint task, you require to specify your initial composition, i.e. :math:`(X,\,Y,\,Z)`.
    In MESA, you can use the following :file:`star_job` inlist options to specify the initial 
    helium :math:`Y` and metallicity :math:`Z`. 
    Indeed, the initial hydrogen mass fraction :math:`X`, will be adjusted internally, so that 
    :math:`X+Y+Z=1`.
    An example is given below::

       change_initial_Y = .true.
       new_Y = 0.276
       change_initial_Z = .true.
       new_Z = 0.014


.. _Task:

Tasks
=====

#. For your assigned initial mass, evolve a star on the main sequence, and stop at :math:`X_{\rm c}=0.50`,
   for a typical Population I star. 
   These stars are younger generation, and are found predominantly in the disk of our Galaxy. 
   The composition of these stars can be roughly assumed to as :math:`(Y,\,Z)=(0.27,\,0.02)`.
   Based on your last MESA profile, answer the following questions:

   .. * Estimate the bound-bound opacity :math:`\kappa_{\rm bb}` by subtracting :math:`\kappa`
   ..   from other opacity sources, i.e. 
   ..   :math:`\kappa_{\rm bb}\approx\kappa-(\kappa_{\rm es}+\kappa_{\rm bf}+\kappa_{H^-}+\kappa_{\rm ff})`.
   ..   Note that :math:`\kappa_{\rm H}` has a restricted validity range in temperature and density
   ..   (Eq. :eq:`k_H`).

   * Plot the logarithm of opacity :math:`\log\kappa` in your model versus the logarithm of temperature
     :math:`\log T`, similar to the figure above.
     On the same plot, overplot the approximate contributions of :math:`\kappa_{\rm es}`, 
     :math:`\kappa_{\rm bf}`, :math:`\kappa_{H^-}` and :math:`\kappa_{\rm ff}`, from Eqs. 
     :eq:`k_es`, :eq:`k_bf`, :eq:`k_H`, and :eq:`k_ff`.

   * What is the order-of-magnitude change of opacity :math:`\kappa` in your model?
     What are the minimum and maximum values?
     Where -- expressed in :math:`\log T` -- do the absolute minimum and absolute maximum of 
     the opacity occur?

   * What is the dominant source of opacity in your model? 
     Explain why.

   * What is the least important source of opacity in your model?
     Explain why.

   * What is the dominant source of opacity at the center and surface of your model?
     what is the dominant source of opacity somewhere in the middle of your star?

   * Locate the *local* maxima of your Rosseland opacity in terms of :math:`\log T`.
     Do these locations coincide with partial ionization zones of specific elements?
     If so, specify which element, and which transition occurs in that specific region.

   * Which opacity component (:math:`\kappa_{\rm es},\, \kappa_{\rm bf},\, \kappa_{\rm H},\, 
     \kappa_{\rm ff},\, {\rm or}\, \kappa_{\rm bb}`) contributes predominantly to the local 
     opacity maxima?

   .. image:: kappa_components.png
      :width: 75%

#. Repeat the same task, but this time for a model with half initial metallicity, i.e. 
   :math:`(Y,\,Z)=(0.27,\, 0.01)`.

   * Discuss the similarities and differences between the two opacity profiles with 
     low and high metallicity models.

   * In addition, explain the percentage of change in the maximum opacity value between 
     high- and low-metallicity model.
     For this purpose, define a parameter, :math:`\Delta`, which compares the relative
     difference between the maximum opacity of a high-Z model 
     :math:`\kappa_{\rm max}^{\rm (hi)}`, and that of a low-Z model 
     :math:`\kappa_{\rm max}^{\rm (lo)}` in percentage.

     .. math:: 

        \Delta [\%]
         = \frac{\kappa_{\rm max}^{\rm (hi)} - \kappa_{\rm max}^{\rm (lo)}}{\kappa_{\rm max}^{\rm (lo)}}
           \,\times\,100.

     :math:`\Delta` gives you a rough estimate of how the maximum opacity in your model 
     can change due to generation of newer (hence higher metallicity) stellar populations in 
     our Galaxy.

#. Now, use the MESA history files for the entire main sequence evolution.
   For your models from Task (1) and Task (2), plot the surface luminosity :math:`\log(L/L_\odot)`, 
   and surface radius :math:`R/R_\odot` versus hydrogen mass fraction in the core :math:`X_{\rm c}`.

   * What is the difference between high-Z versus low-Z stars in terms of their
     radius, effective temperature, luminosity and age, at fixed center hydrogen mass fraction 
     :math:`X_{\rm c}=0.50`?






.. Tasks for the academic year: 2014
.. #. **Opacity variation with mass**:
..    Use the provided inlist, and evolve 5 models at :math:`Z=0.020`, until :math:`X_c=0.50`.
..    For the initial masses, take 1, 1.5, 2, 5 and 30 :math:`{\rm M}_\odot`.
..    Plot :math:`\log\kappa` against :math:`\log T` for all these models.
..    You will come up with a plot like the following

..    .. image:: log_opacity.png
..      :width: 75%

..    Please answer the following questions:

..    * Why lower mass stars are more opaque?

..    * Why for most opacity curves, the minimum of :math:`\log \kappa` is around -0.5?

..    * What does the significant bump around :math:`\log T\approx4.6` correspond to?
..      What about the similar bumps around :math:`\log T\approx 5.3`, and :math:`\log T\approx 6.3`?

.. #. **Opacity variation in a Solar type star:**
..    Now, only take the 1 :math:`{\rm M}_\odot` model.
..    Use Eqs. :eq:`k_es` to :eq:`k_ff` to *approximate* the contribution of every component of the radiative opacity, based on the scaling laws.
..    On a logarithmic scale, create a plot that shows :math:`\log \kappa`, and all components: :math:`\log \kappa_{\rm es}`, 
..    :math:`\log \kappa_{\rm bf}` and :math:`\log \kappa_{\rm ff}`.
..    Now, answer the following questions:

..    * What is the order of magnitude change in :math:`\log \kappa`?

..    * What is the role of electron scattering here?

..    * Which opacity component is in charge at :math:`\log T\gtrsim 5.5`? **Why?**

..    * Which opacity component is in charge around :math:`\log T\approx4.5`? What is happening in this layer?
 
.. #. **Opacity variation in an O-type star:**
..    Now, only take the 30 :math:`{\rm M}_\odot` model.
..    Quite similar to Task 2, use Eqs. :eq:`k_es` to :eq:`k_ff` to *approximate* the contribution of every component of the radiative opacity, 
..    based on the scaling laws.
..    On a logarithmic scale, create a plot that shows :math:`\log \kappa`, and all components: :math:`\log \kappa_{\rm es}`, 
..    :math:`\log \kappa_{\rm bf}` and :math:`\log \kappa_{\rm ff}`.
..    Now, answer the following questions:

..    * What is the order of magnitude change in :math:`\log \kappa`?
..      Compare with a low-mass model!

..    * What is the role of electron scattering here?

..    * What is the contribution of :math:`\log\kappa_{\rm bf}` and :math:`\log\kappa_{\rm ff}`?

..    * Which component is responsible for the difference between the Resseland mean opacity :math:`\log\kappa`
..      and the electron scattering opacity :math:`\log\kappa_{\rm es}`?


