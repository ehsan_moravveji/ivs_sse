
.. _exercise06:

************************************************
Exercise 06: Numerical Differentiation with MESA
************************************************

.. _Introduction:

Introduction 
============

In stellar astrophysics, we deal with several quantities and their differentials.
Example of such is the temperature :math:`T` and its gradient with respect to pressure
:math:`\nabla=\left( \partial\ln T \,/\, \partial\ln\rho \right)`.
MESA as a tool for experimenting with model stars calculates such basic parameters and their 
differentials. 
The purpose of this exercise is to reproduce and compare the pressure :math:`P` derivative
with respect to the mass, i.e. :math:`d\ln P/dm` with the internal calculation of the same quantity
in MESA. 
For this, please study Sec. 6.2 and Fig. 9 in the first MESA instrument paper by `Paxton et al 2011`_,
in addition to the first 50 lines on top of :file:`star_data.inc` file in your root MESA directory::

  more -50 $MESA_DIR/star/public/star_data.inc

.. _Paxton et al 2011: http://adsabs.harvard.edu/abs/2011ApJS..192....3P


In MESA, there are two different quantities:

* Intensive quantities like temperature :math:`T`, density :math:`\rho`, pressure :math:`P`, etc. 
  By doubling the volume, the intensive quantities do not double.
  These quantities are always defined at the center of a cell, and are called ``cell-averages``.
  In this case, the center of the zone is defined by mass, not radius.

* Extensive quantities like radius :math:`r`, mass :math:`m`, etc.
  By doubling the volume, the extensive quantities double.
  These quantities are always defined at cell boundaries.

Based on this variable classification, then differentiation of variables of the the same type, e.g.
:math:`dr/dm` or :math:`d\rho/dT` is straightforward.
However, derivatives of the mix quantities, such as :math:`dT/dr`, have to be carried out with care.
In such cases, we have to decide to differentiate at either the cell center or the cell face.
The reason is that, in e.g. :math:`dT/dr`, the numerator :math:`dT` is the difference of temperature,
where the temperature is intensive, hence at cell centers, and the numerator :math:`dr` consists of 
:math:`r` which is an extensive quantity.
Even more delicate than that, the difference of an extensive quantity, e.g. :math:`dr_k\,=\,r_k\, - \, r_{k+1}` 
is defined at cell center! Please see Fig. 9 in `Paxton et al 2011`_.
Here, :math:`k` denotes the index of each cell, and runs from 1 (surface) to nz (core).
Similarly, pressure :math:`P` is defined at cell centers, but its differnce :math:`dP_k \,=\,P_{k-1} \,-\, P_k`
is situated at cell face.
Please familiarize yourself with this important concept.

.. _Instructions:

Instructions
============

In the following, we try to calculate the logarithmic presure derivative with respect to mass, i.e. :math:`d\ln P/dm`.
The reason we stick to the logarithm is that the slope of the pressure change with respect to mass, is quite modest.
MESA solves the structure equations in Lagrangian frame (Sec. 3.1.2 in the notes), so the mesh in the model is done
based on mass increments :math:`dm`, and its normalized form :math:`dq\,=\,dm/M_\star`.
Therefore, the intensive quantities are defined in the *center of mass* of each cell.
To transfer an intensive quantity from cell center to the cell face, MESA uses a specific mass averaging scheme.
In this scheme, the valua of each intensive quantity, say :math:`T` at the face of :math:`k` th cell, i.e. :math:`\overline{T}_k`
is an average of :math:`T` at the center of :math:`k` th and :math:`k-1` th cell weighted by :math:`\alpha`::

  for k in range(0, nz):
    if k == 0: alpha = 1.0
    if k > 0: alpha = dq[k-1] / (dq[k-1] + dq[k])
    beta      = 1 - alpha
    P_face[k] = alpha * P_cent[k] + beta * P_cent[k-1]

Clearly, you need to use the **dq** column from your profile file.
You will need the above *translation* algorithm whenever you want to transfer one intensive variable from the cell
center to the face.
In the tasks, you will deal with an application of such.

.. _Task:

Task
====

#. Back of envelope question: What is a your estimate of :math:`d\ln P/dm` for a rough model of a Sun?
   Use cgs units.
#. MESA calculates :math:`d\ln P/dm`, and it is available in all profiles with the column name **dlnP_dm**.
   It will look like the following smooth curve:

   .. image:: dlnP_dm.png
      :width: 100%

   You should separately calculate :math:`d\ln P/dm` too. 
   For clarification, an overbar designates an intensive quantitiy which is translated to the cell face. 
   Now, follow these steps:

   * calculate :math:`dP` at each cell face: :math:`\overline{dP}_k = P_{k-1}-P_k`,
   * calculate :math:`dm` at each cell center: :math:`dm_k=m_k-m_{k+1}`,
   * translate :math:`dm` from cell center to the cell face: :math:`\overline{dm}_k=(dm_k+dm_{k-1})/2`,
   * translate pressure from cell center to cell face as above: :math:`\overline{P}_k=\alpha P_k + (1-\alpha)P_{k-1}`,
   * evaluate :math:`dP/dm` at face: :math:`(dP/dm)_k=\overline{dP}_k/\overline{dm}_k`,
   * evaluate :math:`d\ln P/dm` at face: :math:`(d\ln P/dm)_k = (dP/dm)_k/\overline{P}_k`,
   * take a logarithm of your :math:`d\ln P/dm` and that of MESA, and compare :math:`\log_{10}(d\ln P/dm)` 
     versus radius in a plot.

#. Define a quality paramter, called :math:`\Delta` for :math:`d\ln P/dm`

   .. math:: 
      \Delta=\,|\,{\rm MESA} \,-\, {\rm User}\,|,

   It guages in the abslute sense, how well you have reproduced the same profile in MESA.
   Plot :math:`\log\Delta` versus radius for your profile file, and explain if your derivative is close
   to MESA's or not? 
   If so, at what precision?

   Is it comparable to the 64-bit machine double precision? how?
   
.. Hint: The proper way to calculate :math:`dT` at cell face is the following: 
.. :math:`\overline{dT}_k=\overline{T}_k \times (\log T_{k-1} - \log T_k)`.
