
.. _exercise11:

******************************************
Exercise 11: Energy Sources in Stars
******************************************

.. _Introduction:

Introduction 
============
Stars shine, because they loose energy in the form of electromagnetic radiation 
(photons), neutrino emission, and gravitatinal energy loss (that we ignore here).
The conservation of energy dictates that at every step of the evolution of a star,
the net some of all sources of energy must be constant. 
In this exercise, we experiment with this fundamental law of physics.

Stars are sources and sinks of energy.
Energy is produced by (a) nuclear reactions :math:`\epsilon_{\rm nuc}`, in 
addition to (b) gravitational collapse :math:`\epsilon_{\rm grav}`.
At the same time, energy is lost by (a) photons from the surface (that we assosiate with 
stellar luminosity), in addition to (b) thermal neutrinos :math:`\epsilon_{\nu, {\rm th}}`. 
Inside stars, energy is converted and exchanged into the internal energy, gravitational
potential and kinetic energy.
Yet, the sum of all forms should stay fixed.

.. On the main sequence


.. KH timescale = eps_grav / Lum 

.. The two well known reaction networks for hydrogen burning are the proton-proton (pp) chanin,
.. or the CNO cycle.
.. Due to the fact that each network depends on a specific ...weak... reaction 
