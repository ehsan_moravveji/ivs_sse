
! inlist_wd_cool_0.6M



&star_job
      show_log_description_at_start = .false.

      load_saved_model = .true.
      saved_model_name = 'wd_0.6.mod'

      save_model_when_terminate = .false.
      save_model_filename = 'final.mod'
      
      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = 'LOGS/wd.prof'

      kappa_file_prefix = 'gs98' 
         ! modify this to select a different set of preprocessed opacity tables
         
      eos_file_prefix = 'macdonald'   ! use this to avoid the OPAL/SCVH blend problems

      set_initial_dt = .true.
      years_for_initial_dt = 1d2 ! in years

      history_columns_file = 'hist_col.list'
      profile_columns_file = 'prof_col.list'

      pgstar_flag = .false.

/ ! end of star_job namelist



&controls

      max_model_number = 10
      
      ! check for retries and backups as part of test_suite
      ! you can/should delete this for use outside of test_suite
         max_number_backups = 10
         max_number_retries = 40



      prune_bad_cz_min_Hp_height = 0 ! lower limit on radial extent of cz
      remove_mixing_glitches = .true. ! if true, then okay to remove gaps and singletons
      
               ! the following controls are for different kinds of "glitches" that can be removed
            
               clip_D_limit = 0 ! zero mixing diffusion coeffs that are smaller than this         

               okay_to_remove_mixing_singleton = .true.
         
               min_convective_gap = -1 ! 0.1 
                  ! close gap between convective regions if smaller than this (< 0 means skip this)
                  ! gap measured radially in units of pressure scale height
         
               min_thermo_haline_gap = 0
                  ! close gap between thermohaline mixing regions if smaller than this (< 0 means skip this)
                  ! gap measured radially in units of pressure scale height
               
               min_thermo_haline_dropout = 0
               max_dropout_gradL_sub_grada = 1d-3
                  ! if find radiative region embedded in thermohaline,
                  ! and max(gradL - grada) in region is everywhere < max_dropout_gradL_sub_grada
                  ! and region height is < min_thermo_haline_dropout
                  ! then convert the region to thermohaline
         
               min_semiconvection_gap = 0 
                  ! close gap between semiconvective mixing regions if smaller than this (< 0 means skip this)
                  ! gap measured radially in units of pressure scale height
         
               remove_embedded_semiconvection = .false.
                  ! if have a semiconvection region bounded on each side by convection,
                  ! convert it to be convective too.
         
 
      Teff_lower_limit = 3000
 

      initial_mass =  0.60
      initial_z = 0.01d0
      
      use_Type2_opacities = .true.
      Zbase = 0.01d0

      mesh_delta_coeff = 0.75

      which_atm_option = 'WD_tau_25_tables' ! use this for evolution
      
      
      when_to_stop_rtol = 1d-3
      when_to_stop_atol = 1d-3

      photostep = 50
      profile_interval = 100
      history_interval = 100
      terminal_interval = 10
      write_header_frequency = 10
      
      allow_semiconvective_mixing = .true.
      allow_thermohaline_mixing = .true.
      thermo_haline_coeff = 1000  

      MLT_option = 'ML2'
      
      smooth_convective_bdy = .false.


      mixing_length_alpha = 1.8

      ! convergence controls
         varcontrol_target = 1d-5
         
         max_iter_for_resid_tol1 = 3
         tol_residual_norm1 = 1d-5
         tol_max_residual1 = 1d-2
         
         max_iter_for_resid_tol2 = 99  ! make this larger than max allowed iterations
         tol_residual_norm2 = 1d99
         tol_max_residual2 = 1d99

      ! FOR DEBUGGING

      !report_hydro_solver_progress = .true. ! set true to see info about newton iterations
      !report_ierr = .true. ! if true, produce terminal output when have some internal error
      
      !max_years_for_timestep = 3.67628942044319d-05

      !report_why_dt_limits = .true.
      !report_all_dt_limits = .true.
      
      !show_mesh_changes = .true.
      !mesh_dump_call_number = 5189
      !okay_to_remesh = .false.
      
      !trace_evolve = .true.
      
      !hydro_show_correction_info = .true.

      ! hydro debugging
      !hydro_check_everything = .true.
      !hydro_inspectB_flag = .true.
      
      !hydro_numerical_jacobian = .true.
      !hydro_save_numjac_plot_data = .true.
      !small_mtx_decsol = 'lapack'
      !large_mtx_decsol = 'lapack'
      !hydro_dump_call_number = 892

/ ! end of controls namelist


&pgstar
         
         
         TRho_Profile_win_flag = .true.
         show_TRho_Profile_kap_regions = .false.
         show_TRho_Profile_eos_regions = .false.
         show_TRho_Profile_degeneracy_line = .true.
         show_TRho_Profile_Pgas_Prad_line = .true.
         show_TRho_Profile_burn_lines = .true.
         show_TRho_Profile_burn_labels = .true.
         show_TRho_Profile_logQ_limit = .true.


/ ! end of pgstar namelist
