
.. _exercise08:

******************************************************
Exercise 08: Degenerate Electrons in Stellar Interiors
******************************************************

.. _Introduction:

Introduction 
============

In roughly 10 billion years, our Sun will become a red giant star, with its radius reaching beyond the orbit of Mars!
Such a star has a very dillute envelope, but a very dense core. 
This core comprises of pure inert helium.
The core is supported by the electron degeneracy pressure, while the envelope is supported by the gas pressure in the
classical regime.
The purpose of this exercise is to investigate where we need the classical versus the quantum mechanical equations of state, when
dealing with an evolved star.

Based on Eq. (4.23) in the notes, the number density of free electrons :math:`n_e` in the core is 

.. math::
   n_e = \frac{\rho}{\mu_e \, m_u}
   :label: e1
   
where :math:`\mu_e` is the mean molecular weight for electrons (you can refer to :ref:`exercise01`), and
assuming full ionization, it is defined as

.. math::
   \mu_e^{-1} = \sum_i^N\, \frac{X_i\, Z_i}{A_i},
   :label: e2

all other parameters have their usual meaning.
Using Eq. (4.18) in the notes, the Fermi momentum :math:`p_F` is related to the number density of free electrons by

.. math::
   p_F = \left( \frac{3}{8\pi} n_e \right)^{1/3}\, h,
   :label: e3
   
Therefore, you can use Eq. (4.24) in the notes to calculate the dimensionless momentum :math:`x` 

.. math::
   x = \frac{p_F}{m_e\, c}
   :label: e4
   
In the non-relativistic extreme :math:`x<<1`, the pressure supplied by the free electrons is 

.. math::
   P_e = \frac{8\pi m_e^4 c^5}{15\, h^3}\,x^5
   :label: e5
   
and for the relativistic extreme :math:`x>>1`, the electron degeneracy pressure is

.. math::
   P_e =  \frac{2\pi m_e^4 c^5}{3\, h^3}\,x^4
   :label: e6

However, in your model, the exact electron degeneracy pressure :math:`P_e` is somewhere between these two extremes,
and is given by the integral in Eq. (4.21) of the notes

.. math::
   P_e &=&  \frac{8\pi m_e^4 c^5}{3\, h^3}\, \int_0^x \, \frac{\xi^4}{(1+\xi^2)^{1/2}}\, d\xi, \\
       &=& \frac{8\pi m_e^4 c^5}{3\, h^3}\, g(x),
   :label: e7
   
where :math:`g(x)` can be expanded and solved analytically;
you may try to solve it using the `Wolfram Integral Calculator <http://www.wolframalpha.com/calculators/integral-calculator/>`_ 

.. math::
   g(x) = \frac{1}{8} \left[ x(2x^2-3)\sqrt{1+x^2} + 3 {\rm sinh}^{-1} x \right].
   
.. _Instructions:

Instruction
===========
You can use the :file:`/exercises/exercise08/work` directory to compile :file:`./mk` and run :file:`./rn` a given MESA model.
The provided inlist will read from an already prepared input file :file:`pre_zahb.mod`, and takes only 10 steps
to finish.
This model corresponds to a :math:`{\rm 1M}_\odot` red giant star with initial metalicity :math:`Z=0.020`, just
before the helium flash in the core;
thus, it is a rough representation of the future Sun.
Then, you can find the relevant profile :file:`LOGS/core_he_flash.prof` to start working with.
You will need to use **logT** and **logRho** to answer the questions below.
The hydrogen, helium and metal abundances are stored in **x**, **y** and **z** columns, respectively.

.. image:: Rho-T.png
   :width: 75%
   
The :math:`\log T` versus :math:`\log\rho` diagram is a very suitable representation of the physical condition across the 
whole model.
Such a diagram is shown above.
The required minimum condition for buring (:math:`\epsilon_{\rm nuc} \geq 100` erg/g/sec) 
of hydrogen, helium, carbon and oxygen anywhere inside the star (either in the core or in a shell) are shown 
with dashed lines.
Once such minimal conditions are fulfiled anywhere inside the star, the burning takes place.

The definite integral in Eq. :eq:`e7` is sometimes abstracted as :math:`g(x)`, and can have small to quite large values based
on the given :math:`x`.
The figure below shows the *logarithmic* growing behavior of :math:`g(x)`.
The included source file below evaluates :math:`g(x)` by numerical integration, and direct evaluation.

.. plot:: exercise08/g_x.py

.. image:: g_x.png
   :width: 75%

.. _Task:

Task
====

#. Create a diagram with the density :math:`\log \rho` on the x-axis and temperature :math:`\log T` on the y-axis.
   Locate the surface, and the core.
   What are the coordinates for the surface and the core in this diagram?

#. This model is just about to start burning He from outside the core, in an event called Helium flash. 
   Where in the star is favorable for He flash? What is the temperature and density of this point?
   What is the mass and radius coordinate of this point?
   Discuss the physical condition for the matter inside this location?
   
#. Follow Eqs. :eq:`e1` to :eq:`e7`, and answer these questions:
   
   * What is :math:`\mu_e` in the core and at the surface?
   
   * Calculate :math:`n_e` in the core.
   
   * Calculate the Fermi momentum :math:`p_F` and the dimensionless momentum :math:`x` in the core.
   
   * What is your estimate of the core pressure if the electrons are degenerate and non-relativistic?
     What if they are degenerate and relativistic?
     What is the central pressure from your MESA output file?
     Compare these numbers, and discuss.
     
   * Use Eq. :eq:`e7`, and calculate the exact electron degeneracy pressure in the core.
     Explain how you carried out the integral.
     How does it compare with MESA output?

#. Assume ideal gas law for the nuclei in the core, and calculate the corresponding gas pressure :math:`P_g`.
   Compare and discuss the gas pressure and electron degeneracy pressure in the core and surface. 
   In one diagram, plot the particle distribution function for the core and surface of the star, similar to Fig. 4.3 
   in the notes. 




