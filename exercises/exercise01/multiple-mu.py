
import sys, os, glob
import numpy as np 
from ivs_sse import read, plot_profile
import pylab as plt 

profile_files = glob.glob('work/LOGS/*.prof')
n_prof        = len(profile_files)
print ' - Found {0} profile files'.format(n_prof)

prof_dics     = read.read_multiple_mesa_files(profile_files, is_prof=True)

plot_profile.compare_column(list_dic_prof=prof_dics, xaxis='mass', yaxis='mu', 
                   list_lbls=None, xaxis_from=0, xaxis_to=1.1, yaxis_from=0, yaxis_to=2,
                   xtitle=r'Enclosed mass ($q=m_r/M_\odot$)', ytitle='Mean molecular weight', 
                   list_dic_annot=None, leg_loc=1, file_out='multiple-mu.png')