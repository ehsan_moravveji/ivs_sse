.. _exercise01:

**********************************
Exercise 01: Mean Molecular Weight
**********************************

.. _Introduction:

Introduction
========================

From Eq. (2.29) in the notes, the mean molecular weight :math:`\mu` is defined as

.. math::
   \frac{1}{\mu} = \sum_i \frac{X_i(1+\zeta_i)}{A_i},
   :label: e1

where :math:`X_i` is the mass fraction of element :math:`i`, and :math:`A_i` is its corresponding 
atomic number.
:math:`\zeta_i` is the effective number of electrons that each element :math:`i` contributes to the
environment; thus, it represents the degree of ionization of :math:`i`. 
As an example, in a fully neutral environment, :math:`\zeta_i=0`, while in a fully ionized 
environment, the element :math:`i` contributes :math:`\zeta_i=Z_i` electrons, where 
:math:`Z_i` is the number of protons.
In the intermediat situations, :math:`0\leq\zeta_i\leq Z_i`.
In MESA, :math:`i` depends on the burning network that one chooses, 
and ranges from few elements (the basic burning net) to very large number of species
(advanced nets). 
You will see a log on your terminal in the beginning of your MESA run, telling you which elements are being used
in the buring net, hence :math:`i`.

We already know that stars such as our Sun contain small amount of metals, i.e. :math:`Z\approx1.5\%`.
To avoid summing over small fractions of metals, the formula for :math:`\mu` is sometimes simplified 
to Eq. (2.31) in the notes:
  
.. math::
   \bar{\mu}=\left( \frac{3X}{2} + \frac{Y}{4} + \frac{1}{2} \right)^{-1}.
   :label: e2

In this exercise, we would like to test the validity of the approximation in Eq. :eq:`e2`, and assess how far it holds,
and in which regions in the star it can depart from the exact definition in Eq. :eq:`e1`.
For this reason, you will calculate a model of a e.g. 1M :math:`\!_\odot` star, and stop when the center hydrogen
mass fraction :math:`X_c` reaches :math:`\sim33\%`. 
This is a very rough model of the current Sun.

.. _Instructions:

Instructions
============

In your terminal, change your directory to :file:`exercise01/work`, then compile and run the executable::

    $> cd exercise01/work
    $> ./mk
    $> ./rn

Make sure that the make (./mk) is successful to build the executable. If so, then by running the executable 
(./rn), you will see a lot of information poping up on your terminal window about the definition of variables, 
the species in the net, the burning nets used, etc. 
Then, the code starts evolving your model and will stop once :math:`X_c=0.33`. Check your 
:file:`LOGS` directory, since the :file:`history.data` file and the :file:`profile*.prof` files are stored there.
You need to use the last :file:`.prof` file.::

    $> ls LOGS
    history.data    last-profile.prof pgstar.dat

In this case, the last profile file to use is :file:`last-profile.prof`
Now, you're ready for a visualization. See below.

.. _Visualization:

Visualization
=============

We can optionally use few of the Python tools available in the 
`ivs_sse <https://ehsan_moravveji@bitbucket.org/ehsan_moravveji/ivs_sse.git>`_
repository to create a simple plot of the **mu** column in the 
:file:`last-profile.prof` file. 
This column is calculated using Eq. :eq:`e1`.
For that, open and read :file:`mu.py` file.
Below, you can also get the python source code which is a simple plotting script.
The output plot will look something similar to the following figure.
 
.. image:: mu.png
   :width: 75%
  
.. _Task:

Task
====

Your task now is (to use the *optional* script :file:`/exercise01/mu.py` which is provided above, and) to answer the following questions: 

#. Consider three models, one at ZAMS :math:`X_c=0.65`, one in the middle of 
   the main sequence :math:`X_c=0.33`, and one close to the TAMS :math:`X_c=0.01`.
   
   Use pen and paper, and estimate :math:`\mu_{\rm approx}` in the core of your three models based on Eq. :eq:`e2`.
   Plot several :math:`\mu` profiles in a single diagram versus mass, and compare them. 
   The file :file:`multiple-mu.py` gives a simple example. 

   Explain the change in :math:`\mu`.

#. Now, choose only one profile output file.
   Plot the exact :math:`\mu` profile based on Eq. :eq:`e1`, and the approximate :math:`\bar{\mu}` based on Eq. :eq:`e2`. 
   For the latter, you need to use the **x** and **y** columns from the profile data. 
   Place the approximate plot beside the previous one, and give it a different linestyle and color. 
   Then, compare the two profiles, and check whether or not the exact and approximate :math:`\mu` profiles are identical.

#. Calculate the relative difference between the two :math:`\mu` profiles by defining the following :math:`\Delta` function:

   .. math:: 
    \Delta = \frac{|\mu_{\rm exact} \, - \, \mu_{\rm approx}|}{\mu_{\rm exact}}
    :label: e3
   
   Based on the variation of :math:`\Delta` versus radius, where in the star the difference is more pronounced? 
   Close to the surface, or near the core?

#. As explained above, the number of ions used in MESA, hence :math:`i` in Eq. :eq:`e1`, depends on the choice 
   of burning net used. 
   Open the file :file:`inlist_exercise`, and change the choice of barning net from 'basic.net' to 
   'pp_and_cno_extras.net'.
   Does :math:`\Delta` change? Why?

