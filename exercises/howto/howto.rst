.. _howto:

**************************
How to Prepare Lab Reports
**************************

.. _intro:

Why writing a report?
=====================

For the grading of your SSE Lab, you need to solve the assigned exercise(s),
and hand in a written report. I will read your report thoroughly, and score them,
based on the fact how well you solved your problem. During your final exam, you
will be asked oral questions based on your written lab reports.

Report Format
=============

To fascilitate a homogeneous scoring of the reports, it is optimal if every one 
uses identical report style, as given below. However, you are totally free to prepare
your report in whatever format that suits you; what is offered here is just suggestive
to make your life easier too.

I have prepared a LaTeX stylefile that you may download and start using. You can fetch
it from the following link:
:file:`SSE-report.zip`

I appreciate if all your reports do not exceed 4 to 5 pages, and has your full name and
your student number on the header.

