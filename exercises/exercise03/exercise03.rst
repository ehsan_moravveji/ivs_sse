.. _exercise03:

***********************************************
Exercise03: Stars as Mixture of Gas and Photons
***********************************************

.. _Introduction:

Introduction 
============
Based on Eq. (2.57) in the notes, the equation of state for an *ideal* mixture of gas and 
radiation has two components, namely :math:`P_{\rm gas}` and :math:`P_{\rm rad}`, respectively.

.. math::
   P_{\rm ideal} \equiv P_{\rm tot} = \frac{\mathcal{R}}{\bar{\mu}}\rho T \, + \, \frac{a}{3}T^4,
   :label: p01

However, we know that this assumption does not hold over the entire stellar interior. 
We expect that closer to the surface --- where the partial ionization layers of specific species lie ---
we observe significant departures from Eq. :eq:`p01`.
The idea behind this exercise is to understand the deviations from ideal gas law.

Let's assume the thermal and mechanical equilibrium in 1D stellar structure models to be *local*;
this means that the physical property of each layer in the model depends only and only on the physical condition
in the same cell, not on the neighboring cells. 
Therefore, we can additionally examine Eq. :eq:`p01` *locally* in the core and at the surface of the model, during
the evolution from ZAMS up to the TAMS.
Indeed, we are aware that the treatment of the surface conditions in MESA (atmosphere) is quite simplistic.

Eq. :eq:`p01` introduces the pressure of an *ideal* gas.
However, the physics of the equation of state is more complicated than what is given in 
Eq. :eq:`p01`. 
MESA incoroprates additional physics when computing the pressure of stellar plasma.
Therefore, we expect that what MESA computes as pressure deviate from the simplified picture
of ideal gas law.
from the ideal case.
Let's designate the pressure output from MESA by :math:`P_{\rm MESA}`.
In the following, we will compare :math:`P_{\rm ideal}` with :math:`P_{\rm MESA}`.

Another commonly used definition is the contribution of the gas pressure to the total pressure, which is historically
designated by :math:`\beta`, and is:
 
.. math::
   \beta = \frac{P_{\rm gas}}{P_{\rm tot}},
   :label: p02
   
In low mass stars, the nuclear reaction rates are modest, and the gas pressure dominates in Eq. :eq:`p01`, thus :math:`\beta\approx1`.
However, moving towards the massive (OB-type) stars, the energy production from nuclear reactions become more powerful, because the CNO network dominates over the pp chain.
This leads to the development of a strong radiation filed inside the massive stars.
Therefore, by increasing the initial mass, the pressure associated to radiation becomes progressively more and more important;
as a result, for massive stars :math:`\beta < 1`.

.. _Instructions:

Instructions
============

Like usual, compile and run the executable from your :file:`work` directory::

    cd exercise03/work
    ./mk
    ./rn

.. _Task:

Task 
====

You are expected to examine Eq. :eq:`p01` for a low-mass star and a high-mass star.
The initial masses for this exercise are already assigned to you.

#. The global picture: 
   Check if Eq. :eq:`p01` holds across the whole interior in a 
   single snapshot of the two stellar models.
   For that, you only need one MESA profile file per each mass.
   Thus, take each of the initial masses, and evolve your model until :math:`X_c=0.60`.

   Plot :math:`\beta` across your model, and explain its value.
   As usual, define a quantity :math:`\delta` that measures the *relative* difference 
   between :math:`P_{\rm ideal}` and :math:`P_{\rm MESA}`.
   Discuss what you see.

   
#. The evolutionary picture: 
   This time, only evolve the high-mass model, and use the MESA outputs to answer
   this question in detail:
   *How well does the ideal gas law hold in the core of your model?* 
   Similarly, *how well does the ideal gas law hold at the surface of your model?*
   The answer to those questions allow you to address:
   *In which region of the star can we assume the ideal gas law holds?*

   To answer the above questions, only evolve the high-mass model, from ZAMS 
   (:math:`X_c\approx71\%`) until TAMS (:math:`X_c\approx1\%`).
   A useful quantity to plot is :math:`\delta` versus time or :math:`X_{\rm c}`.
   For this purpose, you need to use several columns in your :file:`history.data` file, 
   e.g. ***log_center_T**.
   Search, and see what can be useful there?

Note: The gas constant :math:`\mathcal{R}=0.8314472471220216\times10^8 
\,[{\rm erg K}^{-1} {\rm mol}^{-1}]`, and the radiation density constant 
:math:`a=0.75646\times10^{-14} \,[{\rm erg cm}^{-3} {\rm K}^{-4}]` 
that are internally used in MESA will be printed on your screen everytime the run starts 
and finishes.
Make sure you use them properly in your script.

