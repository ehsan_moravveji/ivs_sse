.. ivs_sse_exercises documentation master file, created by
   sphinx-quickstart on Tue Aug 26 18:25:28 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Stellar Structure and Evolution Lab
===================================

The following set of exercises are adapted to the stellar structure and evolution
course (`SSE`_) at the `Institute of Astronomy <http://fys.kuleuven.be/ster>`_, KU Leuven. 
The SSE Lab is designed and given by 
`Dr. Ehsan Moravveji <http://fys.kuleuven.be/ster/staff/junior-staff/dr.-ehsan-moravveji>`_.
All exercises are adapted to the MESA stellar structure and evolution code.
Before starting, you may need to refer to the three MESA instrument papers, and its
official `MESA star <http://mesa.sourceforge.net>`_ website.

* Paxton B. et al., 2011, ApJS, 192, 3 `Paper 1`_
* Paxton B. et al., 2013, ApJS, 208, 4 `Paper 2`_
* Paxton B. et al., 2015, ApJS, 220, 15 `Paper 3`_

.. _SSE: https://onderwijsaanbod.kuleuven.be/syllabi/e/G0Y54AE.htm#activetab=doelstellingen_idm13283632
.. _Paper 1: http://adsabs.harvard.edu/abs/2011ApJS..192....3P
.. _Paper 2: http://adsabs.harvard.edu/abs/2013ApJS..208....4P
.. _Paper 3: http://adsabs.harvard.edu/abs/2015ApJS..220...15P

The exercise materials in addition to a support Python visualization package is put together
as a repository called **ivs_sse**.
The repository is open-source, and available for download via Bitbucket at the following
URL: `ivs_sse <https://bitbucket.org/ehsan_moravveji/ivs_sse>`_.


.. _Contents:

The following list of exercises do not follow any logical order; 
also, they do not essentially follow the chapter order of the lecture notes!
They are listed and numbered below in the order the idea appeared to me.

Contents:
=========

.. toctree::
   :maxdepth: 2

   exercise01/exercise01.rst
   exercise02/exercise02.rst
   exercise03/exercise03.rst
   exercise04/exercise04.rst
   exercise05/exercise05.rst
   exercise06/exercise06.rst
   exercise07/exercise07.rst
   exercise08/exercise08.rst
   exercise09/exercise09.rst
   exercise10/exercise10.rst

Hints:
======

* The current exercises and the supplied :file:`work` directories in each of them are compatible with
  official MESA release **6794** (which was the latest during the development time of the exercises). 
  For more information, read the `official release notes <http://sourceforge.net/p/mesa/mailman/message/32629541/>`_.
  It is not possible to compile and run with the latest (or perhaps older) MESA versions.

* The :file:`exerciseXX/work/inlist_exercise` file that is available for every exercise XX sets 
  :file:`pgstar_flag = .true.`, which is only useful if you have **X11** enabled on your work station.
  If it is not so, set :file:`pgstar_flag = .false.`. In case you are acessing the IvS computer cluster remotely,
  make sure you ssh with **-XY** flags included.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

