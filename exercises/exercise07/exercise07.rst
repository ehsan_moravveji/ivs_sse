
.. _exercise07:

************************************************
Exercise 07: Simple Stellar Models with Polytropes
************************************************

.. _Introduction:

Introduction 
============
When the physics of stars was advancing more with pen and paper, and the computation power was almost zero, 
the polytropic models provided pretty good estimates of the density and pressure stratification in stars.
Polytropic equation of state is introduced in Chapter 4 of the notes. 
Briefly, we assume that pressure is only determined by knowing the density structure as


.. math::
   P = K \, \rho^{1+1/n},
   :label: p1

where :math:`n \geq 0` is the polytropic index. 
Indeed, Eq. :eq:`p1` also applies to the center of the star; 
therefore, one can calculate the constant :math:`K` knowing center pressure and density.

.. math::
   K = \frac{P_c}{\rho_c^{1+1/n}},
   :label: p2

Then, you need to solve the dimensionless Lane-Emden equation which has the following differential form, and 
is an *initial value* problem.

.. math::
   \frac{d^2\theta}{d\xi^2} + \frac{2}{\xi}\frac{d\theta}{d\xi} + \theta^n\,=\,0.
   :label: p3

where :math:`\xi` is a dimensionless radius, and :math:`\theta(\xi)` gives the normalized density structure.
The initial values are applied at the center of the model :math:`\xi=0`, where :math:`\theta(0)=1` and 
:math:`d\theta/d\xi=0`.

.. math:: 
   \rho = \rho_c \, \theta(\xi)^n,

.. math::
   r = \xi / A = \xi \left[ \frac{(n+1)K}{4\pi G} \right]^{1/2} \, . \, \rho_c^{(1-n)/2n}, 
   :label: p4

Once you know the proportionality constant :math:`K` from Eq. :eq:`p2`, then you can also solve for the gravitational potential in the 
center of the model (see Eq. 4.4 in the notes).
Knowing :math:`n, \, K` and :math:`\rho_c`, you can also evaluate the actual radius of the model based on the 
conversions in Eq. :eq:`p4`.
It is then straightforward to calculate the mass of the polytrope (which can only be bound for :math:`n<5`)

.. math::
   m(r) = 4\pi\, \rho_c \, r^3 \left( -\frac{1}{\xi}\frac{d\theta}{d\xi} \right).


The idea of this exercise is to calculate several models at different evolutionary phases with MESA, and see if a polytropic
model (with an a priori unkonwn index :math:`n`) can be found as a rough match.
We will do this for a pre-main sequence model, a main sequence model and a white dwarf.

.. _Instructions:

Instructions
============

The following script solves Eq. :eq:`p3` for a range of polytropic indices :math:`0\leq n \leq 4.5`.
You may already use it to solve Eq. :eq:`p3` for any desired index.
This script also stores the solution as ascii files, and stores them locally in a directory called 
:file:`output`. 
Each output file has three columns, being the radius :math:`\xi`, density :math:`\theta` and its derivative
:math:`d\theta/d\xi`.
The following plot shows the density distribution (top) for different :math:`n`, and their corresponding 
derivative (bottom).

.. image:: 
   Lane-Emden.png
   :width: 70%

You have to calculate three evolutionary models for 
(a) fully convective pre-main sequence (PreMS) model, 
(b) a main sequence (MS) model of the Sun, and
(c) a white dwarf (WD). 
For that, you only need to specify the correct inlist to use in your :file:`work` directory.
E.g. :file:`inlist_preMS` is for pre-main sequence model.
Once your MESA evolution stops, you can grab the last saved profile, and fulfil the tasks below.

.. _Task:

Task 
====
#. Grab the included Python script that solves for the Lane-Emden equation, and execute it.
   It will store polytropic density profiles and their derivatives in your local :file:`output` directory.
   The filenames have the :math:`n` index appended.

#. Calculate the three MESA models for the three phases given above.
   For each model, do the following:

   * Calculate the density proportionality constant :math:`K` based on Eq. :eq:`p2`

   * Calculate the radius proportionality constant :math:`A` according to Eq. :eq:`p4`

   * Plot :math:`\rho/\rho_c` and :math:`\theta^n(\xi)` versus :math:`\xi` for different polytropic indices :math:`n`.
     Which index is the best representation of your MESA model?

   * What is the radius of your MESA model, compared to that of your best polytropic model?

   * What is the mass of your MESA model, compared to that of your best polytropic model?

   * The first adiabatic exponent is introduced by `S. Chandrasekhar <http://en.wikipedia.org/wiki/Subrahmanyan_Chandrasekhar>`_
     to be :math:`\Gamma_1=(\partial\ln P/\partial\ln\rho)_{\rm S}`.
     What is a rough estimate of :math:`\Gamma_1` for your PreMS, MS and WD models?


