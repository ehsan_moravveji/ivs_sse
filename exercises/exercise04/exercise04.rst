.. _exercise04:

*******************************
Exercise04: Continuity Equation
*******************************

.. _Introduction:

Introduction
============
The first basic equation that governs the structure of a star is the *continuity of mass*.
See Eq. (3.16).
It implies that during the hydrostatic equilibrium phase (:ref:`exercise05`), the mass distribution inside
a star in the Lagrangian frame obeys the following relation

.. math::
   \frac{\partial r}{\partial m} = \frac{1}{4\pi \, r^2 \, \rho(r)},
   :label: c1

When MESA evolves a star, at each snapshot (i.e. constant time), the model must fulfil the condition of continuity,
hence Eq. :eq:`c1` must hold. 
Actually, MESA solves Eq. :eq:`c1`, slightly differently. 
Based on Eq. (5) in the first MESA instrument paper `Paxton et al. (2011) <http://adsabs.harvard.edu/abs/2011ApJS..192....3P>`_
an alternative of Eq. :eq:`c1` is internally solved 

.. math::
   \ln r_k = \frac{1}{3} \ln\left[r_{k+1}^3 + \frac{3}{4\pi}\frac{dm_k}{\rho_k}\right].
   :label: c3

We would like to check how perfectly the equality between the left-hand-side (LHS) and the right-hand-side (RHS) in Eq. :eq:`c3`
holds at a single snapshot of a life of a model star.

.. _Intensive_Extensive_Variables:

Intensive versus Extensive Variables in MESA
============================================

In MESA, there are two different quantities:

* Intensive quantities like temperature :math:`T`, density :math:`\rho`, pressure :math:`P`, etc. 
  By doubling the volume, the intensive quantities do *not* double.
  These quantities are always defined at the center of a cell, and are called ``cell-averages``.
  In the Lagrangian (Eulerian) frame, the center of the zone is defined by mass (radius);
  MESA solves the Lagrangian form of SSE equations.

* Extensive quantities like radius :math:`r`, mass :math:`m`, etc.
  By doubling the volume, the extensive quantities double.
  These quantities are always defined at cell boundaries.

*Question:* How to deal with intensive and extensive variables?
For consistency, one must always be careful about treating the variables in MESA either at cell boundaries, or
at cell centers (by mass).
This means that for those relations that are purely thermodynamical like :math:`c_P-c_V` as in :ref:`exercise02`, 
all ingredients are cell averages, and we can compare apples with apples.
However, those relations that combine intensive and extensive variables (and their derivatives) have to be handled 
with care.
Eq. :eq:`c1` is a perfect example of such a case.
In this case, one has to either shift the intensive quantities (that are originally at cell centers) to the cell edge, 
or alternatively shift the extensive quantities (that are originally at cell edges) to the cell centers.
We advise the former, and practice it here in this exercise.

The proper treatment of physical quantities in MESA are thoroughly addressed in Sec. 6.2 of the MESA first instrument paper
`Paxton et al. (2011) <http://adsabs.harvard.edu/abs/2011ApJS..192....3P>`_ and the accompanying Fig. 9.
I strongly advice you to study that section.


.. _cell_center_to_cell_edge:

Transfering Intensive Quantities to the Cell Boundary
=====================================================

This is easily done by using the mass of each cell with respect to the total mass, 
i.e. :math:`dq_k=dm_k/M_\star`, where :math:`k` is the cell index, :math:`dm_k` is 
the mass increment at the center of cell :math:`k`, and :math:`M_\star` is the total 
mass of the star. Let us designate e.g. the pressure at cell center by 
:math:`P^{\rm center}` and the pressure at cell edge by :math:`P^{\rm edge}`.
Then, the transformation from :math:`P^{\rm center}` to :math:`P^{\rm edge}` is done 
using an interpolation in mass, by the :math:`\alpha` ratio:

.. math::
   \alpha_k &= \frac{dq_{k-1}}{dq_{k-1}+dq_k}, \\
   P^{\rm edge}_k &= \alpha_k \, P^{\rm center}_k + (1-\alpha_k) \, P^{\rm center}_{k-1}.
   :label: c2

Note that for the topmost layer :math:`i=1`, :math:`\alpha_1=1`.
The same transformation algorithm as in :eq:`c2` can be applied to translate the density 
from cell center to the cell edge, and use it in Eq. :eq:`c1` or :eq:`c3`.

Since MESA solves the SSE equations in Lagrangian frame, the mass increment :math:`dm` 
has to be handled with care too. Let me briefly discuss it here.
From Fig. 9 in `Paxton et al. (2011) <http://adsabs.harvard.edu/abs/2011ApJS..192....3P>`_, 
mass is defined at cell boundary. Therefore, the mass increment :math:`dm_k` which is the 
mass of each individual cell is defined in the center of the cell as:

.. math::
   dm_k = m_{k} - m_{k+1},

Note that :math:`dm_{nz}=0`.
However, to  use with other intensive quantities, :math:`dm_k` should be translated to the 
boundary of each cell, so I designate it with an overbar :math:`\overline{dm}_k`.
The :math:`\overline{dm}_k` is essentially the mean of two consecutive :math:`dm_k`:

.. math::
   \overline{dm}_1    &=& \frac{dm_1}{2}, \quad {\rm at \, surface: for \, }\, k=1, \\
   \overline{dm}_k    &=& \frac{dm_k + dm_{k-1}}{2}, \quad {\rm elsewhere \,}\, k\neq1 \\


.. _Task:

Task
============

Take a :math:`5{\rm M}_\odot` main sequence star, at an arbitrary age.
Compare the term :math:`\ln r_k` on the left-hand side (LHS) of Eq. :eq:`c3` with the same quantity calculated by MESA, and 
stored in the profile snapshot.
As a measure of the quality of the equality in Eq. :eq:`c3`, define a dimensionless quantity :math:`\Delta`, and compare the 
relative difference of the RHS and LHS of Eq. :eq:`c3`.

.. math::
   \Delta = \left|\frac{{\rm RHS} - {\rm LHS}}{LHS}\right|.
   
Plot :math:`\log_{10}\Delta` as a function of mass and/or radius in your model.
Discuss how well Eq. :eq:`c3` is internally solved in MESA.
How do you compare :math:`\Delta` with the *double precision* machine accuracy?

Tips: 

#. When fiddling around Eq. :eq:`c3`, make sure you use every parameter is in cgs units.

#. There are few controls that can be found in :file:`/mesa/star/public/controls.default` to control the precision level the 
   solver has to fulfil when solving each of the structure equations. 
   You may consult the full list of possibilities there.
   For example, the following defaults can be reduced in favour of better numerical precision.::
   
    tol_correction_norm = 3d-5
    tol_max_correction = 3d-3

At the end, you must come up with more and less the following diagram for :math:`\log_{10}\Delta` inside your model.

.. image:: Continuity.png
   :width: 75%
   
