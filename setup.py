"""
ivs_sse is the Stellar Structure and Evolution course taught at IvS, hence the name.
This script installs the ivs_sse package which is a smally python repository for some plotting tools.

Example usage: without pip:

$:> python setup.py build
$:> sudo python setup.py install

With pip:

$:> python setup.py sdist
$:> sudo pip install dist/ivs_sse_0.0.tar.gz

And with pip you can uninstall:

$:> sudo pip uninstall ivs_sse

On *buntu systems, the installation directory is

/usr/local/lib/python2.7/dist-packages/ivs_sse

"""


from numpy.distutils.core import setup, Extension
import glob
import sys
from numpy.distutils.command.build import build as _build    
      
setup(
    
    name="ivs_sse",
    version="0.0",
    description="Tools to Use/Interpret the outputs of MESA",
    long_description="reStructuredText format",
    author="Ehsan Moravveji",
    author_email="Ehsan.Moravveji@ster.kuleuven.be",
    url="https://ehsan_moravveji@bitbucket.org/ehsan_moravveji/ivs_sse.git",
    packages=['ivs_sse'],
    
    # entry_points = {
    #     "console_scripts": ["ascii2h5 = mesa_gyre.ascii2h5:main"]}

)

