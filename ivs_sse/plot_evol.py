
"""
This module plots the evolution of various quantities either from multiple history files,
or from multiple profile files.
"""
import sys
import read, plot_profile
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import itertools
from collections import OrderedDict
from ivs.aux.numpy_ext import recarr_join, recarr_addcols

Msun = 1.9892e33    # g
Rsun = 6.9598e10    # cm
G    = 6.67428e-8   # cm^3 g^-1 s^-2

#=================================================================================================================
def Kippenhahn(dic_hist, list_dic, xaxis='center_h1', mass_from=None, mass_to=None, file_out=None):
  """
  Plot the Kippenhahn diagram based on the list of MESA profile
  @param dic_hist: dictionary with the MESA history information of the track. This is normally returned by read.read_h5()
  @type dic_hist: dictionary
  @param list_dic: list of dictionaries containing the profile information from MESA
  @type list_dic: list of dictionaries
  @param xaxis: The evolution is expressed in terms of this variable, valid variables could be 'center_h1', 'model_number'
     or 'star_age'
  @type xaxis: string
  @param file_out: full path to the output pdf plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic)
  if file_out is None:
    message = 'Error: plot_evol: Kippenhahn: output plot file is not specified'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.11, right=0.98, bottom=0.11, top=0.974, hspace=0.02, wspace=0.02)

  #--------------------------------
  # Fiddle around with the input fields
  #--------------------------------
  hist        = dic_hist['hist']
  filename    = dic_hist['filename']
  # dic_params  = param_tools.get_param_from_single_hist_filename(filename)
  hist_wanted = ['conv_mx1_bot', 'conv_mx1_top', 'conv_mx2_bot', 'conv_mx2_top',
                 'epsnuc_M_1', 'epsnuc_M_2', 'epsnuc_M_3', 'epsnuc_M_4', 'epsnuc_M_5',
                 'epsnuc_M_6', 'epsnuc_M_7', 'epsnuc_M_8', 'mass_conv_core', 'model_number',
                 'mx1_bot', 'mx1_top', 'mx2_bot', 'mx2_top', 'star_age', 'star_mass',
                 'Teff', 'log_Teff', 'log_g']
  hist_names  = {}
  for name in hist.dtype.names: hist_names[name] = 0.0
  hist_fields = read.check_key_exists(hist_names, hist_wanted)
  hist_model_number= hist['model_number']
  n_hist          = len(hist_model_number)
  hist_age        = hist['star_age']/1e6  # in Myr
  hist_mass       = hist['star_mass']
  hist_Mcc        = hist['mass_conv_core']
  hist_Xc         = hist['center_h1']
  hist_Yc         = hist['center_he4']
  if hist_fields['conv_mx1_bot']:
    conv_mx1_bot  = hist['conv_mx1_bot'] * hist_mass
    conv_mx1_top  = hist['conv_mx1_top'] * hist_mass
    conv_mx2_bot  = hist['conv_mx2_bot'] * hist_mass
    conv_mx2_top  = hist['conv_mx2_top'] * hist_mass
  # Fix conv_mx... for unwanted switches between larger and smaller zone
  conv_up_top     = np.zeros(n_hist)
  conv_up_bot     = np.zeros(n_hist)
  conv_lo_top     = np.zeros(n_hist)
  conv_lo_bot     = np.zeros(n_hist)
  for i in range(n_hist):
    conv_up_top[i]= np.max([conv_mx1_top[i], conv_mx2_top[i]])
    conv_up_bot[i]= np.max([conv_mx1_bot[i], conv_mx2_bot[i]])
    #conv_lo_top[i]= np.min([np.min([hist_Mcc[i], conv_mx1_top[i]]), conv_mx2_top[i]])
    conv_lo_top[i]= np.min([conv_mx1_top[i], conv_mx2_top[i]])
    conv_lo_bot[i]= np.min([conv_mx1_bot[i], conv_mx2_bot[i]])

  if hist_fields['epsnuc_M_1']:
    epsnuc_M_1    = hist['epsnuc_M_1'] / Msun
    epsnuc_M_2    = hist['epsnuc_M_2'] / Msun
    epsnuc_M_3    = hist['epsnuc_M_3'] / Msun
    epsnuc_M_4    = hist['epsnuc_M_4'] / Msun
    epsnuc_M_5    = hist['epsnuc_M_5'] / Msun
    epsnuc_M_6    = hist['epsnuc_M_6'] / Msun
    epsnuc_M_7    = hist['epsnuc_M_7'] / Msun
    epsnuc_M_8    = hist['epsnuc_M_8'] / Msun

  if hist_fields['Teff']: log_Teff = np.log10(hist['Teff'])
  if hist_fields['log_Teff']: log_Teff = hist['log_Teff']
  if hist_fields['log_g']:
    log_g = hist['log_g']
  else:
    raise SystemExit, 'Kippenhahn: log_g not in hist; implement mxg'
  mxg             = np.argmax(log_g)
  mxg_Xc          = hist_Xc[mxg]
  mxg_age         = hist_age[mxg]
  mxg_mn= hist_model_number[mxg]

  if xaxis == 'center_h1': x_hist = hist_Xc
  if xaxis == 'model_number': x_hist = hist_model_number
  if xaxis == 'star_age': x_hist = hist_age

  # Profiles:
  a_prof      = list_dic[0]['prof']
  prof_wanted = ['mass', 'eps_nuc', 'h1', 'he4', 'mixing_type']
  prof_names  = {}
  for name in a_prof.dtype.names: prof_names[name] = 0.0
  prof_fileds = read.check_key_exists(prof_names, prof_wanted)
  #if not prof_fileds[xaxis]: raise SystemExit, 'Kippenhahn: {0} cannot be xaxis; unavailable from profiles.'.format(xaxis)
  if not prof_fileds['mass']: raise SystemExit, 'Kippenhahn: mass not in profile'
  if not prof_fileds['mixing_type']: raise SystemExit, 'Kippenhahn: mixing_type not in profile'


  #--------------------------------
  # Loop over profiles
  #--------------------------------
  ages     = []
  xvals    = []
  Xc_vals  = []
  mn_vals  = []
  for i_dic, dic in enumerate(list_dic):
    header = dic['header']
    prof   = dic['prof']

    Teff      = header['Teff']
    Xc        = header['center_h1']
    Yc        = header['center_he4']
    M_ini     = header['initial_mass']
    Z_ini     = header['initial_z']
    prof_model_number = header['model_number']
    nz        = header['num_zones']
    Lstar     = header['photosphere_L']  # in Lsun
    Rstar     = header['photosphere_r']  # in Rsun
    L_H       = header['power_h_burn']   # in Lsun
    L_He      = header['power_he_burn']  # in Lsun
    L_neu     = header['power_neu']      # in Lsun
    L_nuc     = header['power_nuc_burn'] # in Lsun
    star_age  = header['star_age']/1e6   # in Myr
    star_mass = header['star_mass']
    dt        = header['time_step']

    mass      = prof['mass']
    eps_nuc   = prof['eps_nuc']
    h1        = prof['h1']
    he4       = prof['he4']
    mixing_type = prof['mixing_type']

    no = mixing_type == 0
    cz = mixing_type == 1
    ov = mixing_type == 2
    sc = mixing_type == 3
    rot= mixing_type == 5

    ages.append(star_age)
    age_arr   = np.array( [star_age] * nz )
    #zero_arr  = np.zeros(nz)
    Xc_vals.append(Xc)
    mn_vals.append(prof_model_number)

    # Set the xaxis
    if xaxis  == 'center_h1': xvals.append(Xc)
    if xaxis  == 'star_age':  xvals.append(star_age)
    if xaxis  == 'model_number': xvals.append(prof_model_number)
    x_prof    = np.ones(nz) + xvals[-1]

    if any(rot):
      #ax.fill_between(x_prof, mass, zero_arr, where=rot, color="gray", alpha=0.01, zorder=0, label=r'Rot')
      ax.scatter(x_prof[rot], mass[rot], color='gray', marker='s', s=4, alpha=0.01, zorder=0)
    if any(cz):
      # ax.fill_between(x_prof, mass, where=cz, color='purple', alpha=0.5, zorder=1)
      ax.scatter(x_prof[cz], mass[cz], color='purple', marker='s', s=4, alpha=0.5, zorder=1)
    if any(ov):
      #ax.fill_between(x_prof, mass, zero_arr, where=ov, color='cyan', alpha=0.5, zorder=2, label=r'Overshoot')
      ax.scatter(x_prof[ov], mass[ov], color='cyan', marker='s', s=4, alpha=0.5, zorder=2)
    if any(sc):
      ax.scatter(x_prof[sc], mass[sc], color='0.10', marker='.', s=1, alpha=0.1, zorder=3)

  #--------------------------------
  # Plot Using History Data
  #--------------------------------
  # Convective zones
  if mass_to is None:
    ax.plot(x_hist, hist_mass, linestyle='solid', lw=1, color='black', zorder=1, label=r'M$_\star$')

  # ax.plot(x_hist, hist_Mcc, linestyle='dashed', lw=1.5, color='blue', zorder=1, label=r'M$_{\rm cc}$')
  #ax.fill_between(x_hist, conv_up_bot, conv_up_top, color='blue', alpha=0.01, zorder=1)
  #ax.fill_between(x_hist, conv_lo_bot, conv_lo_top, color='blue', alpha=0.25, zorder=1)

  # Burning zones
  ax.fill_between(x_hist, epsnuc_M_1, epsnuc_M_4, color='red', alpha=0.15, zorder=2, label=r'1$^{\rm st}$ Burning Zone')
  ax.fill_between(x_hist, epsnuc_M_2, epsnuc_M_3, color='red', alpha=0.25, zorder=3)
  ax.fill_between(x_hist, epsnuc_M_5, epsnuc_M_8, color='orange', alpha=0.15, zorder=2, label=r'2$^{\rm nd}$ Burning Zone')
  ax.fill_between(x_hist, epsnuc_M_6, epsnuc_M_7, color='orange', alpha=0.25, zorder=3)

  if mass_to is None:
    ax.plot(x_hist, log_Teff, linestyle='solid', lw=1, color='gray', label=r'$\log T_{\rm eff}$')

  #--------------------------------
  # Ranges, Annotations and Legend
  #--------------------------------
  if xaxis == 'star_age':
    ax.set_xlim(min([min(ages)*0.95, mxg_age]), max(ages)*1.01)
    ax.set_xlabel(r'Time [Myr]')
  if xaxis == 'center_h1':
    ax.set_xlim(max([mxg_Xc, max(Xc_vals)*1.01]), min(Xc_vals))
    ax.set_xlabel(r'Center $^1$H $X_c$')
  if xaxis == 'model_number':
    ax.set_xlim(max([mxg_mn, min(mn_vals)]), max(mn_vals))
    ax.set_xlabel(r'Model Number')
  if mass_from is None: mass_from = 0.0
  if mass_to is None: mass_to = star_mass * 1.05
  ax.set_ylim(mass_from, mass_to)
  ax.set_ylabel(r'Mass [M$_\odot$]')

  left = 0.04; top = 0.90; dy = 0.04; iy = 0
  # for key, val in dic_params.items():
  #   if key == 'filename': continue
  #   if key == 'M': key_tex = r'M$_{\rm ini}/$M$_\odot$'
  #   #if key == 'eta': key_tex = r'$\eta_{\rm rot}$'
  #   if key == 'ov': key_tex = r'$f_{\rm ov}$'
  #   if key == 'sc': key_tex = r'$\alpha_{\rm sc}$'
  #   if key == 'z': key_tex = r'Z'
  #   if False:
  #     txt = r'{0}={1}'.format(key_tex, val)
  #     print key, val, txt
  #     ax.annotate(txt, xy=(left, top-iy*dy), xycoords='axes fraction', fontsize='x-small')
  #   iy     += 1
  #txt = r'{0}={1}'.format(r'$\alpha_{\rm sc}$', dic_params['sc'])
  #ax.annotate(txt, xy=(left, 0.86), xycoords='axes fraction', fontsize='xx-large')

  # fake plots, just for legend
  ax.plot(-1-x_hist, -conv_lo_bot, color='purple', alpha=1.0, lw=6, label=r'Convective Zone')
  ax.plot(-1-x_hist, -epsnuc_M_6, color='red', alpha=0.25, zorder=3, lw=6, label=r'Burning Zone')
  # ax.plot(-x_prof[cz], -mass[cz], color='gray', lw=6, alpha=0.01, zorder=0, label=r'Rotating Zone')
  ax.plot(-x_prof[cz], -mass[cz], color='cyan', lw=6, alpha=0.5, zorder=2, label=r'Overshooting')
  # ax.plot(-x_prof[cz], -mass[cz], color='0.10', lw=6, alpha=0.1, zorder=3, label=r'Semi-Convection')
  leg = ax.legend(loc=2, fontsize='medium', framealpha=0.5)

  #--------------------------------
  # Save the plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print '\n - plot_evol: Kippenhahn: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def Kippenhahn_by_mix_qtop(dic_hist, xaxis, xaxis_from=None, xaxis_to=None, yaxis_from=None, 
                           yaxis_to=None, leg_loc=None, list_dic_annot=[], file_out=None):
  """
  It is possible to construct Kippenhahn diagram by using the extensive columns provided in MESA history 
  file that have names similar to mix_type_* and mix_qtop_*, where they are both appended with the number
  label of the zone(s).
  """
  header = dic_hist['header']
  hist   = dic_hist['hist']
  M_star = hist['star_mass']  
  M_ini  = M_star[0]
  keys   = hist.dtype.names

  #--------------------------------
  # Enumerate the zones
  #--------------------------------
  n_zones= 0
  for i, key in enumerate(keys):
    if 'mix_qtop_' in key: n_zones += 1

  if n_zones == 0:
    print ' - plot_evol: Kippenhahn_by_mix_qtop: No mixing zones found!'
    print '              Activate "mixing_regions <integer>" option in MESA history_column.list file'
    return 

  if xaxis not in keys:
    raise SystemExit, 'Error: plot_evol: Kippenhahn_by_mix_qtop: {0} not available!'.format(xaxis)

  #--------------------------------
  # Prepare the Figure
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(12, 4))
  plt.subplots_adjust(left=0.05, right=0.985, bottom=0.12, top=0.97)

  xvals   = hist[xaxis]
  lbl     = r'' + '{0:.1f}'.format(M_ini) + r' $M_\odot$'
  ax.plot(xvals, M_star / M_ini, lw=1, color='black', label=lbl)

  #--------------------------------
  # Fill between
  #--------------------------------
  for i_row, row in enumerate(hist[1:]):
    model_number= row['model_number']
    row_mx_type = np.empty(n_zones, dtype=int)
    row_mx_qtop = np.empty(n_zones, dtype=float)
    
    for i_col in range(1, n_zones+1):
      key_mx_type          = 'mix_type_{0}'.format(i_col)
      key_mx_qtop          = 'mix_qtop_{0}'.format(i_col)
      row_mx_type[i_col-1] = row[key_mx_type]
      row_mx_qtop[i_col-1] = row[key_mx_qtop] * row['star_mass'] / M_ini

    for i_mx in range(n_zones):
      mxt    = row_mx_type[i_mx]
      if mxt not in [1, 3, 4]: continue

      qtop   = row_mx_qtop[i_mx]
      if i_mx == 0:
        qbot = 0.0
      else:
        qbot = row_mx_qtop[i_mx-1]

      if mxt == 1:
        clr = 'blue'
      elif mxt == 3:
        clr = 'grey'
      elif mxt == 4:
        clr = 'green'
      else:
        clr = 'black'  # unclassified

      ax.fill_between([model_number], y1=qbot, y2=qtop, color=clr, zorder=2)

  #--------------------------------
  # Add 8 burning regions
  #--------------------------------
  M_ini_cgs = M_ini * Msun
  # ax.fill_between(x=xvals, y1=hist['epsnuc_M_1']/M_ini_cgs, y2=hist['epsnuc_M_4']/M_ini_cgs, 
  #                 where=(hist['epsnuc_M_1']>=0), color='yellow', alpha=0.5, zorder=3, label='Weak Burning')
  ax.fill_between(x=xvals, y1=hist['epsnuc_M_2']/M_ini_cgs, y2=hist['epsnuc_M_3']/M_ini_cgs, 
                  where=(hist['epsnuc_M_2']>=0), color='red', alpha=0.5, zorder=4, label='Strong Burning')

  ax.fill_between(x=xvals, y1=hist['epsnuc_M_5']/M_ini_cgs, y2=hist['epsnuc_M_8']/M_ini_cgs, 
                  where=(hist['epsnuc_M_5']>=0), color='yellow', alpha=0.5, zorder=4)
  ax.fill_between(x=xvals, y1=hist['epsnuc_M_6']/M_ini_cgs, y2=hist['epsnuc_M_7']/M_ini_cgs, 
                  where=(hist['epsnuc_M_6']>=0), color='red', alpha=0.5, zorder=4)

  #--------------------------------
  # Cosmetics
  #--------------------------------
  if xaxis_from is None: xaxis_from = min(xvals) * 0.99
  if xaxis_to is None:   xaxis_to   = max(xvals) * 1.01
  if yaxis_from is None: yaxis_from = -0.0
  if yaxis_to is None:   yaxis_to   = 1.02
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  # if list_dic_annot: plot_commons.add_annotation(ax, list_dic_annot)

  # Only for Legend purpose:
  ax.plot([], [], lw=10, color='blue', label=r'Convective')
  ax.plot([], [], lw=10, color='grey', label=r'Overshooting')
  ax.plot([], [], lw=10, color='green', label=r'Semi-Convective')

  ax.plot([], [], lw=10, color='yellow', label=r'Weak Burning')
  ax.plot([], [], lw=10, color='red', label=r'Strong Burning')

  ax.legend(bbox_to_anchor=(0.15, 0.97), fontsize=10, frameon=False, 
            fancybox=False, shadow=False, borderpad=False)
  
  xtitle = None
  if xaxis == 'model_number': xtitle = 'Model Number'
  if xtitle is not None: ax.set_xlabel(xtitle)  
  ax.set_ylabel(r'Relative Mass $\, m_r/M_\star$')

  plt.savefig(file_out, transparent=False)
  print ' - plot_evol: Kippenhahn_by_mix_qtop: saved {0}'.format(file_out)
  plt.close()

  return None

#=================================================================================================================
def plot_evol_prof_density(list_dic, xaxis, list_lbls=None, xaxis_from=None, xaxis_to=None,
                           yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=None,
                           offset=False, file_out=None):
  """
  Plot the evolution of the density from the pre-main sequence phase up to the TAMS, and specify the convective extent
  by an overlying line
  """
  n_dic = len(list_dic)
#=================================================================================================================
def plot_evol_prof(list_dic, xaxis, yaxis, list_lbls=None, xaxis_from=None, xaxis_to=None,
                   yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=None,
                   offset=False, trace_mixing_zones=False, box_from=None, box_to=None, file_out=None):
  """
  Plot the evolution of the profile of any quantity
  """
  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: plot_evol: plot_evol_prof: Input list is empty.'
    raise SystemExit, message

  dic_sample = list_dic[0]
  if 'prof' not in dic_sample.keys():
    message = 'Error: plot_evol: plot_evol_prof: Input dictionaries miss the "prof" field.'
    raise SystemExit, message
  prof_sample = dic_sample['prof']
  if not xaxis in prof_sample.dtype.names:
    message = 'Error: plot_evol: plot_evol_prof: %s field is missing in "prof" field.' % (xaxis, )
    raise SystemExit, message
  if not yaxis in prof_sample.dtype.names:
    message = 'Error: plot_evol: plot_evol_prof: %s field is missing in "prof" field.' % (yaxis, )
    raise SystemExit, message
  if trace_mixing_zones and not 'stability_type' in prof_sample.dtype.names:
    message = 'Error: plot_evol: plot_evol_prof: "stability_type" is missing from profiles.'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=160)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.11, right=0.98, bottom=0.11, top=0.98, hspace=0.02, wspace=0.02)
  if xtitle:
    ax.set_xlabel(xtitle)
  else:
    ax.set_xlabel(r''+xaxis)
  if ytitle:
    ax.set_ylabel(ytitle)
  else:
    ax.set_ylabel(r''+yaxis)

  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_dic)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)

  # collect the min/max range along x/yaxis for all profile files
  list_min_xaxis = []
  list_max_xaxis = []
  list_min_yaxis = []
  list_max_yaxis = []

  for i_dic, dic in enumerate(list_dic):
    prof = dic['prof']
    header = dic['header']
    logT   = prof['logT']
    n_mesh = len(logT)

    xaxis_arr = prof[xaxis]
    yaxis_arr = prof[yaxis]
    list_min_xaxis.append(np.min(xaxis_arr))
    list_max_xaxis.append(np.max(xaxis_arr))
    list_min_yaxis.append(np.min(yaxis_arr))
    list_max_yaxis.append(np.max(yaxis_arr))

    # specify the extent of CZ if requested
    if trace_mixing_zones:
      mixing_type = prof['mixing_type']
      no = mixing_type == 0
      cz = mixing_type == 1
      ov = mixing_type == 2
      sc = mixing_type == 3
      rot= mixing_type == 5

      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=no, color="white")
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=cz, color="0.90", alpha=0.7)
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=ov, color='blue', alpha=0.5)
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=sc, color='green', alpha=0.5)
      ax.fill_between(xaxis_arr, yaxis_arr, 0, where=rot, color='red', alpha=0.5)

      #yaxis_arr_cz = yaxis_arr[cz]
      #yaxis_arr_ov = yaxis_arr[ov]
      #yaxis_arr_sc = yaxis_arr[sc]
      #yaxis_arr_rot= yaxis_arr[rot]

    #if trace_mixing_zones:
      #mixing_type = prof['mixing_type']
      #no_mixing = np.where((mixing_type == 0))[0]
      #convective_mixing = np.where((mixing_type == 1))[0]
      #overshoot_mixing = np.where((mixing_type == 2))[0]
      #semiconvective_mixing = np.where((mixing_type == 3))[0]
      #thermo_haline_mixing = np.where((mixing_type == 4))[0]
      #rotation_mixing = np.where((mixing_type == 5))[0]
      #minimum_mixing = np.where((mixing_type == 6))[0]
      #anonymous_mixing = np.where((mixing_type == 7))[0]

      #list_dic_conv = plot_profile.identify_mixing_type_edges(convective_mixing)
      #list_dic_over = plot_profile.identify_mixing_type_edges(overshoot_mixing)
      #list_dic_semi = plot_profile.identify_mixing_type_edges(semiconvective_mixing)
      #list_dic_rot  = plot_profile.identify_mixing_type_edges(rotation_mixing)
      #list_dic_anon = plot_profile.identify_mixing_type_edges(anonymous_mixing)

      #if list_dic_conv:
        #for i_zone in range(len(list_dic_conv)):
          #if i_zone > 0: continue
          #zone_from_indx = list_dic_conv[i_zone]['from']
          #zone_to_indx   = list_dic_conv[i_zone]['to']
          #zone_from = xaxis_arr[zone_from_indx]
          #zone_to   = xaxis_arr[zone_to_indx]
          #zone_width = zone_to - zone_from
          ##rect_conv = plt.Rectangle((zone_from, 0), zone_width, n_mesh, facecolor="0.90", edgecolor=None,
                                  ##lw=None)
          ##plt.gca().add_patch(rect_conv)
          ##bbox = Bbox.from_bounds(zone_from, 0, zone_width, n_mesh)

      #if list_dic_over:
        #for i_zone in range(len(list_dic_over)):
          #zone_from_indx = list_dic_over[i_zone]['from']
          #zone_to_indx   = list_dic_over[i_zone]['to']
          #zone_from = xaxis_arr[zone_from_indx]
          #zone_to   = xaxis_arr[zone_to_indx]
          #zone_width = zone_to - zone_from
          #rect_over = plt.Rectangle((zone_from, 0), zone_width, n_mesh, facecolor="Pink")
          #plt.gca().add_patch(rect_over)
          ##bbox = Bbox.from_bounds(zone_from, 0, zone_width, n_mesh)

      #if list_dic_semi:
        #for i_zone in range(len(list_dic_semi)):
          #zone_from_indx = list_dic_semi[i_zone]['from']
          #zone_to_indx   = list_dic_semi[i_zone]['to']
          #zone_from_logT = logT[zone_from_indx]
          #zone_to_logT   = logT[zone_to_indx]
          #zone_width_logT = zone_to_logT - zone_from_logT
          #rect_semi = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="green")
          #plt.gca().add_patch(rect_semi)
          ##bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)

      #if  list_dic_rot:
        #for i_zone in range(len(list_dic_rot)):
          #zone_from_indx = list_dic_rot[i_zone]['from']
          #zone_to_indx   = list_dic_rot[i_zone]['to']
          #zone_from_logT = logT[zone_from_indx]
          #zone_to_logT   = logT[zone_to_indx]
          #zone_width_logT = zone_to_logT - zone_from_logT
          #rect_rot = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="Blue")
          #plt.gca().add_patch(rect_rot)
          ##bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)


      #if any(yaxis_arr_cz):  ax.plot(xaxis_arr[cz], yaxis_arr_cz, lw=2, color='gray')
      #if any(yaxis_arr_ov):  ax.plot(xaxis_arr[ov], yaxis_arr_ov, lw=2, color='blue')
      #if any(yaxis_arr_sc):  ax.plot(xaxis_arr[sc], yaxis_arr_sc, lw=2, color='cyan')
      #if any(yaxis_arr_rot): ax.plot(xaxis_arr[rot], yaxis_arr_rot, lw=2, color='red')

    ax.plot(xaxis_arr, yaxis_arr, lw=1, color=color.to_rgba(i_dic))

  # Fix the axis ranges
  list_min_xaxis = np.asarray(list_min_xaxis)
  list_max_xaxis = np.asarray(list_max_xaxis)
  list_min_yaxis = np.asarray(list_min_yaxis)
  list_max_yaxis = np.asarray(list_max_yaxis)
  xaxis_min = np.min(list_min_xaxis)
  xaxis_max = np.max(list_max_xaxis)
  yaxis_min = np.min(list_min_yaxis)
  yaxis_max = np.max(list_max_yaxis)

  if xaxis_from: xaxis_min = xaxis_from
  if xaxis_to:   xaxis_max = xaxis_to
  if yaxis_from: yaxis_min = yaxis_from
  if yaxis_to:   yaxis_max = yaxis_to
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)

  if not file_out:
     file_out = param.gen_path_plot() + 'Evol-Prof-' + xaxis + '-vs-' + yaxis + '.pdf'
  plt.savefig(file_out)
  print ' - Plot stored at %s' % (file_out, )
  plt.close()

  return None

#=================================================================================================================
def plot_evol_hist(list_dic_hist, xaxis, yaxis, list_lbls=None, list_list_dic_prof=None, xaxis_from=None,
                   xaxis_to=None, yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=None,
                   xlog=False, ylog=False, ref_indx=0, file_out=None):
  """
  Plot the evolution of any quantity available in the history file specified by xaxis, versus another quantity set
  by yaxis. For instance, it is possible to plot the evolution of the size in mass of the hydrogen burning core as
  a function of time/timestep/center_h1/Teff/logg etc, and simultaneously compare (i.e. overplot) with that of another
  track.
  @param list_dic_hist: list of dictionaries, where each dictionary is read by read.read_mesa or read.read_multiple_mesa_files
      and has at least these two keys: 1- 'header', and 2- 'hist'
      The xaxis must match one of the available keys in 'hist'.
  @type list_dic_hist: list of dictionaries
  @param xaxis: a key in 'hist' for the xaxis
  @type xaxis: string
  @param yaxis: a key in 'hist' for the yaxis
  @type yaxis: string
  @param list_lbls: list of strings, for each of the dictionaries in list_dic_hist
  @type list_lbls: list of strings
  @param list_list_dic_prof: defalt=None; if provided, it is complicated! It gives a list of list of dictionaries.
      per each history file in list_dic_hist, there are several profile files stored. These profile files are
      identified and read into a list of dictionaries by read.read_multiple_mesa_files. each dictionary has
      these keys: filename, header, and prof. Here, we mainly use the header only to get model_number and center_h1 for
      each profile file, and put them on the track, to highlight each point along the track where a profile is stored.
  @type list_list_dic_prof: list of list of dictionaries.
  @param xaxis_from, xaxis_to, yaxis_from, yaxis_to: giving the range in xaxis/yaxis
  @type xaxis_from, xaxis_to, yaxis_from, yaxis_to: float
  @param xtitle, ytitle: LaTeX compatible titles for the xaxis/yaxis
  @type xtitle, ytitle: string
  @param leg_loc: specifying the exact location of the legend box. It complies with the matplotlib.pyplot.legend options.
  @type leg_loc: integer, or string.
  @param file_out: full path to the output pdf plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_dic = len(list_dic_hist)
  if n_dic == 0:
    message = 'Error: plot_evol: plot_evol_hist: Input list is empty!'
    raise SystemExit, message

  if list_list_dic_prof:
    n_list_list_dic_prof = len(list_list_dic_prof)
    if n_dic != n_list_list_dic_prof:
      print 'Error: plot_evol: plot_evol_hist: Each track MUST associate with a list of dictionaries in list_list_dic_prof.'
      message = 'n_hist = %s, and n_list = %s' % (n_dic, n_list_list_dic_prof)
      raise SystemExit, message

  print ' - mesa_gyre.plot_evol:  '
  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=160)
  plt.axes([0.13, 0.13, 0.85, 0.85])
  if xtitle: plt.xlabel(xtitle)
  if ytitle: plt.ylabel(ytitle)

  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_dic-1)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)

  ls = itertools.cycle(['dotted', 'dashed', 'dashdot'])

  #--------------------------------
  # Unpack the dictionary contents iteratively
  #--------------------------------
  xaxis_list_min = []; xaxis_list_max = []
  yaxis_list_min = []; yaxis_list_max = []
  for i_hist, dic in enumerate(list_dic_hist):
    if not dic.has_key('hist'):
      print '   Warning: The key "hist" is missing in item number ', i_hist
      continue
    if not dic.has_key('header'):
      print '   Warning:  key "header" is missing in item number ', i_hist
      continue

    header = dic['header']
    hist = dic['hist']
    hist_names = hist.dtype.names
    if not xaxis in hist_names:
      print '   Warning: xaxis = %s is missing in "hist" ' % (xaxis, )
      continue
    if not yaxis in hist_names:
      print '   Warning: yaxis = %s is missing in "hist" ' % (yaxis, )
      continue
    if not 'log_g' in hist_names:
      print '   Warning: log_g not available, mxg not found for {0}'.format(i_hist)
      continue

    log_g     = hist['log_g']
    mxg       = np.argmax(log_g)

    xaxis_arr = hist[xaxis][mxg : ]
    yaxis_arr = hist[yaxis][mxg : ]

    if xlog: xaxis_arr = np.log10(xaxis_arr)
    if ylog: yaxis_arr = np.log10(yaxis_arr)

    if i_hist == ref_indx:
      clr = 'black'
      lstyl = 'solid'
      #list_lbls[ref_indx] = r'Ref: ' + list_lbls[ref_indx]
    else:
      clr = color.to_rgba(i_hist)
      lstyl = ls.next()

    if list_lbls:
      lbl = list_lbls[i_hist]
      plt.plot(xaxis_arr, yaxis_arr, color=clr, linestyle=lstyl, label=lbl)
    else:
      plt.plot(xaxis_arr, yaxis_arr, color=clr, linestyle=lstyl)

    xaxis_list_min.append(min(xaxis_arr))
    xaxis_list_max.append(max(xaxis_arr))
    yaxis_list_min.append(min(yaxis_arr))
    yaxis_list_max.append(max(yaxis_arr))

    #--------------------------------
    # Add profile data - if available - for each track
    #--------------------------------
    if list_list_dic_prof:
      list_dic_prof = list_list_dic_prof[i_hist]
      n_dic_in_list = len(list_dic_prof)
      if n_dic_in_list == 0: continue

      # Check if xaxis and yaxis are available in the HEADER of dic_prof:
      sample_dic_prof_names = list_dic_prof[0]['header'].dtype.names
      if xaxis not in sample_dic_prof_names:
        print '   Warning: plot_evol: plot_evol_hist: %s absent in prof data' % (xaxis)
        continue
      if yaxis not in sample_dic_prof_names:
        print '   Warning: plot_evol: plot_evol_hist: %s absent in prof data' % (yaxis)
        continue

      xaxis_prof_point = []; yaxis_prof_point = []
      for i_dic, dic_prof in enumerate(list_dic_prof):
        xaxis_prof_point.append(dic_prof['header'][xaxis][0])
        yaxis_prof_point.append(dic_prof['header'][yaxis][0])

      xaxis_prof_arr = np.asarray(xaxis_prof_point)
      yaxis_prof_arr = np.asarray(yaxis_prof_point)
      plt.scatter(xaxis_prof_arr, yaxis_prof_arr, s=35, marker='o', color=color.to_rgba(i_hist))

  #--------------------------------
  # Fixing the range, and annotations
  #--------------------------------
  if xaxis_from is not None: xaxis_min = xaxis_from
  else:          xaxis_min = min(xaxis_list_min)
  if xaxis_to is not None:   xaxis_max = xaxis_to
  else:          xaxis_max = max(xaxis_list_max)
  if yaxis_from is not None: yaxis_min = yaxis_from
  else:          yaxis_min = min(yaxis_list_min)
  if yaxis_to is not None:   yaxis_max = yaxis_to
  else:          yaxis_max = max(yaxis_list_max)
  plt.xlim(xaxis_min, xaxis_max)
  plt.ylim(yaxis_min, yaxis_max)

  #plt.annotate('(b)', xy=(0.03, 0.05), xycoords='axes fraction', fontsize='large')
  #plt.annotate('Instantaneous Mixing', xy=(0.50, 2.6), fontsize='medium', rotation=-23.5)

  if list_lbls:
    if not leg_loc: leg_loc = 1
    leg = plt.legend(loc=leg_loc, shadow=True, fontsize='small', framealpha=0.5)

  if not file_out:
    #track_srch_str = param.gen_track_srch_str()['track_srch_str']
    file_out = param.gen_path_plot() + 'Evol-Hist-' + xaxis + '-vs-' + yaxis + '.pdf'
  fig.savefig(file_out, dpi=160)
  print '  Plot stored at: %s' % (file_out, )
  plt.close()

  return fig

#=================================================================================================================
def plot_evol_prof_N_adim(list_dic, xaxis, list_lbls=None, xaxis_from=None, xaxis_to=None,
                   yaxis_from=None, yaxis_to=None, xtitle=None, leg_loc=None,
                   offset=False, box_from=None, box_to=None, file_out=None):
  """
  This is just a wrapper for plot_evol_prof by adding another field to each dictionary for adimensional N: "N_adim"
  Then, plot_evol_prof is called passing all arguments to it, and using yaxis='N_adim'.
  We strongly suggest you to read the documentation for plot_evol_prof. Note that it is no longer required to provide
  yaxis and ytitle as input arguments.
  """
  import commons
  import numpy.lib.recfunctions as nlrf
  from matplotlib.transforms import Bbox
  from matplotlib.path import Path

  dic_conv = commons.conversions()
  cd_to_Hz = dic_conv['cd_to_Hz']

  n_dic = len(list_dic)
  if n_dic == 0:
    message = 'Error: plot_evol: plot_evol_prof_N_adim: Input list is empty.'
    raise SystemExit, message

  dic_sample = list_dic[0]
  if 'prof' not in dic_sample.keys():
    message = 'Error: plot_evol: plot_evol_prof_N_adim: Input dictionaries miss the "prof" field.'
    raise SystemExit, message
  prof_sample = dic_sample['prof']
  prof_names = prof_sample.dtype.names
  if not xaxis in prof_names:
    message = 'Error: plot_evol: plot_evol_prof_N_adim: %s filed is missing in "prof" field.' % (xaxis, )
    raise SystemExit, message

  required_keys = ['brunt_N2', 'brunt_N', 'brunt_N2_dimensionless', 'brunt_N_dimensionless', 'brunt_nu',
                   'brunt_frequency', 'log_brunt_N', 'log_brunt_N2', 'log_brunt_N2_dimensionless',
                   'lamb_S2', 'lamb_S', 'lamb_SL']
  dic_avail_keys = {}
  for name in prof_names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)

  if not any(dic_keys.values()):
    print 'Error: plot_evol: plot_evol_prof_N_adim: All these fields are missing: '
    print required_keys
    raise SystemExit, 'Repeat MESA tracks with "brunt_N2_dimensionless" preserved in profiles.'

  # The best possibility is when 'brunt_N_dimensionless' is available, and no unit conversion is requird.
  # The worst case is when e.g. 'log_brunt_N2' is provided where some conversion is required.
  # The following procedure is written from the worst to the best to ensure we end up with the neatest profile
  # for N_adim!

  plt.figure()
  if all([box_from, box_to]):
    rect = plt.Rectangle((xaxis_from, box_from), xaxis_to-xaxis_from, box_to-box_from, facecolor="gray", alpha=0.5)
    plt.gca().add_patch(rect)
    bbox = Bbox.from_bounds(xaxis_from, box_from, xaxis_to-xaxis_from, box_to-box_from)

  list_dic_prof_extended = []
  for i_dic, dic in enumerate(list_dic):
    prof = dic['prof']
    header = dic['header']
    prof_names = prof.dtype.names
    star_mass = header['star_mass']
    star_mass_cgs = star_mass * Msun
    radius = header['photosphere_r']
    radius_cgs = radius * Rsun
    GM_R3 = G * star_mass_cgs / radius_cgs**3.0  # Hz^2
    sqrt_GM_R3 = np.sqrt(GM_R3)  # Hz
    Hz_to_adim = 1.0/sqrt_GM_R3  # e.g. N_adim = N_Hz * Hz_to_adim

    # the following require unit conversion
    if dic_keys['brunt_nu']:
      N_Hz = prof['brunt_nu'] * 1e-6
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['brunt_frequency']:
      N_cd = prof['brunt_frequency']
      N_Hz = N_cd * cd_to_Hz
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['log_brunt_N2']:
      N2_Hz2 = np.power(10., prof['log_brunt_N2'])
      N_Hz = np.sqrt(N2_Hz2)
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['log_brunt_N']:
      N_Hz = np.power(10.0, prof['log_brunt_N'])
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['brunt_N2']:
      N_Hz = np.sqrt(prof['brunt_N2'])
      N_adim = N_Hz * Hz_to_adim
    if dic_keys['brunt_N']: N_adim = prof['brunt_N'] * Hz_to_adim
    # The following do not require unit conversion
    if dic_keys['log_brunt_N2_dimensionless']:
      N2_adim = np.power(10.0, prof['log_brunt_N2_dimensionless'])
      N_adim  = np.sqrt(N2_adim)
    if dic_keys['brunt_N2_dimensionless']: N_adim = np.sqrt(prof['brunt_N2_dimensionless'])
    if dic_keys['brunt_N_dimensionless']: N_adim = prof['brunt_N_dimensionless']

    # Include Lamb Frequency if available
    S_adim = np.zeros(len(N_adim))
    if dic_keys['lamb_S2']: S_adim = np.sqrt(prof['lamb_S2']) * Hz_to_adim
    if dic_keys['lamb_SL']: S_adim = prof['lamb_SL'] * Hz_to_adim
    if dic_keys['lamb_S']: S_adim = prof['lamb_S'] * Hz_to_adim

    # Add the new field to the previous record array
    #N_adim = np.core.records.fromrecords(N_adim, dtype=[('N_adim',float)])
    #new_prof = recarr_join(prof, )
    #test_rec_arr = np.core.rec.fromarrays(N_adim, [('N_adim',float)])
    #print type(test_rec_arr), len(test_rec_arr), test_rec_arr.shape
    #new_prof = recarr_addcols(prof, [list(N_adim)], [('N_adim', '<f8')])
    ##nlrf.append_fields(prof, 'N_adim', data=N_adim, dtypes=new_dtypes, asrecarray=True, usemask=False)
    #list_dic_prof_extended.append(new_prof)
    #new_prof = 0.0


    if offset:
      N_adim += i_dic * offset
      S_adim += i_dic * offset
    plt.plot(prof[xaxis], N_adim)
    plt.plot(prof[xaxis], S_adim)


  # Now, pass the new list of profile dictionaries with the new "N_adim" field to plot_evol_prof
  #plot_evol_prof(list_dic_prof_extended, xaxis, 'N_adim', list_lbls=list_lbls, xaxis_from=xaxis_from, xaxis_to=xaxis_to,
                   #yaxis_from=yaxis_from, yaxis_to=yaxis_to, xtitle=xtitle, leg_loc=leg_loc,
                   #ytitle=r'Brunt Vaisala Frequency $N$ [\sqrt{GM/R^3}]', offset=offset, box_from=box_from, box_to=box_to,
                   #file_out=file_out)

  #print ' - plot_evol: plot_evol_prof_N_adim: Redirecting the plotting to plot_evol.plot_evol_prof()'
  plt.xlim(xaxis_from, xaxis_to)
  plt.ylim(yaxis_from, yaxis_to)
  plt.savefig(file_out)
  print '   Plot %s created \n' % (file_out, )
  plt.close()

  return None

#=================================================================================================================

