
import numpy as np
import read
import matplotlib
import matplotlib.pylab as plt

#=================================================================================================================
def compare_eta(list_dic_hist, list_lbls=None, ref_indx=0, from_pre_ms=True, from_zams=False,
                eta_surface=False, eta_center=False, eta_core_bndry=False, v_surf=False,
                by_L=False, by_t=False, by_n=False, by_Xc=False, 
                x_from=0, x_to=100, y_from=0, y_to=1.1, file_out=None):
  """
  This function compares the evolution of the surface/center/core_bndry of eta (=Omega/Omega_crit). 
  There are various choices for the x-axis, and also for the y-axis
  """
  import itertools
  
  n_hist = len(list_dic_hist)
  if n_hist == 0:
    message = 'Error: plot_hist: compare_eta: Input list is empty.'
    raise SystemExit, message
  if not any([eta_center, eta_core_bndry, eta_surface, v_surf]):
    print 'Error: plot_hist: compare_eta: Set ONE of these to True:'
    message = '    eta_center, eta_core_bndry or eta_surface, v_center, v_core_bdry, v_surf'
    raise SystemExit, message
  if not any([by_L, by_t, by_n, by_Xc]):
    message = 'Error: plot_hist: compare_eta: Set ONE of by_L, by_t, by_n, by_Xc to True.'
    raise SystemExit, message
  if all([from_pre_ms, from_zams]) or not any([from_pre_ms, from_zams]): 
    raise SystemExit, 'Error: plot_hist, compare_eta: Set either from_pre_ms OR from_zams to True.'
  
  required_keys = ['surf_avg_omega', 'surf_avg_omega_crit', 'surf_avg_omega_div_omega_crit',
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'surf_avg_v_div_v_crit', 
                   'log_L', 'log_Lnuc', 'log_LH', 'log_LHe', 'star_age', 'model_number',
                   'center_h1']
  ref_hist = list_dic_hist[ref_indx]['hist']
  dic_avail_keys = {} 
  hist_names = ref_hist.dtype.names
  for name in hist_names: dic_avail_keys[name] = 0.0
  dic_keys = read.check_key_exists(dic_avail_keys, required_keys)
  
  # Terminate if essential keys are missing
  if not dic_keys['surf_avg_omega_div_omega_crit'] or not all([dic_keys['surf_avg_omega'], 
                                                               dic_keys['surf_avg_omega_crit']]):
    raise SystemExit, 'Error: plot_hist: compare_eta: code 1'
  if v_surf:
    if not dic_keys['surf_avg_v_rot']: raise SystemExit, 'Error: plot_hist: compare_eta: code 2'
  
  #---------------------------
  # Define the Plot
  #---------------------------
  fig = plt.figure(figsize=(6,4.5))
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.105, right=0.99, bottom=0.10, top=0.98, hspace=0.02, wspace=0.02)

  colormap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_hist-1)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=colormap)

  ls = itertools.cycle(['dotted', 'dashed', 'dashdot'])
    
  #---------------------------
  # Iterate over all hist data
  #---------------------------
  for i_hist, dic_hist in enumerate(list_dic_hist):
    hist = dic_hist['hist']
    header  = dic_hist['header']

    log_g = hist['log_g']
    mxg = np.argmax(log_g)
    if from_pre_ms: start_indx = 0
    if from_zams: start_indx = mxg

    # fix x-axis and its range
    if by_L:
      if not dic_keys['log_L']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing log_L'
      log_L = hist['log_L']
      if not dic_keys['log_Lnuc'] and (not dic_keys['log_LH'] and not dic_keys['log_LHe']):
        raise SystemExit, 'Error: plot_hist: compare_eta: Missing log_Lnuc and/or log_LH, log_LHe.'
      if dic_keys['log_Lnuc']: 
        log_Lnuc = hist['log_Lnuc']
      elif (dic_keys['log_LH'] and dic_keys['log_LHe']):
        Lnuc = np.power(10.0, hist['log_LH']) + np.power(10.0, hist['log_LHe'])
        log_Lnuc = np.log10(Lnuc)
      log_Lnuc_div_L = log_Lnuc - log_L
      xaxis = log_Lnuc_div_L[start_indx : -1]
      xtitle = r'Nuclear to Total Luminosity $\log(L_{\rm nuc}/L_{\rm total})$'
        
    if by_t:   
      if not dic_keys['star_age']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing star_age.'
      star_age = hist['star_age']
      xaxis = star_age[start_indx : -1] / 1e6
      xtitle = r'Star Age [Myr]'
    
    if by_n:
      if not dic_keys['model_number']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing model_number.'
      model_number = hist['model_number']
      xaxis = model_number[start_indx : -1]
      xtitle = r'Model Number'
    
    if by_Xc:
      if not dic_keys['center_h1']: raise SystemExit, 'Error: plot_hist: compare_eta: Missing center_h1'
      center_h1 = hist['center_h1']
      xaxis = center_h1[start_indx : -1]
      xtitle = r'Center Hydrogen Mass Fraction [$X_c$]'
    
    if eta_surface:
      if not dic_keys['surf_avg_omega_div_omega_crit'] or (not dic_keys['surf_avg_omega'] and not dic_keys['surf_avg_omega_crit']):
        raise SystemExit, 'Error: plot_hist: compare_eta: Missing surf_avg_... omega.'
      if dic_keys['surf_avg_omega'] and dic_keys['surf_avg_omega_crit']: 
        surf_avg_omega_div_omega_crit = hist['surf_avg_omega']/hist['surf_avg_omega_crit']
      if dic_keys['surf_avg_omega_div_omega_crit']:
        surf_avg_omega_div_omega_crit = hist['surf_avg_omega_div_omega_crit']
      yaxis = surf_avg_omega_div_omega_crit[start_indx : -1] 
      ytitle = r'Surface $\eta_{\rm rot}=\Omega_{\rm eq}/\Omega_{\rm crit}$'
   
    if eta_center:
      if not dic_keys['center_omega_div_omega_crit']: 
        raise SystemExit, 'Error: plot_hist: compare_eta: center_omega_div_omega_crit missing'
      yaxis = hist['center_omega_div_omega_crit'][start_indx : -1]
      ytitle = r'Center $\eta_{\rm rot}=\Omega_{\rm eq}/\Omega_{\rm crit}$'
      
    if eta_core_bndry:
      if not dic_keys['cz_omega_div_omega_crit']: 
        raise SystemExit, 'Error: plot_hist: compare_eta: missing cz_omega_div_omega_crit'
      else:
        yaxis = hist['cz_omega_div_omega_crit'][start_indx : -1]
      ytitle = r'$\eta_{\rm rot}$ at Conv. Core Boundary'  
      
    if v_surf:
      if not dic_keys['surf_avg_v_rot']: raise SystemExit, 'Error: plot_hist: compare_eta: surf_avg_v_crit missing.'
      yaxis = hist['surf_avg_v_rot'][start_indx : -1]
      ytitle = r'Surface Rotation Velocity $v_{\rm rot}$ [km sec$^{-1}$]'
      
    
    #---------------------------
    # Iteratively, Overplot   
    #---------------------------
    if i_hist == ref_indx: 
      clr = 'black'
      lstyl = 'solid' 
      lbl = r'Ref: ' + list_lbls[i_hist]
    else: 
      clr = color.to_rgba(i_hist)
      lstyl = ls.next() 
      lbl = list_lbls[i_hist]
    ax.plot(xaxis, yaxis, linestyle=lstyl, color=clr, lw=2, label=lbl)
      
    # Add the ZAMS point
    if from_pre_ms: ax.scatter([xaxis[mxg]], [yaxis[mxg]], marker='o', color='red', s=25)  
      
  ax.set_xlabel(xtitle, fontsize='medium')
  ax.set_xlim(x_from, x_to)
  ax.set_ylabel(ytitle, fontsize='medium')
  ax.set_ylim(y_from, y_to)

  #---------------------------
  # Determine the location of Legend
  #---------------------------
  leg_loc = 0    # default
  if by_L and eta_surface: leg_loc = 2
  if by_Xc and eta_surface: leg_loc = 2
  if by_Xc and v_surf: leg_loc = 1
  if list_lbls:
    leg = ax.legend(loc=leg_loc, shadow=True, fancybox=True, fontsize='small')
  
  
  if not file_out: file_out = 'Compare-evol-eta.pdf'
  plt.savefig(file_out, dpi=200)
  print '\n - compare_eta: %s saved. \n' % (file_out, )
  plt.close()
  
  return None
        

#=================================================================================================================
#=================================================================================================================
#=================================================================================================================
#=================================================================================================================



