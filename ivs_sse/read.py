"""
  This module provides different tools to read in the data form MESA consistently.
  It supports reading a single hostory log file, a single profile, or a large number of
  files from a repository.
  
  log history:
  - 19 August 2014: published on bitbucket
  - 25 July 2013: created.
  
"""
import sys,os
import logging

try:
  import h5py
except:
  print 'h5py not installed! \n sudo pip install h5py'
import numpy as np

#=================================================================================================================
def read_h5(h5_file):
  """
  This function receives a full path to an HDF5 file containing MESA output, and store its content into a dictionary.
  It automatically identifies if the file is a hist file or a prof file. The output dictionary has the following keys:
   - filename: full path to the HDF5 file
   - source: 
   - header: the header information of the ascii file
   - hist: the numpy record array for the history table, if the input file is a hist file
   - prof: the numpy record array for the profile table, if the input file is a prof file
  @param h5_file: full path to the h5 file
  @type h5_file: string
  @return: dictionary with all information stored as key:value pairs explained above
  @rtype: Dictionary
  """
  if not os.path.isfile(h5_file): 
    message = 'Error: ascii2h5: read_h5: The input file: %s is missing.' % (h5_file, )
    raise SystemExit, message
  
  # Open the file
  try:
    f = h5py.File(h5_file, 'r')
  except:
    logging.error('read_h5: Unable to open {0}'.format(h5_file))
    return None
  
  data_group = f['/profile']
  header = dict( data_group.attrs.items() )
  data_names = data_group.keys()
  data = []
  for data_name in data_names:
    Dset = data_group[data_name][...]
    data.append(Dset.T)
  
  # convert the list of record arrays to named numpy record array.
  data = np.rec.fromarrays(data, names=data_names)
  
  # Close the file
  f.close()

  # decide whether the file is a history or a profile file 
  if 'model_number' in header.keys(): 
    is_hist=False; is_prof=True
  else: 
    is_hist=True; is_prof=False
 
  dic = {}
  dic['filename'] = h5_file
  dic['source'] = '' # read this from h5 attribute
  dic['header'] = header
  if is_hist and not is_prof: dic['hist'] = data
  if is_prof and not is_hist: dic['prof'] = data
   
  return dic

#=================================================================================================================
def check_fix_header(header):
  """
  This function checks the repetition in the header names, and fixes the repeated names by new names like copy_1, 
  copy_2 etc.
  @param header: list of strings
  @type param: list of string
  @return: 
   - flag: boolean, which specifies whether a change in header is applied or not.
   - new_header: list of string. It is a fixed list
  @rtype: boolean, list
  """
 
  n_hdr = len(header)
  if n_hdr == 0: 
    message = 'Error: read: check_fix_header: Empty header!'
    raise SystemExit, message
  
  n_tot_repeat = 0
  i_fix = 0
  for i_hdr in range(n_hdr-1):
    name = header[i_hdr]
    exclude_name = header[i_hdr+1:]
    flag = name in exclude_name
    if not flag: continue
    
    n_excluded = len(exclude_name)
    ind_repeat = [ind for ind in range(n_excluded) if name == exclude_name[ind]]
    n_repeat = len(ind_repeat)
    n_tot_repeat += n_repeat
    
    for i_repeat in range(n_repeat):
      i_fix += 1
      header[i_hdr+ind_repeat[i_repeat]+1] += '_fix' + str(i_fix)
     
  return flag, header

#=================================================================================================================
def read_multiple_mesa_files(files, just_header=None, is_hist = False, is_prof = False):
  """
  'Reads "multiple" MESA hist/prof files. Returns a list of dictionaries.'
  'Each item in the list is a dictionary and each dictionary is associated to'
  'each single hist/prof file.'
  'Keys are the header and column titles.'
  'Values for header keys are floats, and values for columns are numpy arrays.'
  """
  list_flags = [is_hist, is_prof]
  if all(list_flags) or not any(list_flags):
    message = 'Error: read: read_multiple_mesa_files: set ONLY "is_hist" OR "is_prof" flag to True'
    raise SystemExit, message
  if type(files) is not type([]):
    message = 'Error: read: read_multiple_mesa_files: You must provide a "list" of input filename(s)'
    raise SystemExit, message
  
  n_files = len(files)
  if n_files == 0:
    print 'File list is empty. Exit'
    raise SystemExit, 0

  # if is_prof:
  #   files = st.sort_list_files_by_model_number(files, extension='.prof')

  print ' - mesa_gyre.read.read_multiple_mesa_files:'
  
  # read the first file, and extract the datatype, and pass the dtype to read_mesa_ascii
  header, data = read_mesa_ascii(files[0])
  dtype = data.dtype
  
  data_lis = []
  n_skip = 0
  for i_file, mesa_file in enumerate(files):
    dic = {}
    if not os.path.isfile(mesa_file):     # Insert an empty dictionary, to keep track of the file
      print '   %s does not exist.' % (mesa_file, )
      n_skip += 1
      dic['filename'] = ''
      dic['header'] = 0.0
      if is_hist: dic['hist'] = np.rec.fromarrays([0], names=['empty'])
      if is_prof: dic['prof'] = np.rec.fromarrays([0], names=['empty'])
      #data_lis.append(dic)     # Not a very good idea
      continue
    #header, data = read_mesa(mesa_file, just_header=just_header)
    header, data = read_mesa_ascii(mesa_file, dtype=dtype)
    dic['filename'] = mesa_file
    dic['header'] = header
    if is_hist: dic['hist'] = data
    if is_prof: dic['prof'] = data
    data_lis.append(dic)
    if n_files<10: 
      print '   Read: %s, N_columns: %s, N_lines: %s' % (mesa_file, len(data.dtype.names), len(data))
  
  if n_skip>0: print '   %s files were skipped reading. \n' % (n_skip, )       
  else: print '   All %s files successfully read.\n' % (n_files, )
    
  return data_lis

#=================================================================================================================
def read_ekstrom(list_files):
  """
  Read the evolutionary tracks published by Ekstrom et al. 2012, A&A, "Grids of stellar models with rotation - 
  I. Models from 0.8 to 120 Msun at solar metallicity (Z = 0.014)"
  The track files have a strange problem; there are two lg(Teff) columns! We mark the second as "bad"
  @param list_files: list of strings giving full path to each track file
  @type list_files: list of strings
  @return: list of dictionaries giving the data content as a numpy record array
  @rtype: list of dictionaries
  """
  n_files = len(list_files)
  if n_files == 0:
    logging.error('Error: read: read_ekstrom: Input list is empty!')
    raise SystemExit
  
  list_dic = []
  for i_f, f in enumerate(list_files):
    if not os.path.isfile(f):
      logging.warning('Warning: read: read_ekstrom: {0} is missing'.format(f))
      print ' - Warning: read: read_ekstrom: {0} is missing'.format(f)
      continue
    with open(f, 'r') as r: lines = r.readlines()
    n_lines = len(lines)
    header  = lines[0].rstrip('\r\n').split()
    n_hdr   = len(header)
    ind_bad = [i for i in range(n_hdr) if header[i] == 'lg(Teff)']
    header[ind_bad[-1]] = 'bad'
    units   = lines[1]
    junk    = lines[2]
    
    data    = []
    for i_line, a_line in enumerate(lines[3:]):
      a_line = a_line.rstrip('\r\n').split()
      data.append(a_line)
    
    rec = np.core.records.fromarrays(np.array(data, float).T, names=header)
    
    dic = {}
    dic['filename'] = f
    dic['hist'] = rec
    
    list_dic.append(dic)
    
  return list_dic
    
#=================================================================================================================
def get_file_lines(filename):
  'Receives the filename, and enumerates the number of lines in the file.'
  
  fid = open(filename)
  fread = fid.read()
  fsplit = fread.split('\n')
  n_lines = len(fsplit)
  fid.close()
  
  return n_lines

#=================================================================================================================
def check_key_exists(dic, required_keys):
  #'Compares the keys in dic with the list of required_keys. 
  #Returns a dictionary with True/False boolean logicals for 
  #the fields that are present/missing.'

  keys = dic.keys()
  n_keys = len(keys)
  n_req_keys = len(required_keys)
  key_exists = []
  for i_req, which_req_key in enumerate(required_keys):
    for i_key, which_key in enumerate(keys):
      comp_two = cmp(which_req_key, which_key)
      flag = False
      if comp_two == 0:
        flag = True
        break
    key_exists.append(flag)
    
  flag_dic = {}
  for i, key in enumerate(required_keys):
    flag_dic[key] = key_exists[i]

  return flag_dic

#=================================================================================================================
def get_dtype(list_names):
  """
  Return hard-coded data types for data read from MESA history files or profiles.
  We treat integer columns as integer (np.int32), and all other float columns as single precision (np.float32), 
  except they belong to the list of double precision columns (np.float64).
  @param list_names: list of column names from the data block of the MESA history or profile file
  @type list_names: list of strings
  @return: numpy-compatible list of tuples for data type, each tuple is associated with a column, and has two elements. 
       The first element is the column name, and the second is the numpy data type
  @rtype: list of tuples
  """
  n_col = len(list_names)
  if n_col == 0:
    message = 'get_dtype: empty input list of names'
    logging.error(message)
    raise SystemExit, message
  
  set_int8_columns  = get_mesa_int8_col_names()    # for byte integer columns -128 < int < 128
  set_int16_columns = get_mesa_int16_col_names()   # for integer columns -32768 < int < 32767
  set_int32_columns = get_mesa_int32_col_names()   # for long integers -2147483648 < int < 2147483647
  set_dp_columns    = get_mesa_dp_col_names()      
  
  list_tup_out = []
  for i_tup, name in enumerate(list_names):
    if name in set_int8_columns:
      list_tup_out.append( (name, np.int8) ) 
    elif name in set_int16_columns:
      list_tup_out.append( (name, np.int16) )
    elif name in set_int32_columns:
      list_tup_out.append( (name, np.int32) )
    elif name in set_dp_columns:
      list_tup_out.append( (name, np.float64) )
    else:
      list_tup_out.append( (name, np.float64) )
  
  return np.dtype(list_tup_out)
  
#=================================================================================================================
def get_mesa_int8_col_names():
  """
  Return the names of those columns that contain integer values in MESA history or profile files. They are listed 
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  @return: set of strings with those columns that contain integer values in MESA
  @rtype: set
  """
  output = set([ 'mix_type_1', 'mix_type_2', 'mix_type_3', 'mix_type_4', 'mix_type_5', 'mix_type_6',             # hist
               'burn_type_1', 'burn_type_2', 'burn_type_3', 'burn_type_4', 'burn_type_5', 'burn_type_6',         # hist
               'mixing_type', 'mlt_mixing_type', 'sch_stable', 'ledoux_stable', 'stability_type'                 # prof
                ] )
      
  return output

#=================================================================================================================
def get_mesa_int16_col_names():
  """
  Return the names of those columns that contain integer values in MESA history or profile files. They are listed 
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  @return: set of strings with those columns that contain integer values in MESA
  @rtype: set
  """
  output = set([ 'num_zones', 'cz_zone', 'cz_top_zone', 'num_backups', 'num_retries', 'zone'] )
      
  return output

#=================================================================================================================
def get_mesa_int32_col_names():
  """
  Return the names of those columns that contain integer values in MESA history or profile files. They are listed 
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  @return: set of strings with those columns that contain integer values in MESA
  @rtype: set
  """
  output = set([ 'model_number', 'version_number', 'nse_fraction' ] )
      
  return output

#=================================================================================================================
def get_mesa_dp_col_names():
  """
  Return the names of those columns that contain double precision floats in MESA history or profile files. They are listed 
  in <mesa>/star/defaults/history_columns.list and <mesa>/star/defaults/profile_columns.list
  These are those specific columns in the profiles that are used for the purpose of generating GYRE or GraCo input files. 
  Since these codes require double precision input, these columns have to be preserved in double precision format.
  @return: set of strings with those columns that contain float values in MESA
  @rtype: set
  """
  output = set(['radius', 'mass', 'dq', 'eps_grav', 'cp', 'cv', 'Cv', 'brunt_N2_composition_term', 'lamb_S2',
                'q_div_xq', 'chiT_div_chiRho', 'kappa', 'kappa_rho', 'kappa_T', 'epsilon', 'epsilon_rho', 'epsilon_T',
                'omega', 'pressure', 'logP', 'pgas', 'prad', 'density', 'logRho', 'temperature', 'logT', 'luminosity',
                'luminosity_rad', 'luminosity_conv', 'gamma1', 'gradT', 'grada', 'brunt_N2', 'lnq', 'Cp', 'ln_free_e',
                'A2', 'Pg_div_P', 'gradr', 'h1', 'h2', 'he3', 'he4', 'li7', 'be7', 'c12', 'c13', 'n14', 'n15', 'o16',
                'o17', 'be9', 'si28' ])
           
  return output

#=================================================================================================================
def read_mesa_ascii(filename, dtype=None):
  """
  Read an history or profile ascii output from MESA.
  @param filename: full path to the input ascii file
  @type filename: string
  @param dtype: numpy-compatible dtype object. if it is not provided, it will be retrieved from read.get_dtype()
  @type dtype: list of tuples
  @return dictionary of the header of the file, and the record array for the data block. It can be called like this
     >>> header, data = read.read_mesa_ascii('filename')
  @rtype: dictionary and numpy record array
  """
  if not os.path.isfile(filename):
    message = 'read_mesa_ascii: {0} does not exist'.format(filename)
    logging.error(message)
    raise SystemExit, message
  
  if not check_file_is_ascii(filename):
    message = 'read_mesa_ascii: {0} is not ascii'.format(filename)
    logging.error(message)
    #raise SystemExit, message
  
  with open(filename, 'r') as r: lines = r.readlines()
  logging.info('read_mesa_ascii: {0} is read into list of lines'.format(filename))

  skip          = lines.pop(0)
  header_names  = lines.pop(0).rstrip('\r\n').split()
  header_strs   = lines.pop(0).rstrip('\r\n').split()
  header_vals   = []
  for val in header_strs:
    if 'D' in val: 
      val = val.replace('D', 'E')
    header_vals.append(val)
  temp          = np.array([header_vals], np.float64).T
  header        = np.core.records.fromarrays(temp, names=header_names)
  skip          = lines.pop(0)
  skip          = lines.pop(0)
  
  col_names     = lines.pop(0).rstrip('\r\n').split()
  n_cols        = len(col_names)
  if dtype is None:
    col_dtype     = get_dtype(col_names)
  else:
    col_dtype   = dtype
  if n_cols != len(col_dtype):
    message = 'read_mesa_ascii: Incompatible number of provided datatype objects'
    logging.error(message)
    raise SystemExit, message
  
  data          = []
  for i_line, line in enumerate(lines):
    if not line.rstrip('\r\n').split(): continue  # skip empty lines
    line = line.replace('D', 'E')
    data.append(line.rstrip('\r\n').split())
  
  data = np.core.records.fromarrays(np.array(data, np.float64).transpose(), dtype=col_dtype)
    
  return header, data

#=================================================================================================================
def check_file_is_ascii(filename):
  """
  Check if the file is trely ascii. Sometimes the files are corrupted on the VSC at the instance of writing to the disk. 
  The file header is ruined, and and ascii file looks like a binary file to the OS. 
  For the Windows Operating System (and different variants), we ignore this test, because the 
  "file" is not a respected command in Microsoft Windows command prompt. In this case, we return
  True, and bypass the file checks.
  @param filename: full path to the file to be checked
  @type filename: string
  @return: flag about the file being ascii (True) or not (False)
  @rtype: boolean
  """
  operating_system = sys.platform
  if 'win' in operating_system: return True 

  cmd = 'file ' + filename
  file_type = os.popen(cmd).readlines()[0]
  if 'ASCII text' in file_type: 
    flag = True
  elif 'ERROR: line 163: regex error 17, (illegal byte sequence)' in file_type:
    flag = True
  else:
    flag = False
    
  return flag

#=================================================================================================================
#=================================================================================================================
  