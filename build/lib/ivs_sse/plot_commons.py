
import logging
import numpy as np
import pylab as plt


#=================================================================================================================
#=================================================================================================================
def add_annotation(axis, list_dic_annot):
  """
  Add annotations to the axis
  @param axis: matplotlib axis, mainly created by matplotlib.figure.add_subplot()
  @type axis: matplotlib axis
  @param list_dic_annot: list of dictionaries with annotation key-value pairs. For possible keys see documentation
  @type list_dic_annot: list of dictionaries
  @return: axis updated with the annotation text
  @rtype: matplotlib axis
  """
  n_annot = len(list_dic_annot)
  if n_annot==0:
    raise SystemExit, 'Error: plot_profile: add_annotation: Input list is empty'
  
  for i_annot, dic_annot in enumerate(list_dic_annot):
      txt      = r'Annotation'
      xy       = (0.5, 0.90)
      rotation = 0
      xycoords = 'axes fraction'
      fontsize = 'small'
      alpha    = 1
      color    = 'black'
      family   = 'fantasy'
      style    = 'normal'
      
      if dic_annot.has_key('txt'):      txt      = dic_annot['txt']
      if dic_annot.has_key('xy'):       xy       = dic_annot['xy']
      if dic_annot.has_key('rotation'): rotation = dic_annot['rotation']
      if dic_annot.has_key('xycoords'): xycoords = dic_annot['xycoords']
      if dic_annot.has_key('fontsize'): fontsize = dic_annot['fontsize']
      if dic_annot.has_key('alpha'):    alpha    = dic_annot['alpha']
      if dic_annot.has_key('color'):    color    = dic_annot['color']
      if dic_annot.has_key('family'):   family   = dic_annot['family']
      if dic_annot.has_key('style'):    style    = dic_annot['style']
      
      axis.annotate(s=txt, xy=xy, xycoords=xycoords, rotation=rotation, fontsize=fontsize,
                  color=color, 
                  family=family, 
                  style=style, 
                  alpha=alpha)
  return axis
  
#=================================================================================================================
def set_canvas_to_black(fig):
  """
  Set the figure canvas to black, and the textuals to white.
  Remember to update plt.savefig() with the following additional keywords:
  plt.savefig('figure.png', facecolor=fig.get_facecolor(), transparent=True)
  @param fig: matplotlib figure object
  @type fig: matplotlib figure object
  @return: axis updated with new colors
  @rtype: matplotlib axis
  """
  fig.patch.set_facecolor('black')
  ax = fig.gca()
  ax.spines['bottom'].set_color('white')
  ax.spines['top'].set_color('white')
  ax.spines['left'].set_color('white')
  ax.spines['right'].set_color('white')
  ax.xaxis.label.set_color('white')
  ax.yaxis.label.set_color('white')
  ax.tick_params(axis='x', colors='white')
  ax.tick_params(axis='y', colors='white')

  return fig

#=================================================================================================================
