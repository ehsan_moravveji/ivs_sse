
"""
This module provides some post-processing plotting tools based on the input profile data from MESA.
The idea is that the data be read by the mesa_gyre.read.read_multiple_mesa_files, and a list of
dictionaries containing the whole data be passed to different functions in this module.
Any repeated reading must be avoided here.
"""

import sys, os
import numpy as np
import read

import matplotlib
from matplotlib.transforms import Bbox
import matplotlib.pyplot as plt
from scipy import interpolate
import itertools

Msun = 1.9892e33    # gr; as defined in mesa/const/const_def.f
Rsun = 6.9598e10    # cm; as defined in mesa/const/const_def.f
c_grav = 6.67428e-8 # cgs; as defined in mesa/const/const_def.f

#=================================================================================================================
def compare_column(list_dic_prof, xaxis=None, yaxis=None, list_lbls=None, xlog=False, xpower=False,
                   ylog=False, ypower=False, xaxis_from=None, xaxis_to=None, yaxis_from=None, yaxis_to=None,
                   xtitle=None, ytitle=None, list_dic_annot=None, leg_loc=None, file_out=None):
  """
  Compare the profile of a quantity (from MESA profile columns) on the yaxis w.r.t. xaxis
  @param list_dic_prof: list of dictionaries containing profile information
  @type list_dic_prof
  @param xaxis/yaxis: the name of the column that represents the x/y-axis
  @type xaxis/yaxis: string
  @param list_lbls: list of labels per each dictionary in the input list_dic_prof. Mandatory!
  @type list_lbls: list of strings
  @param xlog/ylog: take a logarithm of the x- and/or y-values and them put them on axis
  @type xlog/ylog: boolean
  @param xpower/ypower: take the x- and/or y-values to the power 10, and them put them on axis
  @type xpower/ypower: boolean
  @param xaxis_from/xaxis_to/yaxis_from/yaxis_to: range in x- and y-axis
  @type xaxis_from/xaxis_to/yaxis_from/yaxis_to: float
  @param xtitle/ytitle: The title to put on the axis
  @type xtitle/ytitle: string
  @param file_out: the full path to the output plot file
  @type file_out: string
  @return: None
  @rtype: None
  """
  n_prof = len(list_dic_prof)
  # if list_lbls is None:
  #   raise SystemExit, 'plot_profile: compare_column: Set list_lbls'
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax  = fig.add_subplot(111)
  plt.subplots_adjust(left=0.13, right=0.98, bottom=0.12, top=0.97)

  #--------------------------------
  # Prepare the plot and panels
  #--------------------------------
  import itertools
  ls = itertools.cycle(['solid', 'dashdot', 'dashed', 'dotted'])
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_prof)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)

  #--------------------------------
  # Loop over profiles, and Plot them!
  #--------------------------------
  list_min_x = []; list_max_x = []
  list_min_y = []; list_max_y = []
  for i_dic, dic in enumerate(list_dic_prof):
    header = dic['header']
    prof   = dic['prof']

    xvals  = prof[xaxis]
    yvals  = prof[yaxis]

    if xlog: xvals = np.log10(xvals)
    if ylog: yvals = np.log10(yvals)
    if xpower: xvals = np.power(10.0, xvals)
    if ypower: yvals = np.power(10.0, yvals)
    list_min_x.append(np.min(xvals))
    list_max_x.append(np.max(xvals))
    list_min_y.append(np.min(yvals))
    list_max_y.append(np.max(yvals))
    if list_lbls:
      lbl = r''+list_lbls[i_dic]
    else:
      lbl = ''

    ax.plot(xvals, yvals, linestyle=ls.next(), color=color.to_rgba(i_dic), lw=1.5, label=lbl)

  #--------------------------------
  # Annotations and Legend
  #--------------------------------
  if xaxis_from is None: xaxis_from = min(list_min_x)
  if xaxis_to is None:   xaxis_to   = max(list_max_x)
  if yaxis_from is None: yaxis_from = min(list_min_y)
  if yaxis_to is None:   yaxis_to   = max(list_max_y)
  ax.set_xlim(xaxis_from, xaxis_to)
  ax.set_ylim(yaxis_from, yaxis_to)
  if xtitle: ax.set_xlabel(xtitle)
  if ytitle: ax.set_ylabel(ytitle)

  if list_dic_annot is not None: add_annotation(ax, list_dic_annot)
  if list_lbls and leg_loc:
    leg = ax.legend(loc=leg_loc, fontsize='small')

  #--------------------------------
  # Finalize the Plot
  #--------------------------------
  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_profile: compare_column: stored {0}'.format(file_out)
    plt.close()

  return None


#=================================================================================================================
def plot_multiple_columns(dic_prof, xaxis=None, list_yaxis=[], list_lbls=[], xaxis_from=None, xaxis_to=None,
                          yaxis_from=None, yaxis_to=None, xtitle=None, ytitle=None, leg_loc=1, file_out=None):
  """
  Plot multiple quantities on the y-axis at the same time. all requested quantities must be already available in the
  input dictionary.
  @param dic_prof: dictionary with 'prof' and 'header' keys, containing the whole information from the MESA profile outpt
  @type dic_prof: dictionary
  """
  header = dic_prof['header']
  prof   = dic_prof['prof']
  names  = prof.dtype.names

  if xaxis not in names:
    print 'Error: plot_profile: plot_multiple_columns: {0} not in profile'.format(xaxis)
    raise SystemExit
  for a_name in list_yaxis:
    if a_name not in names:
      print 'Error: plot_profile: plot_multiple_columns: yaxis: {0} not in profile'.format(a_name)
      raise SystemExit
  n_y = len(list_yaxis)

  #--------------------------------
  # Prepare the Plot
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax  = fig.add_subplot(111)
  fig.subplots_adjust(left=0.13, right=0.97, bottom=0.13, top=0.97)

  if list_lbls is None: n_lbl = 0
  else: n_lbl = len(list_lbls)

  #--------------------------------
  # Set Up the Colors
  #--------------------------------
  import matplotlib.cm as cm
  import itertools
  cmap = plt.get_cmap('brg')
  norm = matplotlib.colors.Normalize(vmin=0, vmax=n_y)
  color = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
  linestyles = itertools.cycle(['solid', 'dashed', 'dashdot', 'dotted'])

  #--------------------------------
  # Loop over the y-axis and plot them
  #--------------------------------
  for i_y, yaxis in enumerate(list_yaxis):
    xvals = prof[xaxis]
    yvals = prof[yaxis]

    cl = color.to_rgba(i_y)
    ls = linestyles.next()

    if n_lbl>0:
      ax.plot(xvals, yvals, linestyle=ls, color=cl, lw=2, label=r''+list_lbls[i_y])
    else:
      ax.plot(xvals, yvals, linestyle=ls, color=cl, lw=2)

  #--------------------------------
  # Legend and Annotations
  #--------------------------------
  x_from = np.min(xvals); x_to = np.max(xvals)
  y_from = np.min(yvals); y_to = np.max(yvals)
  if xaxis_from is not None: x_from = xaxis_from
  if xaxis_to   is not None: x_to   = xaxis_to
  if yaxis_from is not None: y_from = yaxis_from
  if yaxis_to   is not None: y_to   = yaxis_to
  ax.set_xlim(x_from, x_to)
  ax.set_ylim(y_from, y_to)
  if xtitle: ax.set_xlabel(xtitle)
  if ytitle: ax.set_ylabel(ytitle)

  leg = ax.legend(loc=leg_loc, framealpha=0.5, fontsize='small')

  if file_out:
    plt.savefig(file_out, dpi=200)
    print ' - plot_profile: plot_multiple_columns: {0} saved'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def plot_hist_mesh(dic_prof, logT_step=None, mass_step=None, radius_step=None, file_out=None):
  """
  This function makes a 3-panel plot, showing the mesh distribution in the input model (MESA profile) in terms
  of logT, mass and radius.
  @param
  """
  if not dic_prof.has_key('header') or not dic_prof.has_key('prof'):
    message = 'Error: plot_profile: plot_hist_mesh: "header" or "prof" keys are missing from input dictionary'
    raise SystemExit, message

  if not any([logT_step, mass_step, radius_step]):
    message = 'Error: plot_profile: plot_hist_mesh: Choose one of logT_step, mass_step, radius_step'
    raise SystemExit, message

  header = dic_prof['header']
  prof = dic_prof['prof']
  prof_names = prof.dtype.names
  dic_avail_names = {}
  for name in prof_names: dic_avail_names[name] = 0.0
  requested_keys = ['logT', 'mass', 'dq', 'logdq', 'logR', 'radius', 'mixing_type',
                    'brunt_N', 'brunt_N2']
  dic_keys = read.check_key_exists(dic_avail_names, requested_keys)

  if not dic_keys['logT'] or not dic_keys['mass']:
    print 'Warning: plot_profile: plot_hist_mesh: "logT" and "mass" unavailable from prof recarray '
    return None
  if not dic_keys['dq'] and not dic_keys['logdq']:
    print 'Warning: plot_profile: plot_hist_mesh: "dq" and "logdq" both unavailable from prof recarray '
    return None
  if not dic_keys['radius'] and not dic_keys['logR']:
    print 'Warning: plot_profile: plot_hist_mesh: "radius" and "logR" both unavailable from prof recarray '
    return None

  logT = prof['logT']
  mass = prof['mass']
  if dic_keys['dq'] and not dic_keys['logdq']: dq = prof['dq']
  if not dic_keys['dq'] and dic_keys['logdq']: dq = np.power(10.0, prof['logdq'])
  if dic_keys['dq'] and dic_keys['logdq']: dq = prof['dq']
  if dic_keys['radius'] and not dic_keys['logR']: radius = prof['radius']
  if not dic_keys['radius'] and dic_keys['logR']: radius = np.power(10.0, prof['logR'])
  if dic_keys['radius'] and dic_keys['logR']: radius = prof['radius']
  if dic_keys['mixing_type']:
    mixing_type = prof['mixing_type']
    no_mixing = np.where((mixing_type == 0))[0]
    convective_mixing = np.where((mixing_type == 1))[0]
    overshoot_mixing = np.where((mixing_type == 2))[0]
    semiconvective_mixing = np.where((mixing_type == 3))[0]
    thermo_haline_mixing = np.where((mixing_type == 4))[0]
    rotation_mixing = np.where((mixing_type == 5))[0]
    minimum_mixing = np.where((mixing_type == 6))[0]
    anonymous_mixing = np.where((mixing_type == 7))[0]

    list_dic_conv = identify_mixing_type_edges(convective_mixing)
    list_dic_over = identify_mixing_type_edges(overshoot_mixing)
    list_dic_semi = identify_mixing_type_edges(semiconvective_mixing)
    list_dic_rot  = identify_mixing_type_edges(rotation_mixing)
    list_dic_anon = identify_mixing_type_edges(anonymous_mixing)

  nlines = len(dq)
  dr = np.zeros(nlines)
  for i in range(1, nlines-1): dr[i] = radius[i-1] - radius[i]

  fig = plt.figure(figsize=(9,3))
  plt.subplots_adjust(left=0.065, right=0.99, bottom=0.16, top=0.97, hspace=0.30, wspace=0.04)
  ax0 = fig.add_subplot(111)

  # highlight the background with boxes representing different mixing zones
  if dic_keys['mixing_type']:
    if list_dic_conv:
      for i_zone in range(len(list_dic_conv)):
        zone_from_indx = list_dic_conv[i_zone]['from']
        zone_to_indx   = list_dic_conv[i_zone]['to']
        zone_from_logT = logT[zone_from_indx]
        zone_to_logT   = logT[zone_to_indx]
        zone_width_logT = zone_to_logT - zone_from_logT
        rect_conv = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="0.90", edgecolor=None,
                                  lw=None)
        plt.gca().add_patch(rect_conv)
        bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)

    if list_dic_over:
      for i_zone in range(len(list_dic_over)):
        zone_from_indx = list_dic_over[i_zone]['from']
        zone_to_indx   = list_dic_over[i_zone]['to']
        zone_from_logT = logT[zone_from_indx]
        zone_to_logT   = logT[zone_to_indx]
        zone_width_logT = zone_to_logT - zone_from_logT
        rect_over = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="Pink")
        plt.gca().add_patch(rect_over)
        bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)

    if  list_dic_semi:
      for i_zone in range(len(list_dic_semi)):
        zone_from_indx = list_dic_semi[i_zone]['from']
        zone_to_indx   = list_dic_semi[i_zone]['to']
        zone_from_logT = logT[zone_from_indx]
        zone_to_logT   = logT[zone_to_indx]
        zone_width_logT = zone_to_logT - zone_from_logT
        rect_semi = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="green")
        plt.gca().add_patch(rect_semi)
        bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)

    if  list_dic_rot:
      for i_zone in range(len(list_dic_rot)):
        zone_from_indx = list_dic_rot[i_zone]['from']
        zone_to_indx   = list_dic_rot[i_zone]['to']
        zone_from_logT = logT[zone_from_indx]
        zone_to_logT   = logT[zone_to_indx]
        zone_width_logT = zone_to_logT - zone_from_logT
        rect_rot = plt.Rectangle((zone_from_logT, 0), zone_width_logT, nlines, facecolor="Blue")
        plt.gca().add_patch(rect_rot)
        bbox = Bbox.from_bounds(zone_from_logT, 0, zone_width_logT, nlines)


  # Fix number of bins and ranges:
  #if not logT_step: logT_step = 0.01
  #if not mass_step: mass_step = 0.001
  #if not radius_step: radius_step = 0.01
  #n_radius = radius[0]/radius_step

  logT_from = logT[-1]; logT_to = logT[0]
  mass_from = 0.0; mass_to = mass[0]
  radius_from = 0.0; radius_to = radius[0]

  # Histogram w.r.t. logT
  if logT_step is not None:
    n_logT = (logT[-1] - logT[0])/logT_step
    hist_arr, bins, patches = ax0.hist(logT, bins=n_logT, range=(logT_to, logT_from), color='blue')
    hist_arr = np.asarray(hist_arr)
    ax0.set_xlim(logT_from, logT_to)
    ax0.set_xlabel(r'Temperature $\log T$ [K]')
    ax0.set_ylabel(r'Count')
    ax0.set_ylim(0, max(hist_arr)*1.05)
    ax0.annotate(r'$\delta \log T$='+str(logT_step), xy=(0.3, 0.80), xycoords='axes fraction')

  # Histogram w.r.t. mass
  if mass_step is not None:
    n_mass = mass[0]/mass_step
    hist_arr, bins, patches = ax0.hist(mass, bins=n_mass, range=(mass_from, mass_to), color='blue')
    hist_arr = np.asarray(hist_arr)
    ax0.set_xlim(mass_from, mass_to)
    ax0.set_xlabel(r'Mass [M$_\odot$]')
    ax0.set_ylabel(r'Count')
    ax0.set_ylim(0, max(hist_arr)*1.05)
    ax0.annotate(r'$\delta M$='+str(mass_step), xy=(0.3, 0.80), xycoords='axes fraction', fontsize='medium')

  ax0.annotate(r'N$_{\rm mesh}=$'+str(nlines), xy=(0.3, 0.90), xycoords='axes fraction', fontsize='medium')
  str_Xc = r'%0.4f' % (dic_prof['header']['center_h1'], )
  ax0.annotate(r'$X_{\rm c}=$'+str_Xc, xy=(0.3, 0.70), xycoords='axes fraction')

  # Add the Brunt-Vaisala Frequency, if available
  if not dic_keys['brunt_N2'] and dic_keys['brunt_N']:
    brunt_N = prof['brunt_N']
    neg_indx = brunt_N <= 0.0
    #brunt_N[neg_indx] = 1e-20
    brunt_N2 = brunt_N**2.0
  if dic_keys['brunt_N2'] and not dic_keys['brunt_N']:
    brunt_N2 = prof['brunt_N2']
    #brunt_N2[brunt_N2<=0.0] = 1e-10
  brunt_N2[:] -= min(brunt_N2)
  brunt_N2[:] += 1e-10
  log_brunt_N2 = np.log10(brunt_N2)
  scale_log_brunt_N2 = log_brunt_N2 * 15
  #scale_log_brunt_N2 = log_brunt_N2/max(log_brunt_N2) * max(hist_arr)
  #scale_log_brunt_N2 = brunt_N2/np.mean(brunt_N2) * np.mean(hist_arr)

  print min(log_brunt_N2), max(log_brunt_N2), min(scale_log_brunt_N2), max(scale_log_brunt_N2)

  ax0.plot(logT, scale_log_brunt_N2, color='Red')

  if not file_out: file_out = 'Hist-Mesh.pdf'
  plt.savefig(file_out)
  print ' - Plot %s Created.' % (file_out, )
  plt.close()

  return None


#=================================================================================================================
def plot_kappa_components(dic_prof, xaxis='logT', file_out=None):
  """
  The radiative opacity comprises of different components:
    - bound-bound transitions
    - bound-free transitions (including H^- breakup)
    - free-free transitions
  and a constant background opacity due to electrons scattering.
  This routine plots the Rossland opacity as read from profile, and also compares that with the Kramers law (for bf transition),
  free-free transitions and the electron scattering.
  @param dic_prof: dictionary with profile information returned by e.g. read.read_mesa()
  @type dic_prof: dictionary
  @param xaxis: the choice of x-axis parameter. Note that it should be already available in the profile data structure. default='logT'
  @type xaxis: string
  @param file_out: full path to the output plot file
  @type file_out: string
  @return None
  @rtype None
  """
  header = dic_prof['header']
  prof     = dic_prof['prof']
  names  = prof.dtype.names
  required_fileds = [xaxis, 'logRho', 'logT', 'opacity', 'x', 'y', 'z']
  for a_name in required_fileds:
    if a_name not in names:
      raise SystemExit, 'Error: plot_profile: plot_kappa_components: "{0}" not available in profile columns'.format(a_name)

  logT     = prof['logT']
  kappa    = prof['opacity']
  logRho   = prof['logRho']
  x        = prof['x']
  y        = prof['y']
  z        = prof['z']
  nz       = len(x)

  kappa_es = 0.200 * (1. + x)
  log_kappa_es = np.log10(kappa_es)
  log_kappa_bf = np.log10(4.3e25) + np.log10(z) + np.log10(1 + x) + logRho -3.5 * logT
  log_kappa_ff  = np.log10(3.7e22) + np.log10(x + y) + np.log10(1 + x) + logRho -3.5 * logT

  ind_H             = np.where((logT >= np.log10(3e3)) & (logT <= np.log10(6e3)) & (logRho>=-10) & (logRho <= -5)
                                            & (z <= 0.02))[0]
  n_H                = len(ind_H)
  if n_H > 0:
    print ' - plot_profile: plot_kappa_components: Including H- opacity!'
    logT_H_minus       = logT[ind_H]
    log_kappa_H_aux    = np.log10(2.5e-31) + np.log10(z/0.02) + np.log10(1 + x) + 0.5*logRho + 9. * logT
    log_kappa_H        = np.zeros(nz) - 100.0
    log_kappa_H[ind_H] =log_kappa_H_aux[ind_H]
    kappa_bf           = np.power(10, log_kappa_bf) + np.power(10, log_kappa_H)
    log_kappa_bf       = np.log10(kappa_bf)
    log_kappa_H        = log_kappa_H[ind_H]

  #--------------------------------
  # Prepare the Plot
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=200)
  ax  = fig.add_subplot(111)
  fig.subplots_adjust(left=0.13, right=0.97, bottom=0.13, top=0.97)

  ax.plot(logT, np.log10(kappa), lw=2, color='black', linestyle='solid', label=r'Rosseland Mean Opacity $\kappa_{\rm rad}$')
  ax.plot(logT, log_kappa_es, lw=1.5, color='blue', linestyle='dashed', label=r'Electron Scattering $\kappa_{\rm es}$')
  ax.plot(logT, log_kappa_bf, lw=1.5, color='red', linestyle = 'dashdot', label=r'Bound-Free Opacity $\kappa_{\rm bf}$')
  if n_H > 0:
    ax.plot(logT_H_minus, log_kappa_H, color='green', linestyle='dashed', label=r'H$^-$ Opacity $\kappa_{H^-}$')
  ax.plot(logT, log_kappa_ff, lw=1.5, color='grey', linestyle='dotted', label=r'Free-Free Opacity $\kappa_{\rm ff}$')

  ax.set_xlim(7.5, 3.5)
  if xaxis == 'logT':
    xlabel = r'Temperature $\log T$ [K]'
  else:
    xlabel = r'' + xaxis.replace('_', ' ') + ''
  ax.set_xlabel(xlabel)
  # ax.set_ylim(-1,  1)
  ax.set_ylabel(r'Radiative Opacity $\,\log \kappa\,$ [cm$^2$ gr$^{-1}$]')

  leg1 = ax.legend(loc=2, fontsize='small')

  if file_out:
    plt.savefig(file_out)
    print ' - plot_profile: plot_kappa_components: saved {0}'.format(file_out)
    plt.close()

  return None

#=================================================================================================================
def identify_mixing_type_edges(indx_arr):
  """
  This function receives an array with indexes in the profile model, where mixing_type == certain_mixing_type. However,
  during the evolution the extent and the number of different mixing zones vary. This function determines the number of
  zones with certain_mixing_type, and also returns the index of their boundaries.
  @param indx_arr: array with indexes in the profile model where mixing_type == certain_mixing_type
  @type indx_arr: Numpy array
  @return: list of dictionaries with zone information. Each dictionary has the following three keys:
      - zone: integer, which is the zone label, starting from the surface inward
      - from: integer, index of the upper edge of the zone
      - to: integer, index of the inner edge of the zone
  @rtype: list of dictionaries
  """
  n_indx = len(indx_arr)
  if n_indx < 2: return None

  count_zones = 1  # at least one zone exists
  indx_start = indx_arr[0]; indx_end = -1; indx_next = -1
  list_dic_zones = []



  for i in range(n_indx-1):
    jump = False
    indx = indx_arr[i]
    indx_next = indx_arr[i+1]

    if (indx_next == indx + 1): continue
    else: jump = True

    if jump:
      indx_end = indx

      dic_zone = {}
      dic_zone['zone'] = count_zones
      dic_zone['from'] = indx_start
      dic_zone['to']   = indx_end
      list_dic_zones.append(dic_zone)

      indx_start = indx_next
      count_zones += 1

  # Add the last zone manually:
  dic_zone = {}
  dic_zone['zone'] = count_zones
  dic_zone['from'] = indx_start
  dic_zone['to']   = indx_arr[-1]
  list_dic_zones.append(dic_zone)

  return list_dic_zones


#=================================================================================================================



