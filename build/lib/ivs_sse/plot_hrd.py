"""
This module provides methods to plot a single or multiple MESA evolutionary tracks
as an Hertzsprung-Russel diagram (HRD), or as a Kiel diagram (i.e. logTeff vs. log_g)
"""
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from pylab import *
import math
import numpy as np

import read as rd


#=================================================================================================================
def plot_kiel_diagram(list_hist, list_list_dic_prof=None, list_lbls=None, ref_indx=0, 
                      list_dic_stars=None, list_dic_mesa_gyre = None, 
                      file_out=None, logTeff_from=None, logTeff_to=None, 
                      logg_from=None, logg_to=None, *args, **kwargs):
  """
  Kiel Diagram -i.e. log_Teff vs. log_g - for a grid of MESA tracks.
  @param list_hist: list of dictionaries, where each dictionary contains all *.hist information
              Values for each of dictionary key is either a single integer/float/string or
              a 1D numpy array.
  @type list_hist: list of dictionaries
  @param list_list_dic_prof: defalt=None; if provided, it is complicated! It gives a list of list of dictionaries.
      per each history file in list_hist, there are several profile files stored. These profile files are 
      identified and read into a list of dictionaries by read.read_multiple_mesa_files. each dictionary has 
      these keys: filename, header, and prof. Here, we mainly use the header only to get logTeff and logg for 
      each profile file, and put them on the track, to highlight each point along the track where a profile is 
      stored.
  @type list_list_dic_prof: list of list of dictionaries.
  @param list_lbls: list of labels, where each label corresponds to one track in list_hist
  @type list_lbls: list of strings
  @param list_dic_stars: list of dictionaries with stars loaded from mesa_gyre.stars
  @type list_dic_stars: list of dictionaries
  @param list_dic_mesa_gyre: list of MESA profile data, to flag the position of available profile on the HRD track
  @type list_dic_mesa_gyre: list of dictionaries 
  @param file_out: full path to store the Kiel diagram plot as a pdf file
  @type file_out: string
  @param logTeff_from, logTeff_to: the range of logTeff on the x-axis; defalt = None
  @type logTeff_from, logTeff_to: float
  @param logg_from, logg_to: the range of log_g on the y-axis; defalt = None
  @type logg_from, logg_to: float
  @return: None
  @rtype:  None
  """
  import itertools
  
  n_tracks = len(list_hist)
  if n_tracks == 0:
    message = 'Error: plot_hrd: plot_kiel_diagram: Input list is empty'
    raise SystemExit, message
    
  if list_list_dic_prof: n_list_list_dic_prof = len(list_list_dic_prof)

  dic_sample = list_hist[0]
  if not dic_sample.has_key('hist'):
    message = 'Error: plot_hrd: plot_kiel_diagram: sample dictionary has no .hist field (which is numpy record array itself)'
  hist_sample = dic_sample['hist']
  dic_avail_keys = {}
  for name in hist_sample.dtype.names: dic_avail_keys[name] = 0.0
  required_keys = ['initial_mass', 'initial_z', 'Teff', 'log_Teff', 'log_g', 'gravity',
                   'surf_avg_v_rot', 'surf_avg_v_crit']
  dic_keys = rd.check_key_exists(dic_avail_keys, required_keys)
  flag_initial_mass = dic_keys['initial_mass']
  flag_initial_z    = dic_keys['initial_z']
  flag_log_Teff = dic_keys['log_Teff']
  flag_log_g    = dic_keys['log_g']
  flag_surf_v_rot  = dic_keys['surf_avg_v_rot']
  flag_surf_v_crit = dic_keys['surf_avg_v_crit']

  if not flag_log_Teff or not flag_log_g:
    print 'Kiel requires log_g and log_Teff. '
    print 'log_Teff:', flag_log_Teff, ' and log_g:', flag_log_g
    message = 'Error: plot_hrd: plot_kiel_diagram: Exit'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting; Load colors
  #--------------------------------
  fig = plt.figure(figsize=(6,4), dpi=600)
  ax = fig.add_subplot(111)
  plt.subplots_adjust(left=0.09, right=0.97, bottom=0.105, top=0.97, hspace=0.01, wspace=0.01)
  ax.set_xlabel(r'$\log$' +r'$\, T_{\rm eff}$', fontsize='medium')
  ax.set_ylabel(r'$\log \, g \, {\rm [cgs]}$', fontsize='medium')
  
  cmap = plt.get_cmap('brg')
  norm = mpl.colors.Normalize(vmin=0, vmax=n_tracks)
  color = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
  
  ls = itertools.cycle(['dotted', 'dashed', 'dashdot'])

  #--------------------------------
  # Read the grid iteratively
  #--------------------------------
  tracks_min_log_Teff = np.zeros(n_tracks)
  tracks_max_log_Teff = np.zeros(n_tracks)
  tracks_min_log_g    = np.zeros(n_tracks)
  tracks_max_log_g    = np.zeros(n_tracks)

  for i_track in range(n_tracks):
    hist = list_hist[i_track]['hist']
    mxg = 0
    if flag_log_g:
      log_g = hist['log_g']
      max_g = log_g.max()
      # mxg = np.argmax(log_g)
      log_g = log_g[mxg:]
    log_Teff = hist['log_Teff'][mxg:]
    n_lines = len(log_Teff)
    #if flag_phot_L: log_L = np.log10(hist['photosphere_L'])[mxg:]
    #if flag_log_L: log_L = hist['log_L'][mxg:]
    if flag_initial_mass: m_ini = hist['initial_mass']
    if flag_initial_z:    z_ini = hist['initial_z']
    if flag_surf_v_rot:  v_rot = hist['surf_avg_v_rot'][mxg:]
    if flag_surf_v_crit: v_cri = hist['surf_avg_v_crit'][mxg:]
    
    tracks_min_log_Teff[i_track] = log_Teff.min()
    tracks_max_log_Teff[i_track] = log_Teff.max()
    tracks_min_log_g[i_track] = log_g.min()
    tracks_max_log_g[i_track] = log_g.max()
    
    # if list of profiles are also provided, flag each profile file along the track with a dot
    if list_list_dic_prof:
      which_list_dic = list_list_dic_prof[i_track]
      n_dic_in_list = len(which_list_dic)
      list_prof_logTeff = np.zeros(n_dic_in_list)
      list_prof_logg    = np.zeros(n_dic_in_list)
      for i_dic_prof, dic_prof in enumerate(which_list_dic):
        dic_prof_header = dic_prof['header']
        one_prof_Teff = dic_prof_header['Teff']
        one_prof_mass = dic_prof_header['star_mass']
        one_prof_radi = dic_prof_header['photosphere_r']
        logg_sun      = np.log10(27542.29)
        one_prof_logg = np.log10(one_prof_mass/(one_prof_radi**2.0)) + logg_sun
        # Put profile points if available  
        list_prof_logTeff[i_dic_prof] = np.log10(one_prof_Teff[0])
        list_prof_logg[i_dic_prof]    = one_prof_logg[0]
      #list_prof_logTeff = np.asarray(list_prof_logTeff)
      #list_prof_logg    = np.asarray(list_prof_logg)

    # Find appropriate on-screen text based on search param
    if list_lbls: 
      #if i_track == ref_indx: list_lbls[ref_indx] = r'Ref: ' + list_lbls[ref_indx]
      leg_label = r'' + list_lbls[i_track]
    else:
      leg_label = ''
           
    lw = 1
    if list_list_dic_prof: lw = 1       
    if i_track == ref_indx:
      lstyle = 'solid'
      clr = 'black'
    else:
      lstyle = ls.next()
      clr = color.to_rgba(i_track)
    #lstyle = 'solid'
      
    # Now, plot the track
    if list_lbls:
      ax.plot(log_Teff, log_g, linewidth=lw, linestyle=lstyle, color=clr, label = leg_label)
    else:
      ax.plot(log_Teff, log_g, linewidth=lw, linestyle=lstyle, color='gray')
    # Highlight those models along the track for which a profile file is stored
    if list_list_dic_prof:
      ax.scatter(list_prof_logTeff, list_prof_logg, s=25, color=color.clr)
    # Highlight the ZAMS
    ax.scatter([log_Teff[0],], [log_g[0],], 50, color='purple', label='ZAMS')

    # ax.scatter([log_Teff[3000],], [log_g[3000],], 50, color='red', label='Red Giant Branch')
    # ax.scatter([log_Teff[200],], [log_g[200],], 50, color='orange', label='Red Giant Branch')
    # ax.scatter([log_Teff[-100],], [log_g[-100],], 50, color='green', label='Core He Burning')
    # leg = ax.legend(loc = 4, shadow=True, fontsize='medium')

  #--------------------------------
  # Final Plot Specifications
  #--------------------------------
  if list_lbls:
    leg = ax.legend(loc = 4, shadow=True, fontsize='small')
  max_log_Teff = tracks_max_log_Teff.max()
  min_log_Teff = tracks_min_log_Teff.min()
  max_log_g    = tracks_max_log_g.max()
  min_log_g    = tracks_min_log_g.min()
  #xlim(max_log_Teff+0.02, min_log_Teff-0.02)
  #ylim(max_log_g+0.2, min_log_g-0.1)
  if logTeff_from is not None: xlim_1 = logTeff_from
  else: xlim_1 = max_log_Teff+0.02
  if logTeff_to is not None: xlim_2 = logTeff_to
  else: xlim_2 = min_log_Teff-0.02
  if logg_from is not None: ylim_1 = logg_from
  else: ylim_1 = max_log_g + 0.05
  if logg_to is not None: ylim_2 = logg_to
  else: ylim_2 = min_log_g-0.05
  #   
  ax.set_xlim(xlim_1, xlim_2)
  ax.set_ylim(ylim_1, ylim_2)
    
  #--------------------------------
  # Add Observed Stars with Asymmetric
  # Errorbars
  #--------------------------------
  if False and list_dic_stars:
    conny_stars = rd.add_stars('star_lists/aerts.list')
    for col_names in conny_stars.dtype.names:
      if col_names == 'log_g':
        obs_log_g = conny_stars[col_names]
      if col_names == 'e_log_g':
        obs_log_g_err = conny_stars[col_names]
      if col_names == 'Teff':
        Teff_col = col_names
        obs_log_Teff = np.log10(conny_stars[col_names])
      if col_names == 'e_Teff':
        if conny_stars[col_names].any > 0:
          err_Teff_col = col_names
          obs_log_Teff_err_hi = np.log10(conny_stars[Teff_col]+conny_stars[err_Teff_col]) - np.log10(conny_stars[Teff_col])
          obs_log_Teff_err_lo = np.log10(conny_stars[Teff_col]) - np.log10(conny_stars[Teff_col]-conny_stars[err_Teff_col])
        obs_log_Teff_err = [obs_log_Teff_err_lo, obs_log_Teff_err_hi]
      
    ax.errorbar(obs_log_Teff, obs_log_g, xerr = obs_log_Teff_err, yerr = obs_log_g_err, 
                 color = 'Red', fmt = 'o')
    for i in range(len(conny_stars)):
      ax.text(obs_log_Teff[i], obs_log_g[i], conny_stars['Name'][i], ha='left', va='center', color='Red',
              transform=gca().transAxes, fontsize = 10, clip_on = True)
    
  #--------------------------------
  # Add Stars from a list
  #--------------------------------
  if list_dic_stars:
    n_dic_stars = len(list_dic_stars)
    print '\n - plot_hrd: plot_kiel_diagram: Including %s stars from the input list' % (n_dic_stars, )
    for i_star, dic_star in enumerate(list_dic_stars):
      star_name = dic_star['name'][0]
      star_Teff = dic_star['Teff']
      star_log_Teff = dic_star['log_Teff']
      star_Teff_err = dic_star['err_Teff_1s']
      star_log_Teff_err = dic_star['err_log_Teff_1s']
      star_logg = dic_star['logg']
      star_logg_err = dic_star['err_logg_1s']
      ax.errorbar(star_log_Teff, star_logg, xerr = star_log_Teff_err, yerr = star_logg_err, color = 'Orange', fmt='s')

  #--------------------------------
  # Add points from MESA GYRE grid to show the track with
  # available seismic information
  #--------------------------------
  if list_dic_mesa_gyre:
    n_list_dic_mesa_gyre = len(list_dic_mesa_gyre)
    for i_dic in range(n_list_dic_mesa_gyre):
      which_dic = list_dic_mesa_gyre[i_dic]
      model_Teff = which_dic['Teff']
      model_logg = which_dic['log_g']
      ax.scatter([model_Teff,], [model_logg,], 10, color='red')

  #--------------------------------
  # Annotations, Texts and Legend
  #--------------------------------
  left = 0.06; top = 0.94; lower = 0.045
  
  # ax.annotate('(a)', xy=(0.05, 0.90), xycoords='axes fraction', fontsize='large')
  # ax.annotate(r'$M = 3.2 M_\odot$', xy=(0.05, 0.82), xycoords='axes fraction', fontsize='large')
  ax.annotate(r'$Z = 0.014$', xy=(0.05, 0.77), xycoords='axes fraction', fontsize='large')
  #ax.annotate(r'0\leq f_{\rm ov}\leq 0.03', xy=(0.05, 0.80), xycoords='axes fraction', fontsize='large')


  #--------------------------------
  # Set Plot Name, and Finalize the Plot
  #--------------------------------
  if not file_out:
    file_out = 'Kiel-Diagram.png'
  fig.savefig(file_out, dpi = 600)
  plt.close()
  print ' - Kiel Diagram stored at: %s \n' % (file_out, )

  return None
  

#=================================================================================================================
def hrd(list_dic, list_lbls=None, list_dic_stars=None, log_Teff_from=None, log_Teff_to=None, 
                  log_L_from=None, log_L_to=None, file_out=None):
  """
  HR Diagram for a grid of MESA tracks.
  @param list_dic: list of dictionaries, where each dictionary contains all *.hist information
         Values for each of dictionary key is either a single integer/float/string or
         a 1D numpy array.
  @type list_dic: list
  @param list_lbls: list of labels, where each label corresponds to one track in list_hist
  @type list_lbls: list of strings
  @param list_dic_stars: list of dictionaries with stars loaded from mesa_gyre.stars
  @type list_dic_stars: list of dictionaries
  @param log_Teff_from, log_Teff_to: left and right values for log_Teff on the xaxis.
  @type log_Teff_from, log_Teff_to: float
  @param log_L_from, log_g_to: lower and upper limits on log luminosity to display on the yaxis.
  @type log_L_from, log_g_to: float  
  @param file_out: full path to the output HR diagram pdf file
  @type file_out: string
  @return: None
  @rtype: None
  """ 
   
  n_tracks = len(list_dic)
  if n_tracks == 0:
    print 'Error: hrd: Input list is empty'
    raise SystemExit, 0
  if n_tracks == 1:
    plot_single_hrd(list_dic[0]['hist']) # passes the only dictionary to plot a single HRD
  
  hist_sample = list_dic[0]['hist']
  hist_sample_names = hist_sample.dtype.names
  dic_avail_keys = {}
  for key in hist_sample_names:
    dic_avail_keys[key] = 0.0
  required_keys = ['initial_mass', 'initial_z', 'log_Teff', 'photosphere_L', 'log_L', 
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'log_g']
  dic_keys = rd.check_key_exists(dic_avail_keys, required_keys)
  flag_initial_mass = dic_keys['initial_mass']
  flag_initial_z    = dic_keys['initial_z']
  flag_log_Teff     = dic_keys['log_Teff']
  flag_phot_L       = dic_keys['photosphere_L']
  flag_log_L        = dic_keys['log_L']
  flag_log_g        = dic_keys['log_g']
  flag_surf_v_rot   = dic_keys['surf_avg_v_rot']
  flag_surf_v_crit  = dic_keys['surf_avg_v_crit']

  if not flag_log_Teff or (not flag_log_L and not flag_phot_L):
    print 'HRD requires L and Teff. '
    print 'log_Teff:', flag_log_Teff, 'photosphere_L:', flag_phot_L, ' and log_L: ', flag_log_L
    message = 'Error: hrd: plot_single_hrd: Exit'
    raise SystemExit, message

  #--------------------------------
  # Prepare For Plotting
  #--------------------------------
  fig, ax = plt.subplots(1, figsize=(6,4))
  ax.set_xlabel(r'$\log$' +r'$\, T_{\rm eff}$')
  ax.set_ylabel(r'$\log$' +r'$\, (L/L_\odot)$')

  import itertools
  colors = itertools.cycle(['k', 'r', 'b', 'grey', 'c', 'g', 'y', 'purple'])

  #--------------------------------
  # Read the grid iteratively
  #--------------------------------
  i_track = 0
  tracks_min_log_Teff = np.zeros(n_tracks)
  tracks_max_log_Teff = np.zeros(n_tracks)
  tracks_min_log_L    = np.zeros(n_tracks)
  tracks_max_log_L    = np.zeros(n_tracks)

  for i, dic in enumerate(list_dic):
    dic_track = dic['hist']
    mxg = 0
    if flag_log_g:
      log_g = dic_track['log_g']
      max_g = log_g.max()
      mxg = np.argmax(log_g)
    log_Teff = dic_track['log_Teff'][mxg:]
    n_lines = len(log_Teff)
    if flag_phot_L: log_L = np.log10(dic_track['photosphere_L'])[mxg:]
    if flag_log_L: log_L = dic_track['log_L'][mxg:]
    if flag_initial_mass: m_ini = dic_track['initial_mass']
    if flag_initial_z:    z_ini = dic_track['initial_z']
    if flag_surf_v_rot:  v_rot = dic_track['surf_avg_v_rot'][mxg:]
    if flag_surf_v_crit: v_cri = dic_track['surf_avg_v_crit'][mxg:]
    
    tracks_min_log_Teff[i_track] = log_Teff.min()
    tracks_max_log_Teff[i_track] = log_Teff.max()
    tracks_min_log_L[i_track] = log_L.min()
    tracks_max_log_L[i_track] = log_L.max()

    # Find appropriate on-screen text based on search param
    leg_label = ''
    if flag_initial_z and flag_z_all and not flag_mass_all:
      leg_label = r'$Z=$' + r'$' + '{:.3f}'.format(z_ini) + '$'
    #if flag_eta_all and not flag_mass_all:
      #leg_label = r'$\eta_{rot}$' + r'$' + '{:.2f}'.format()
           
    # Now, plot the track
    clr = colors.next()

    if list_lbls:
      ax.plot(log_Teff, log_L, linewidth = 2, linestyle='-', color=clr, 
               label = list_lbls[i_track], zorder=1)
    else:
      ax.plot(log_Teff, log_L, linewidth = 2, linestyle='-', color=clr, zorder=1)
    ax.scatter([log_Teff[0],], [log_L[0],], 50, color='purple', zorder=1)

    i_track += 1

  #--------------------------------
  # Final Plot Specifications
  #--------------------------------
  max_log_Teff = tracks_max_log_Teff.max()
  min_log_Teff = tracks_min_log_Teff.min()
  max_log_L    = tracks_max_log_L.max()
  min_log_L    = tracks_min_log_L.min()
  if log_Teff_from: max_log_Teff = log_Teff_from
  if log_Teff_to:   min_log_Teff = log_Teff_to
  if log_L_from:    min_log_L    = log_L_from
  if log_L_to:      max_log_L    = log_L_to
  ax.set_xlim(max_log_Teff, min_log_Teff)
  ax.set_ylim(min_log_L, max_log_L)
    
  #--------------------------------
  # Include Observed Stars
  #--------------------------------
  if list_dic_stars is not None:
    n_dic_stars = len(list_dic_stars)
    for i_dic, dic_star in enumerate(list_dic_stars):
      names = dic_star.keys()
      star_log_Teff = dic_star['log_Teff']
      star_log_Teff_err = dic_star['err_log_Teff_1s']
      star_log_L = dic_star['log_L']
      star_log_L_err = dic_star['err_log_L_1s']
      
      if 'marker' in names: marker = dic_star['marker']
      else: marker = 's'
      if 'ms' in names: ms = dic_star['ms']
      else: ms = 10
      if 'mfc' in names: mfc = dic_star['mfc']
      else: mfc = 'navy'
      if 'ecolor' in names: ecolor = dic_star['ecolor']
      else: ecolor = mfc
      if 'mec' in names: mec = dic_star['mec']
      else: mec = 'purple'
      
      #plt.errorbar(star_log_Teff, star_log_L, xerr=star_log_Teff_err, yerr=star_log_L_err, ecolor=ecolor,
                  #elinewidth=2, capsize=5, marker=marker, mfc=mfc, mec=mec, ms=ms, zorder=2, 
                  #label=r''+dic_star['name'][0])
      ax.errorbar(star_log_Teff, star_log_L, xerr=3.*star_log_Teff_err, 
                   yerr=[[3.*dic_star['er_L_l']], [3.*dic_star['er_L_u']]], ecolor=ecolor,
                  elinewidth=2, capsize=5, marker=marker, mfc=mfc, mec=mec, ms=ms, zorder=2, 
                  label=r''+dic_star['name'][0])
    if n_dic_stars <=10: leg_star = ax.legend(loc=4, fontsize='small')
    
  #--------------------------------
  # Annotations, Texts and Legend
  #--------------------------------
  if list_lbls:
    leg = ax.legend(loc=2, shadow=True)
  
  left = 0.06; top = 0.94; lower = 0.045

  #--------------------------------
  # Set Plot Name, and Finalize the Plot
  #--------------------------------
  if not file_out: 
    file_out = 'HR-Diagram.png'
  print '   HRD Grid plot stored at: ', file_out
  plt.savefig(file_out, dpi = 160)
  plt.close()

  return
  

#=================================================================================================================
def plot_single_hrd(recarr):
  """ 
  HR Diagram for a single track with on-plot details of the choices of
  initial mass, rotation rate w.r.t critical, overshooting, semi-convection 
  and metalicity.
  @param recarr: record array with keys taken from the MESA hist file header and columns
  @type recarr: numpy record array
  @return: None
  @rtype: None
  """ 
  
  required_keys = ['initial_mass', 'initial_z', 'log_Teff', 'photosphere_L', 'log_L', 
                   'surf_avg_v_rot', 'surf_avg_v_crit', 'log_g']
  avail_keys = recarr.dtype.names
  dic_avail_keys = {}
  for key in avail_keys:
    dic_avail_keys[key] = 0.0
  dic_keys = rd.check_key_exists(dic_avail_keys, required_keys)
  flag_initial_mass = dic_keys['initial_mass']
  flag_log_Teff = dic_keys['log_Teff']
  flag_phot_L   = dic_keys['photosphere_L']
  flag_log_L    = dic_keys['log_L']
  flag_log_g    = dic_keys['log_g']
  flag_surf_v_rot  = dic_keys['surf_avg_v_rot']
  flag_surf_v_crit = dic_keys['surf_avg_v_crit']

  if not flag_log_Teff or (not flag_log_L and not flag_phot_L):
    print 'HRD requires L and Teff. '
    print 'log_Teff:', flag_log_Teff, 'photosphere_L:', flag_phot_L, ' and log_L: ', flag_log_L
    print 'Error: plot_hrd: plot_single_hrd: Exit'
    raise SystemExit, 0
  
  mxg = 0                      # the index of maximum surface gravity. Marks the ZAMS
  if flag_log_g: 
    log_g = recarr['log_g']
    max_g = log_g.max()
    # mxg   = np.argmax(log_g)
  log_Teff = recarr['log_Teff'][mxg:]
  n_lines = len(log_Teff)
  if flag_phot_L: log_L = np.log10(recarr['photosphere_L'])[mxg:]
  if flag_log_L: log_L = recarr['log_L'][mxg:]
  if flag_initial_mass: m_ini = recarr['initial_mass']
  if flag_surf_v_rot: v_rot = recarr['surf_avg_v_rot'][mxg:]
  if flag_surf_v_crit: v_cri = recarr['surf_avg_v_crit'][mxg:]
  
  min_log_Teff = log_Teff.min()
  max_log_Teff = log_Teff.max()
  min_log_L    = log_L.min()
  max_log_L    = log_L.max()
  
  fig, ax = plt.subplots(1, figsize=(6, 4), dpi=160)
  plt.subplots_adjust(left=0.14, right=0.97, bottom=0.13, top=0.97)

  ax.set_xlim(max_log_Teff+0.02, min_log_Teff-0.02)
  ax.set_ylim(min_log_L-0.1, max_log_L+0.1)
  #xticks(np.arange(max_log_Teff, min_log_Teff, 0.05))
  #yticks(np.arange(min_log_L, max_log_L, 0.5))
  #ax.xaxis.set_major_locator()
  ax.set_ylabel(r'Luminosity $\log(L/L_\odot)$')
  ax.set_xlabel(r'Effective Temperature $\log T_{\rm eff}$')
   
  ax.plot(log_Teff, log_L, color='black', linewidth = 2, linestyle='-')
  ax.scatter([log_Teff[0],], [log_L[0],], 50, color='purple')

  file_out = 'HR-Diagram.png'
  print 'HRD plot stored at: ', file_out
  plt.savefig(file_out, dpi = 160)
  plt.close()

  return  
   
#=================================================================================================================
  
  
  
  