
ivs_sse Package
===============

Contents
--------
  * About
  * Disclaimer
  * Downloading
  * Installing
  * Updating
  * Uninstalling
  * Exercises
  * MESA Root Directory
  * Contact


About
-----
   The `ivs_sse` package provides the full lab materials, exercise texts and a set of analysis tools 
   written in [Python](http://www.diveintopython.net) to interact with MESA output files. The package 
   is centered around the [Stellar Structure and Evolution (SSE) course](
   https://onderwijsaanbod.kuleuven.be/syllabi/e/G0Y54AE.htm#activetab=doelstellingen_idp1404960) 
   taught by Prof. Dr. Conny Aerts and Dr. Ehsan Moravveji at Institute of Astronomy (IvS), KU Leuven.


Disclaimer
----------
   This repository is copyrighted to Ehsan Moravveji, and is freely available through bitbucket.
   It comes without any warranty; so, the users are encouraged to read the documentation of each 
   subroutine and also the source code before using any of the tools.

Downloading
-----------
   There are multiple ways you can download and store the source file(s).
   In what follows, the commands that you execute in your command prompt always start with
   `$>`.
   You can **clone** the latest version of the `ivs_sse` into your local
   machine, and start using it. Make sure you have [git](https://git-scm.com) already installed 
   on your machine. Then, all you need is to download the repisotory at any desired path on your 
   local working machine; let's call this local directory `<my-path>` hereafter. Then, in your 
   command prompt, execute the following:

   ```$> cd <my-path> ```
   
   ```$> git clone https://bitbucket.org/ehsan_moravveji/ivs_sse.git ```

   Now, a new directory should have been created called `<my-path>/ivs_sse`.
   If this is not the case, please contact me. You may spend some good time exploring different
   folders in the new repository.


Installing
----------
   The easiest way is to add the full path to the `ivs_sse` Python sub-directory in your
   `~/.bash_profile`. This ensures that you can still import and use the Python modules
   without requiring to install them on systems which you do not have any admin permission.
   For that, follow these simple steps:
   
   * In your command prompty, type:
   
     ```$> vim ~/.bash_profile ```

     Then, add the following line somewhere in this file, save and quit it (`Esc + :wq`),  
     and `source` it like below:
   
     ```export PYTHONPATH=$PYTHONPATH:<my-path>/ivs_sse ```
     
     ```$> source ~/.bash_profile ```
     
   * That should already work. So, the next step is to test, and make sure you may use it.
     You can now enter the Python command-line environment, and try to 
     `import` the `ivs_sse` package like the following:
   
     ```$> python ```
   
     ```$> import ivs_sse ```
   
   Basically, the import command should not complain, assuming that the paths are correctly 
   inserted in your `~/.bash_profile`, and the `sourc`ing has been successful.
   If there are no errors, you're ready to GO! Otherwise, contact me please.


Updates
-------
   Since the `ivs_sse` package is constantly evolving, you may always want to fetch the most up-to-date
   version of the repository. For that, you can still use the `git` command:
   
   ```$> cd <my-path> ```
   
   ```$> git pull ```
   
   or 
   
   ```$> git remote update ```
   
   Do you see the changes? If not, please contact me.


Uninstalling
------------
   It's always sad to say goodbye, but if you have to, just delete or comment out the
   path to the `ivs_sse` Python package from your `~/.bash_profile`:
 
   ```$> vim ~/.bash_profile ```
   
   ``` # export PYTHONPATH=$PYTHONPATH:<my-path>/ivs_sse ```

   followed by sourcing your `~/.bashrc` file again in your command prompt

   ```$> source ~/.bashrc ```

   That should be sufficient to restrict Python from any access to import the `ivs_sse`
   package.


Exercises
---------
   The exercises for the course are routinely updated, and you may find them by opening this file
   in your browser (e.g. Safari, Firefox, etc.):

   ```<my-path>/ivs_sse/exercises/_build/html/index.html ```

MESA Root Directory
-------------------
   At IvS, MESA is already installed and maintained (now at the official version 6794). You only need to 
   activate it, to have access to it, and start using it. For that, simply execute the following command 
   in your prompt once you're logged into one of the compute nodes (pleiades) at IvS
   
   ```$> source /home/setup/mesa-r6794.sh ```

   To check whether or not it worked, check if the MESA root directory `$MESA_DIR` is defined:
   
   ```$> echo $MESA_DIR ```

   If the output is empty, please contact our system administer, Bram Vandoren.


Contact
-------
   The `ivs_sse` repository is maintained by Ehsan Moravveji.
   Please feel free to forward your bug reports, feedbacks and comments to 
   Ehsan.Moravveji - at - ster.kuleuven.be
