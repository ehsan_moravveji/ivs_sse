
import glob
try:
  from ivs_sse import read, plot_hrd, plot_profile
except:
  print 'Failed to import from ivs_sse.'
  sys.exit()

##### NOTICE ##### 
# If you're not already familiar with how to use ivs_sse Python tools 
# to interact with MESA outputs, I recommend studying the other tutorial
# file called "ex01_reading.py" first. Below, I assume you perfectly know
# how to use read.read_multiple_mesa_files()

##### IDEA ##### 
# We use the MESA outputs in work/LOGS/ folder to create two simple plots:
# 1. HR diagram (using history file) 
# 2. Comparing internal temperature profiles of two models, one close to 
#    ZAMS, and another close to TAMS (using two profiles). 

##### READING ##### 
# First, find the relevant profile files, using glob
profile_files   = sorted(glob.glob('work/LOGS/profile*.data'))
# Then, read all ascii profile files
list_dics_prof  = read.read_multiple_mesa_files(profile_files, is_prof=True)
# choose only the first and last profile, and put them into a list 
prof_first_last = [list_dics_prof[0], list_dics_prof[-1]]

# Also, read the only history.data file, and return it as a list of dictionary
dic_hist  = read.read_multiple_mesa_files(['LOGS/history.data'], is_hist=True)

# Now, the plotting:
# To do/undo each of these plots, change False/True flags.

if False:
  print 'Create a HR diagram for a single track'
  plot_hrd.hrd(dic_hist, list_lbls=[r'History'], log_Teff_from=4.5, log_Teff_to=3.9, 
                    log_L_from=2, log_L_to=6, file_out='HRD.png')

if False:
  print 'Compare internal temperture profile (logT) for the first and last profiles'
  plot_profile.compare_column(prof_first_last, xaxis='mass', yaxis='logT', list_lbls=['First', 'Last'], 
  	               xlog=False, xpower=False, ylog=False, ypower=False, xaxis_from=0, xaxis_to=15, 
  	               yaxis_from=3, yaxis_to=6.5, xtitle=r'Mass', ytitle='$\log T$', 
  	               leg_loc=1, file_out='Compare-logT.png')
