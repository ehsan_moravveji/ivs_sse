
import sys, glob
try:
  from ivs_sse import read
except:
  print 'Failed to import from ivs_sse.'
  sys.exit()

# MESA produces only one history file per each track. 
# So, below, we give the absolute path to this file:
history_file  = 'work/LOGS/history.data'

# MESA can produce many profiles along each track.
# Thus, we have to search for all of them, and 
# get the list of profiles from the LOGS directory.
# in Python, sorted() sorts a list, and glob searches
# for files. Google it ...
profile_files = sorted(glob.glob('work/LOGS/profile*.prof'))
# "profile_files" gives a list of strings. Each item in the 
# list is a full path to one of profile files that we read 
# in the next step below
n_prof        = len(profile_files)
print ' Found {0} profiles'.format(n_prof)

##### Reading #####
# Read the history and profiles into a (list of) dictionary data structures
# we use read.read_multiple_mesa_files() function. Read the documentaion of 
# this subroutine in "ivs_sse/ivs_sse/read.py" to get familiar with its input
# and output.

##### HISTORY #####
# First, read the single history file, and reaturn the output as a dictionary:
dic_hist = read.read_multiple_mesa_files([history_file], is_hist=True, is_prof=False)[0]

# to know the contents of this dictionary:
print ' Available fields in a history are: \n', dic_hist.keys()

# Let's retrieve the header of the history as a new variable called "hist_header"
hist_header = dic_hist['header']
# What information can we find in the history header?
print '\n Available fields in the history header: \n', hist_header.dtype.names
# They should sound familiar! They are those items already at the top of your
# history file, right?

# And, let's retrieve the contents of the data. We store it as "hist_data"
hist_data   = dic_hist['hist']
# What columns (arrays) are available from this data part?
print '\n Available columns in the data: \n', hist_data.dtype.names
# They should sound familiar, too! They are those items in your history file columns

# Therefore, evolutionary (time-dependent) information is only available from the header, 
# and some from the data. As an example:
print ' What is the initial mass, and metalicity of the model?'
print ' M={0} Msun, and Z={1}'.format(hist_header['initial_mass'][0], hist_header['initial_z'][0])

print ' What is the starting and ending age of the model after the evolution?'
print ' Start Age={0}, and end age={1} [yr]'.format(hist_data['star_age'][0], hist_data['star_age'][-1])



##### PROFILE ##### 
# Reading profiles is very similar to history file, but here, we read a lot of files 
# at once, so we can give a list of filenames to read, and read.read_multiple_mesa_files()
# returns a list of dictionaries for all those input files. You get as you give!
# Read the profiles into a list of dictionaries:
list_dic_prof = read.read_multiple_mesa_files(profile_files, is_hist=False, is_prof=True)

# Now, let's take a look at only one profile from the whole list
# From the list of data, choose the last profile to work with
dic_prof = list_dic_prof[-1]

# to know the contents of the profile data dictionary:
print ' Available fields in a profile are: \n', dic_prof.keys()

# Let's retrieve the header of the file into a new variable called "prof_header"
prof_header = dic_prof['header']
print '\n Available fields in the profile header: \n', prof_header.dtype.names

# And, let's retrieve the contents of the data, 
prof_data   = dic_prof['prof']
print '\n Available columns in the data: \n', prof_data.dtype.names

# Therefore, equilibrium information (time-independent) is only available from the header, 
# and some from the data. As an example
print ' What is the age and effective temperature of this specific profile?'
print ' Age={0} yr, and Teff={1} K'.format(prof_header['star_age'][0], prof_header['Teff'][0])

# Now, you're familiar with reading and interacting with MESA outputs. You can use them to 
# create meaningful plots, and explore visually. For that purpose, you may now want to go to the
# next tutorial file: "ex02_plotting.py"